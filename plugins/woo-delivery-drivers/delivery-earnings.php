<?php
/**
 *  Dokan Dashboard Template
 *
 *  Dokan Main Dahsboard template for Fron-end
 *
 *  @since 2.4
 *
 *  @package dokan
 */
?>
<div class="dokan-dashboard-wrap">
    <?php
        /**
         *  dokan_dashboard_content_before hook
         *
         *  @hooked get_dashboard_side_navigation
         *
         *  @since 2.4
         */
        do_action( 'dokan_dashboard_content_before' );
    ?>

    <div class="dokan-dashboard-content">

        <?php
            /**
             *  dokan_dashboard_content_before hook
             *
             *  @hooked show_seller_dashboard_notice
             *
             *  @since 2.4
             */
            do_action( 'dokan_help_content_inside_before' );
			
			
        ?>

        <article class="help-content-area earning_mn">
        <div class="dokan-ajax-response"></div>
        <button class="tablink dokan-btn dokan-btn-theme" onclick="openPage('Home', this, 'green')" id="defaultOpen"><i class="fa fa-user">&nbsp;</i> All Delivery</button>
    	 <!--<button class="tablink dokan-btn dokan-btn-theme" onclick="openPage('reg_dr', this, 'green')" >Earnings</button>
		<button class="tablink dokan-btn dokan-btn-theme" onclick="openPage('News', this, 'green')" >Delivery Drivers</button>-->
		
        <!-- <?php if('stripe' == get_option( 'ddwc_driver_payment_type' )){ ?>
       <a href="<?php echo home_url('my-account/commission-withdraw/'); ?>" class="tablink dokan-btn dokan-btn-theme" id="stripe_withdrow">Withdraw Earning by Stripe</a> 
      <?php } ?> -->
        <?php $orders_counts = dokan_count_orders( dokan_get_current_user_id() ); ?>
        
        
        
        <div id="Home" class="tabcontent">
    		
        <?php 
		
			$vendor_id = get_current_user_id();
			global $wpdb;
			global $woocommerce;
			if (isset($_GET['pageno'])) {
				$pageno = $_GET['pageno'];
			} else {
				$pageno = 1;
			}
			$no_of_records_per_page = 10;
			$offset = ($pageno-1) * $no_of_records_per_page; 
			
			/*if(isset($_GET['order_status'])){
				$status = $_GET['order_status'];
				$status_qr = "AND order_status='$status";
			}else{
				$status_qr = 'all';
				}*/
		
				
		
			
			$ruseltsal = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}dokan_orders WHERE seller_id =$vendor_id");
			
			$total_rows =count($ruseltsal);
			$total_pages = ceil($total_rows / $no_of_records_per_page);
				
			//$ruselts = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}drivers_earning WHERE vendor_id =$vendor_id LIMIT $offset, $no_of_records_per_page");	
			$ruselts = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}dokan_orders WHERE seller_id =$vendor_id  LIMIT $offset, $no_of_records_per_page");	
				
			$total_earning = $wpdb->get_results("SELECT SUM(delivery_fee) as earning FROM {$wpdb->prefix}drivers_earning WHERE vendor_id =$vendor_id AND payment_reciver lIKE 'vendor'");
			
			
	
	$current_bl = $result->earnings - $result->withdraw;
	
	$earning = number_format((float)$current_bl, 2, '.', '');
			
			
		?>
            
         <div class="total_delivery">
        	
            <div class="del-earning half-bls">
            	<div class="del-bl">
                	<div class="del-icn"><i class="fa fa-money"></i></div><div class="del-count"><p class="ord_count"><?php echo round($total_earning[0]->earning,2); ?></p> <p>Delivery Earnings</p></div>
                    <div style="clear:both;"></div>
                </div>
            </div> 
            
            <div class="del-cmpl half-bls">
            	<div class="del-bl">
                	<div class="del-icn"><i class="fa fa-truck">&nbsp;</i></div><div class="del-count"><p class="ord_count"><?php echo $orders_counts->{'wc-completed'}; ?></p> <p>Delivered</p></div>
                    <div style="clear:both;"></div>
                </div>
            </div>
            
            <div class="del-pending half-bls">
            	<div class="del-bl">
                	<div class="del-icn pending"><i class="fa fa-truck">&nbsp;</i></div><div class="del-count"><p class="ord_count"><?php echo $orders_counts->total - $orders_counts->{'wc-completed'}; ?></p> <p>Pending</p></div>
                    <div style="clear:both;"></div>
                </div>
            </div>
        
        </div>   
            <div style="clear:both;"></div>
 <!--<ul class="list-inline order-statuses-filter earning_filter">
  <li> <a href="<?php echo home_url('dashboard/delivery-earnings'); ?>"> All (<?php echo $total_rows; ?>) </a> </li>
  <li> <a href="<?php echo home_url('dashboard/delivery-earnings'); ?>?order_status=wc-completed"> Completed  </a> </li>
  <li> <a href="<?php echo home_url('dashboard/delivery-earnings'); ?>?order_status=wc-processing"> Processing  </a> </li>
  <li> <a href="<?php echo home_url('dashboard/delivery-earnings'); ?>?order_status=wc-ready-to-ship"> Ready to Ship </a> </li>
  <li> <a href="<?php echo home_url('dashboard/delivery-earnings'); ?>?order_status=wc-driver-assigned"> Driver Assigned  </a> </li>
  <li> <a href="<?php echo home_url('dashboard/delivery-earnings'); ?>?order_status=wc-out-for-delivery"> Out for Delivery </a> </li>
  <li> <a href="<?php echo home_url('dashboard/delivery-earnings'); ?>?order_status=wc-order-returned"> Order Returned </a> </li>
</ul>-->
               
        
 <form id="deliveryorder-details" method="POST" class="dokan-form-inline">
  <table class="dokan-table dokan-table-striped">
    <thead>
      <tr>
        <th id="cb" class="manage-column column-cb"> 
         Sr.No.
        </th>
        <th>Order</th>
        <th>Order Total</th>
        <th>Delivery Fee</th>
        <th>Paid To</th>
       <?php /*?> <th>Payment Type</th><?php */?>
        <th>Status</th>
        <th>Driver</th>
        <th>Date</th>
        
      </tr>
    </thead>
     <tbody>
    <?php if(count($ruselts)> 0) {
    	
		$srno = $offset + 1;
   		foreach($ruselts as $key => $val){
			
		$order = wc_get_order( $val->order_id );	
    	
		
		$delivery_details = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}drivers_earning WHERE vendor_id =$val->seller_id AND order_id = $val->order_id");	
		$date=date_create($delivery_details[0]->dat_created);
		
		$driver_name = $delivery_details[0]->driver_name;
		$driver_id = get_post_meta($val->order_id, 'ddwc_driver_id', true);
		if(empty($driver_name) && !empty($driver_id)){
				$driver_name = get_user_meta($driver_id, 'first_name', true).' '.get_user_meta($driver_id, 'last_name', true);
			}
		
		//echo  round($delivery_details[0]->delivery_fee, 2);
		
		
	 
    ?>
    
      <tr>
        <th class="dokan-order-select check-column"> <?php echo $srno; ?>
        </th>
        <td class="dokan-order-id column-primary" data-title="Order">
        
          <?php if ( current_user_can( 'dokan_view_order' ) ): ?>
                                <?php echo '<a href="' . esc_url( wp_nonce_url( add_query_arg( array( 'order_id' => $val->order_id ), dokan_get_navigation_url( 'orders' ) ), 'dokan_view_order' ) ) . '" target="_blank"><strong>' . sprintf( __( '#%s', 'dokan-lite' ), esc_attr( $val->order_id ) ) . '</strong></a>'; ?>
                            <?php else: ?>
                                <?php echo '<strong>' . sprintf( __( 'Order %s', 'dokan-lite' ), esc_attr( $val->order_id ) ) . '</strong>'; ?>
                            <?php endif ?>
        
        </td> 
        <td class="dokan-order-total" data-title="Order Total">
        	
             <?php echo $order->get_formatted_order_total(); ?>
        
        </td>
        <td class="dokan-order-earning" data-title="Earning">
        <?php //if($delivery_details[0]->payment_reciver =='vendor'){?>
        	<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span><?php echo round($delivery_details[0]->delivery_fee, 2);  ?></span>
        <?php //} ?>
        </td>
        <td class="dokan-order-status" data-title="Status"><span class="dokan-label1 reciver-<?php echo $delivery_details[0]->payment_reciver; ?>" style="text-transform:capitalize;"> <?php echo $delivery_details[0]->payment_reciver; ?> </span></td>
         <td class="dokan-order-status" data-title="Status"><span class="dokan-label dokan-label-earning" style="text-transform:capitalize;"> <?php echo dokan_get_prop( $order, 'status' ); ?> </span></td>
        <td class="dokan-order-customer" data-title="<?php echo $driver_name;?>"> <?php echo $driver_name; ?> </td>
        <td class="dokan-order-date" data-title="Date"><abbr title="<?php echo date_format($date,"m-d-Y H:i:s"); ?>"><?php echo date_format($date,"m-d-Y H:i:s"); ?></abbr></td>
        <td class="diviader"></td>
      </tr>
     <?php
	 $srno++;
		}
    
     }else{ 
		echo '<tr>
        <th class="dokan-order-select check-column"> Opps No data found!
        </th>';
		echo ' </tr>';
		
	 } ?>
    </tbody>
  </table>
</form>

<div class="pagination-wrap">

	<?php $pagenum      = isset( $_GET['pageno'] ) ? absint( $_GET['pageno'] ) : 1;
                    $base_url = dokan_get_navigation_url('delivery-earnings');

                    if ( $total_pages > 1 ) {
                        echo '<div class="pagination-wrap">';
                        $page_links = paginate_links( array(
                            'current'   => $pagenum,
                            'total'     => $total_pages,
                            'base'      => $base_url. '%_%',
                            'format'    => '?pageno=%#%',
                            'add_args'  => false,
                            'type'      => 'array',
                            'prev_text' => __( '&laquo; Previous', 'dokan-lite' ),
                            'next_text' => __( 'Next &raquo;', 'dokan-lite' )
                        ) );

                        echo '<ul class="pagination"><li>';
                        echo join("</li>\n\t<li>", $page_links ); // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped
                        echo "</li>\n</ul>\n";
                        echo '</div>';
                    }
	?>				
   
</div>

     
          	
        </div>
        
        <div id="reg_dr" class="tabcontent">
        
        
        
        </div>
        
        

        </article><!-- .dashboard-content-area -->

         <?php
            /**
             *  dokan_dashboard_content_inside_after hook
             *
             *  @since 2.4
             */
            do_action( 'dokan_dashboard_content_inside_after' );
        ?>


    </div><!-- .dokan-dashboard-content -->

    <?php
        /**
         *  dokan_dashboard_content_after hook
         *
         *  @since 2.4
         */
        do_action( 'dokan_dashboard_content_after' );
    ?>
    
             
             
    
<style>
article.help-content-area.earning_mn button {
    margin-top: 0;
}
ul.earning_filter {
    display: inline-block;
    margin-left: 0;
    padding-left: 0;
}
ul.earning_filter li {
    display: inline-block;
    padding-right: 22px !important;
}

span.dokan-label.dokan-label-earning {
    background: #454545;
    font-size: 14px;
    font-weight: 500;
}
.del-count p {
    margin-bottom: 0;
    font-size: 15px;
}
.total_delivery .half-bls {
    width: 33.3333%;
    float: left;
    margin-bottom: 36px;
    text-align: center;
    margin-top: 10px;
}
.del-bl {
    border: 1px solid #ddd;
    width: 90%;
    margin: 0 auto;
    padding: 0px;
    border-radius: 4px;
    box-shadow: 0px 0px 12px 3px #d8d8d8;
}
.del-icn {
    width: 32%;
    float: left;
    padding: 20px;
    font-size: 26px;
    color: #fff;
    background: green;
    border-radius: 4px 0px 0px 4px;
}
.del-icn.pending {
    background: red;
}
.del-count {
    width: 68%;
    float: left;
    padding: 9px;
}
p.ord_count {
    font-size: 24px;
    color: #066fa0;
    font-weight: 600;
}
</style>    
    
<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();


</script>
</div><!-- .dokan-dashboard-wrap -->