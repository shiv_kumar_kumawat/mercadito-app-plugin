jQuery(document).ready(function ($) {
	jQuery("select#ddwc_driver_id").change(function(e) {
		var itemid =  $(this).attr("name");
		var metakey =  $(this).attr("id");
		var metavalue = this.value;
		// Update driver ID metadata.
		$.post(WPaAjax.ajaxurl,{
			action : "ddwc_delivery_driver_settings",
			item_id : itemid,
			metakey : metakey,
			metavalue : metavalue
		});
	});
	// Remove the link click wrapper on WooCommerce Edit Orders screen.
	$("td.delivery_driver.column-delivery_driver a").click(function(){ return false });
	
jQuery(".add_note").click(function(){
		
var row_id = jQuery(this).attr("data-rowid");
	jQuery('.withdrow_note').append('<div class="dokan-modal-dialog"><div class="dokan-modal"><div class="dokan-modal-content" style="width: 500px; height: auto;"><section class="dokan-modal-main has-footer"><header class="modal-header"><h1>Update Note</h1> <button class="modal-close modal-close-link dashicons dashicons-no-alt" onclick="close_module();"><span class="screen-reader-text">Close modal panel</span></button></header> <div class="modal-body"><textarea rows="3"></textarea></div> <footer class="modal-footer"><div class="inner"><button class="button button-primary button-large"  onclick="admin_update_note('+row_id+');">Update Note</button></div></footer></section></div></div> <div class="dokan-modal-backdrop" onclick="close_module();"></div></div>');
	return false;
	});

	
});
 
function close_module(){
		jQuery(".withdrow_note").empty();
}

function admin_update_note(row_id){
	
	var siteurl = WPaAjax.ajaxurl;
	var message = jQuery('.modal-body textarea').val();
	
	jQuery.ajax({
                type: 'POST',
                dataType: "json",
                url :  siteurl,
                data : {
					row_id : row_id,
                    message: message,
                    action: 'adminUpdateNote',
                }, 
                success : function(response){
				if(response) {
					alert(response.message);
					location.reload();
				
				}
               } 
       });
		
	
}


