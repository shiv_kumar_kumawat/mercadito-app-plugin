<?php
/**
 * Helper functions.
 *
 * @link       https://www.deviodigital.com
 * @since      2.0
 *
 * @package    DDWC
 * @subpackage DDWC/admin
 * @author     Devio Digital <contact@deviodigital.com>
 */

/**
 * Change order statuses
 *
 * @since 2.0
 */
function ddwc_driver_dashboard_change_statuses() {

	// Get the order.
	$order_id = filter_input( INPUT_GET, 'orderid' );
	$order    = wc_get_order( $order_id );

	// Out for delivery + note.
	$out_for_delivery = filter_input( INPUT_POST, 'outfordelivery' );
	$driver_note      = filter_input( INPUT_POST, 'outfordeliverymessage' );

	// Redirect URL.
	$redirect_url = apply_filters( 'ddwc_driver_dashboard_change_statuses_redirect_url', get_option( 'woocommerce_myaccount_page_id' ) . 'driver-dashboard/?orderid=' . $order_id );

	do_action( 'ddwc_driver_dashboard_change_statuses_top' );

	// Update order status if marked OUT FOR DELIVERY by Driver.
	if ( isset( $out_for_delivery ) ) {

		// Update order status.
		$order->update_status( 'out-for-delivery' );

		// Add driver note (if added).
		if ( isset( $driver_note ) && ! empty( $driver_note ) ) {
			// The text for the note.
			$note = esc_attr__( 'Driver Note', 'ddwc' ) . ': ' . esc_html( $driver_note );
			// Add the note
			$order->add_order_note( $note );
			// Save the data
			$order->save();
		}

		// Run additional functions.
		do_action( 'ddwc_email_customer_order_status_out_for_delivery' );

		// Redirect so the new order details show on the page.
		wp_redirect( get_permalink( apply_filters( 'ddwc_driver_dashboard_change_status_out_for_delivery_url', $redirect_url, $order_id ) ) );
	}

	// Variables for order returned.
	$order_returned = filter_input( INPUT_POST, 'orderreturned' );
	$driver_note    = filter_input( INPUT_POST, 'ordermessage' );

	// Update order status if marked RETURNED by Driver.
	if ( isset( $order_returned ) ) {

		// Update order status.
		$order->update_status( 'order-returned' );

		// Add driver note (if added).
		if ( isset( $driver_note ) && ! empty( $driver_note ) ) {
			// The text for the note.
			$note = esc_attr__( 'Driver Note', 'ddwc' ) . ': ' . esc_html( $driver_note );
			// Add the note
			$order->add_order_note( $note );
			// Save the data
			$order->save();
		}

		// Run additional functions.
		do_action( 'ddwc_email_admin_order_status_returned' );

		// Redirect so the new order details show on the page.
		wp_redirect( get_permalink( apply_filters( 'ddwc_driver_dashboard_change_status_returned_url', $redirect_url, $order_id ) ) );
	}

	$order_completed = filter_input( INPUT_POST, 'ordercompleted' );
	$driver_note     = filter_input( INPUT_POST, 'ordermessage' );

	// Update order status if marked COMPLETED by Driver.
	if ( isset( $order_completed ) ) {

		// Update order status.
		$order->update_status( 'completed' );

		// Add driver note (if added).
		if ( isset( $driver_note ) && ! empty( $driver_note ) ) {
			// The text for the note.
			$note = esc_attr__( 'Driver Note', 'ddwc' ) . ': ' . esc_html( $driver_note );
			// Add the note
			$order->add_order_note( $note );
			// Save the data
			$order->save();
		}

		// Run additional functions.
		do_action( 'ddwc_email_admin_order_status_completed' );

		// Redirect so the new order details show on the page.
		wp_redirect( get_permalink( apply_filters( 'ddwc_driver_dashboard_change_status_completed_url', $redirect_url, $order_id ) ) );
	}

	do_action( 'ddwc_driver_dashboard_change_statuses_bottom' );

}
add_action( 'wp_loaded', 'ddwc_driver_dashboard_change_statuses' );

/**
 * Change order status forms
 *
 * Displayed on the driver dashboard, allowing the driver to change
 * the status of an order as they deliver to customer.
 *
 * @since 2.0
 */
function ddwc_driver_dashboard_change_status_forms() {

	// Get the order ID.
	$order_id     = filter_input( INPUT_GET, 'orderid' );
	$order        = wc_get_order( $order_id );
	$order_data   = $order->get_data();
	$order_status = $order_data['status'];

	do_action( 'ddwc_driver_dashboard_change_status_forms_top' );

	// Create variable.
	$change_status = '';

	// Change status form if status is "driver assigned".
	if ( 'driver-assigned' == $order_status ) {
		$change_status  = '<h4>' . esc_attr__( 'Change Status', 'ddwc' ) . '</h4>';
		$change_status .= '<form method="post">';
		$change_status .= '<p><strong>' . esc_attr__( 'Message for store manager (optional)', 'ddwc' ) . '</strong></p>';
		$change_status .= '<input type="text" name="outfordeliverymessage" value="" placeholder="' . esc_attr__( 'Add a message to the order', 'ddwc' ) . '" class="ddwc-ofdmsg" />';
		$change_status .= '<input type="hidden" name="outfordelivery" value="out-for-delivery" />';
		$change_status .= '<input type="submit" value="' . esc_attr__( 'Out for Delivery', 'ddwc' ) . '" class="button ddwc-change-status" />';
		$change_status .= wp_nonce_field( 'ddwc_out_for_delivery_nonce_action', 'ddwc_out_for_delivery_nonce_field' ) . '</form>';
	}

	// Change status form if status is "out for delivery".
	if ( 'out-for-delivery' == $order_status ) {
		$change_status  = '<h4>' . esc_attr__( 'Change Status', 'ddwc' ) . '</h4>';
		$change_status .= '<form method="post" class="driver_updateorder" id="dr_updateorder">';
		$change_status .= '<p><strong>' . esc_attr__( 'Message for store manager (optional)', 'ddwc' ) . '</strong></p>';
		$change_status .= '<input type="text" name="ordermessage" value="" placeholder="' . esc_attr__( 'Add a message to the order', 'ddwc' ) . '" class="ddwc-ofdmsg" />';
		$change_status .= '<input type="submit" name="orderreturned" data-val="return" value="' . esc_attr__( 'Returned', 'ddwc' ) . '" class="button ddwc-change-status order-returned" />';
		$change_status .= '<input type="submit" id="ordercompleted" data-val="complete" name="ordercompleted" value="' . esc_attr__( 'Completed', 'ddwc' ) . '" class="button ddwc-change-status" />';
		$change_status .= wp_nonce_field( 'ddwc_order_completed_nonce_action', 'ddwc_order_completed_nonce_field' ) . '</form>';
		
		$change_status .= '<div class="otpmatch" style="display:none;"><div class="otpmatch_section">
		<form action="#" method="post" id="otpmatch_form">
<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
<label for="order_otp">Enter OTP<span class="required">*</span></label>
<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="order_otp" id="order_otp" autocomplete="given-name" value="" required>
<input type="hidden" value="'.$order_id.'" id="otporder" name="otporder" />
</p>
<button type="submit" class="woocommerce-Button button otpsubmit" name="save_account_details" value="Save changes">Submit</button>
<!--<button type="submit" class="woocommerce-Button button withoutotpsubmit" name="save_account_details" value="">Complete without OTP</button>-->
</form>
</div>
</div>';
	}

	do_action( 'ddwc_driver_dashboard_change_status_forms_bottom' );

	// Display order status form.
	echo apply_filters( 'ddwc_driver_dashboard_change_status', $change_status, $order_status );
}

/**
 * Checks if a particular user has one or more roles.
 *
 * Returns true on first matching role. Returns false if no roles match.
 *
 * @uses get_userdata()
 * @uses wp_get_current_user()
 *
 * @param array|string $roles Role name (or array of names).
 * @param int $user_id (Optional) The ID of a user. Defaults to the current user.
 * @return bool
 */
function ddwc_check_user_roles( $roles, $user_id = null ) {
	// Set user.
    if ( is_numeric( $user_id ) ) {
        $user = get_userdata( $user_id );
	} else {
        $user = wp_get_current_user();
	}
	// Bail if no user.
    if ( empty( $user ) ) {
        return false;
	}
	// Get user roles.
    $user_roles = (array) $user->roles;
	// Loop through user roles.
    foreach ( (array) $roles as $role ) {
        if ( in_array( $role, $user_roles ) ) {
			return true;
		}
    }

    return false;
}

/**
 * Delivery driver average rating
 *
 * @param int $driver_id
 * @return void
 * @since 2.5
 */
function ddwc_driver_rating( $driver_id ) {
	/**
	 * Args for Orders with Driver ID attached
	 */
	$args = array(
		'post_type'      => 'shop_order',
		'posts_per_page' => -1,
		'post_status'    => 'any',
		'meta_key'       => 'ddwc_driver_id',
		'meta_value'     => $driver_id
	);

	/**
	* Get Orders with Driver ID attached
	*/
	$assigned_orders = get_posts( $args );
	$order_count     = 0;
	$driver_rating   = 0;

	/**
	* If Orders have Driver ID attached
	*/
	if ( $assigned_orders ) {
		// Loop through orders.
		foreach ( $assigned_orders as $driver_order ) {

			// Get an instance of the WC_Order object.
			$order = wc_get_order( $driver_order->ID );

			// Get the order data.
			$order_data   = $order->get_data();
			$order_id     = $order_data['id'];
			$order_status = $order_data['status'];

			// Only run if the order is Completed.
			if ( 'completed' === $order_status ) {
				$order_rating = get_post_meta( $order_id, 'ddwc_delivery_rating', TRUE );
				// Display driver rating.
				if ( ! empty( $order_rating ) ) {
					$order_count++; // potential ratings.
					$driver_rating = $driver_rating + $order_rating;
				}
			}
		}
	}

	// Set defaults.
	$average_rating      = NULL;
	$driver_rating_final = '';

	if ( 0 != $driver_rating ) {
		// Average rating.
		$average_rating = $driver_rating / $order_count;
		$average_rating = round( $average_rating, 1 );
		// Driver rating final.
		$driver_rating_final = $average_rating . '/5';
	}

	return $driver_rating_final;
}

/**
 * Delivery Adddress Google Map Geocode
 *
 * @param string $delivery_address
 * @since 2.7
 */
function ddwc_delivery_address_google_map_geocode( $delivery_address ) {

	// Prepare the delivery address for Google Maps geocode.
	$delivery_address = str_replace( ' ', '+', $delivery_address );

	// Get delivery address details from Google Maps.
	$geocode = file_get_contents( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $delivery_address . '&key=' . get_option( 'ddwc_settings_google_maps_api_key' ) );
	$output  = json_decode( $geocode );

	// Get the delivery address latitude and longitude.
	$latitude  = $output->results[0]->geometry->location->lat;
	$longitude = $output->results[0]->geometry->location->lng;

	// Delivery address (lat/lng).
	$delivery_address = $latitude . ',' . $longitude;

	// Error messages.
	if ( 'OK' != $output->status ) {
		// Remove the map via it's filter.
		add_filter( 'ddwc_delivery_address_google_map', '__return_false' );

		// Default error message.
		$error_message = __( 'The delivery address is returning NULL.', 'ddwc' );

		// Google Maps error message.
		if ( NULL != $output ) {
			$error_message = $output->error_message;
		}
		// Display an error message.
		echo '<p class="ddwc-map-api-error-msg">' . esc_html( $error_message ) . '</p>';
	}

	return $delivery_address;
}

/**
 * Driver table for administrators
 * 
 * This function is used in the driver_dashboard in order to display a table of all 
 * drivers, along with specific details about each driver.
 * 
 * @since  3.0
 * @return string
 */
function ddwc_driver_dashboard_admin_drivers_table() {
	// Driver args.
	$args = array(
		'role'    => 'driver',
		'orderby' => 'user_nicename',
		'order'   => 'ASC'
	);

	// Filter args.
	$args = apply_filters( 'ddwc_driver_dashboard_admin_drivers_args', $args );

	// Get users.
	$drivers = get_users( $args );

	/**
	 * If Orders have Driver ID attached
	 */
	if ( $drivers ) {

		$thead = array(
			esc_attr__( 'Name', 'ddwc' ),
			esc_attr__( 'Status', 'ddwc' ),
			esc_attr__( 'Rating', 'ddwc' ),
			esc_attr__( 'Contact', 'ddwc' ),
		);

		$thead = apply_filters( 'ddwc_driver_dashboard_admin_table_thead', $thead );

		// Drivers table title.
		$drivers_table  = '<h3 class="ddwc delivery-drivers">' . __( 'Delivery Drivers', 'ddwc' ) . '</h3>';

		// Drivers table start.
		$drivers_table .= '<table class="ddwc-dashboard delivery-drivers">';

		// Drivers table head.
		$drivers_table .= '<thead><tr>';

		// Loop through $thead.
		foreach ( $thead as $row ) {
			// Add td to thead.
			$drivers_table .= '<td>' . $row . '</td>';
		}

		// End drivers table thead.
		$drivers_table .= '</tr></thead>';

		// Drivers table tbody.
		$drivers_table .= '<tbody>';

		// Loop through drivers.
		foreach ( $drivers as $driver ) {
			// Driver unavailable.
			$availability = '<span class="driver-status unavailable">' . esc_attr__( 'Unavailable', 'ddwc' ) . '</span>';

			// Driver available.
			if ( get_user_meta( $driver->ID, 'ddwc_driver_availability', true ) ) {
				$availability = '<span class="driver-status available">' . esc_attr__( 'Available', 'ddwc' ) . '</span>';
			}

			// Driver rating.
			$driver_rating_final = ddwc_driver_rating( $driver->ID );

			// Driver phone number.
			$driver_number = get_user_meta( $driver->ID, 'billing_phone', true );

			// Empty var.
			$phone_number = '';

			// Driver phone number button.
			if ( $driver_number ) {
				$phone_number = '<a href="tel:' . esc_html( $driver_number ) . '" class="button ddwc-button"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M20 22.621l-3.521-6.795c-.008.004-1.974.97-2.064 1.011-2.24 1.086-6.799-7.82-4.609-8.994l2.083-1.026-3.493-6.817-2.106 1.039c-7.202 3.755 4.233 25.982 11.6 22.615.121-.055 2.102-1.029 2.11-1.033z"/></svg></a>';
			}

			// Get driver userdata.
			$user_info = get_userdata( $driver->ID );

			// Driver email address.
			$driver_email = $user_info->user_email;

			// Empty var.
			$email_address = '';

			// Driver email address button.
			if ( $driver_email ) {
				$email_address = '<a href="mailto:' . esc_html( $driver_email ) . '" class="button ddwc-button"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M0 3v18h24v-18h-24zm6.623 7.929l-4.623 5.712v-9.458l4.623 3.746zm-4.141-5.929h19.035l-9.517 7.713-9.518-7.713zm5.694 7.188l3.824 3.099 3.83-3.104 5.612 6.817h-18.779l5.513-6.812zm9.208-1.264l4.616-3.741v9.348l-4.616-5.607z"/></svg></a>';
			}

			if ( get_user_meta( $driver->ID, 'ddwc_driver_picture', true ) ) {
				$driver_pic = get_user_meta( $driver->ID, 'ddwc_driver_picture', true );
				$driver_img = '<a href="' . $driver_pic['url'] . '"><img src="' . $driver_pic['url'] . '" alt="' . esc_html( $driver->display_name ) . '" /></a>';
			}

			$tbody = array(
				$driver_img . '<span class="driver-name">' . esc_html( $driver->display_name ) . '</span> <a href="' . admin_url( 'user-edit.php?user_id=' . $driver->ID ) . '">(' . __( 'edit', 'ddwc' ) . ')</a>',
				$availability,
				$driver_rating_final,
				$email_address . $phone_number,
			);

			$tbody = apply_filters( 'ddwc_driver_dashboard_admin_table_tbody', $tbody );

			// Drivers tbody tr.
			$drivers_table .= '<tr>';

			// Loop through $tbody.
			foreach ( $tbody as $row ) {
				// Add td to tbody.
				$drivers_table .= '<td>' . $row . '</td>';
			}

			// End drivers table tbody tr.
			$drivers_table .= '</tr>';
		}

		// End drivers table tbody.
		$drivers_table .= '</tbody>';

		// End drivers table.
		$drivers_table .= '</table>';
	}

	do_action( 'ddwc_admin_drivers_table_before' );

	// Display table.
	echo $drivers_table;

	do_action( 'ddwc_admin_drivers_table_after' );
}


//Driver Registration 



function update_user_profile() {
	

	
	$test_enable = get_option( 'ddwc_driver_test_enable');
	
	if($test_enable == 'yes'){
		$secret_key = get_option( 'ddwc_driver_test_secret_key');
	}else{
		$secret_key = get_option( 'ddwc_driver_live_secret_key');;
		}
	
	$licence_imgfront   = sanitize_text_field( $_POST['attach_id_front'] );
    $licence_img_back 	= sanitize_text_field( $_POST['attach_id_back'] );
	$l_front =  wp_get_attachment_image_src( $licence_imgfront);
	$l_back = wp_get_attachment_image_src( $licence_img_back);
	
	$account_number   		= sanitize_text_field( $_POST['ddwc_driver_account_number'] );
	$bank_name      		= sanitize_text_field( $_POST['ddwc_driver_bank_name'] );
    $account_holder_name    = sanitize_text_field( $_POST['ddwc_driver_account_holder_name'] );
    $account_holder_type 	= sanitize_text_field( $_POST['ddwc_driver_account_holder_type'] );
	$ssn_number    			= sanitize_text_field( $_POST['ddwc_driver_ssn_number'] );
	$ssn_last4 = substr($ssn_number, -4);
	$routing_number 		= sanitize_text_field( $_POST['ddwc_driver_routing_number'] );
	$date_ofb 				= sanitize_text_field( $_POST['ddwc_driver_dob'] );
	$date  = strtotime($date_ofb);
	$day   = date('d',$date);
	$month = date('m',$date);
	$year  = date('Y',$date);	
		
		
     $user_id = $_POST['driver_id'];
	
	 $trans_type = $_POST['ddwc_driver_transportation_type'];
	 $vehical_mdl = $_POST['ddwc_driver_vehicle_model'];
	 $vehical_clr = $_POST['ddwc_driver_vehicle_color'];
	 $license_plate = $_POST['ddwc_driver_license_plate']; 
	 $driver_phone = $_POST['phone']; 
	 $driver_city = $_POST['ddwc_driver_transportation_city'];
	
	if($user_id){
		
		$user_info = get_userdata( $user_id);
		$dr_email = $user_info->user_email;
		$dr_fname = $user_info->first_name;
		$dr_lname = $user_info->last_name;
	
	try {	  
		//$full_name = $dr_fname.' '.$dr_lname;  
		$urls = home_url('my-account');
		$stripe = new \Stripe\StripeClient($secret_key);
		$c_date = time();
		$account = $stripe->accounts->create([
		 "type" => "custom",
		 "country" => "US",
		 "email" => $dr_email,
		//"requested_capabilities" => ["card_payments", 'transfers'],
		'capabilities' => [
	   // 'card_payments' => ['requested' => true],
		'transfers' => ['requested' => true],
	  ],
		 "business_profile" => ["name" => "Love IE Local","url" => "https://loveielocal.com/", "mcc"=>'5045'],
		 "business_type" => "individual",
		 "individual" => ["first_name" => $dr_fname, "last_name" => $dr_lname, "dob"=>["day"=>$day,"month"=>$month,"year"=>$year],
		 "ssn_last_4" => $ssn_last4, "id_number" => $ssn_number, "phone" => $driver_phone,"email" => $dr_email,
		 "address" => ["city" => "Fall River", "state" => "MA" , "postal_code" => 02721, "line1" => "374 William S Canning Blvd"],
		  ],
		 "external_account" => ["object"=>"bank_account","currency"=>"usd","country"=>"US","account_number" => $account_number , "routing_number"=>$routing_number, "bank_name" => $bank_name, "account_holder_name" => $account_holder_name, "account_holder_type" => 'individual'],
		 
		  //"external_account" => ["object"=>"bank_account","currency"=>"usd","country"=>"US","account_number" => '000123456789' , "routing_number"=>'110000000', "bank_name" => 'TEST STRIPE BANK', "account_holder_name" => $account_holder_name, "account_holder_type" => 'individual'],
		 
		 "tos_acceptance" => ["date"=>$c_date,"ip"=>$_SERVER['REMOTE_ADDR']]
	]);
		
		
		$stripeAccountId = $account->id;
		
		
		if($stripeAccountId){
			
		$stripeAccountObj = $stripe->accounts->retrieve($stripeAccountId);
			
		//$error = new WP_Error();

        //Some validation

       // if(is_wp_error()){
        //    $error_code = $error->get_error_code(); //returns "my-error-code" (String)
         //   throw new Exception( "Error thrown"); //How to send error code
      //  }   	
			
		
		}
		
		} catch (Exception $e) {
			
		
			//$error->add('regerror',$e->getMessage());
			//wc_add_notice( $e->getMessage(), 'error' );
			//echo 'Caught exception: ',  $e->getMessage(), "\n";
			//wp_send_json_error( __( '<strong>Stripe Error : "'.$e->getMessage().'"</strong>' ));
			echo json_encode(array('status'=>400, 'message'=>$e->getMessage()));
			exit;
		}
		
		if($stripeAccountId){		
			if(!function_exists('wp_get_current_user')) {
				include(ABSPATH . "wp-includes/pluggable.php"); 
			}
			
			$user = get_userdata($user_id); 
			$user->set_role( 'driver' );	
			
			update_user_meta( $user_id, 'stripe_account_id', $stripeAccountId);
			update_user_meta( $user_id, 'ddwc_driver_availability', 'on');
			
			update_user_meta( $user_id, 'phone', $driver_phone );
			update_user_meta( $user_id, 'ddwc_driver_license_plate', $license_plate );
			update_user_meta( $user_id, 'ddwc_driver_transportation_type', $trans_type );
			update_user_meta( $user_id, 'ddwc_driver_vehicle_model', $vehical_mdl );
			update_user_meta( $user_id, 'ddwc_driver_vehicle_color', $vehical_clr ); 
			update_user_meta( $user_id, 'ddwc_driver_transportation_city', $driver_city );
			
			 
			update_user_meta( $user_id, 'ddwc_driver_account_number', $account_number );
			update_user_meta( $user_id, 'ddwc_driver_bank_name', $bank_name );
			update_user_meta( $user_id, 'ddwc_driver_account_holder_name', $account_holder_name );
			update_user_meta( $user_id, 'ddwc_driver_account_holder_type', $account_holder_type );
			update_user_meta( $user_id, 'ddwc_driver_ssn_number', $dr_city );
			update_user_meta( $user_id, 'ddwc_driver_ssn_number', $ssn_number );
			update_user_meta( $user_id, 'ddwc_driver_routing_number', $routing_number );  
			
			update_user_meta( $user_id, 'ddwc_driver_dob', $date_ofb );
		
			update_user_meta( $user_id, 'ddwc_driver_licence_front', $l_front[0] );
			update_user_meta( $user_id, 'ddwc_driver_licence_back', $l_back[0] ); 
		
			echo json_encode(array('status'=>200, 'message'=>'Profile updated successfully.'));
			exit;
		
		}

	}
	
}
add_action( 'wp_ajax_update_user_profile', 'update_user_profile' );
add_action( 'wp_ajax_nopriv_update_user_profile', 'update_user_profile' );

/*if (isset($_POST['user_registeration1']))
{
  	
	
	
	
	$test_enable = get_option( 'ddwc_driver_test_enable');
	
	if($test_enable == 'yes'){
		$secret_key = get_option( 'ddwc_driver_test_secret_key');
	}else{
		$secret_key = get_option( 'ddwc_driver_live_secret_key');;
		}
	
	$licence_imgfront   = sanitize_text_field( $post_data['licence_img_front'] );
    $licence_img_back 	= sanitize_text_field( $post_data['licence_img_back'] );
	$l_front =  wp_get_attachment_image_src( $licence_imgfront);
	$l_back = wp_get_attachment_image_src( $licence_img_back);
	
	$account_number   		= sanitize_text_field( $_POST['ddwc_driver_account_number'] );
	$bank_name      		= sanitize_text_field( $_POST['ddwc_driver_bank_name'] );
    $account_holder_name    = sanitize_text_field( $_POST['ddwc_driver_account_holder_name'] );
    $account_holder_type 	= sanitize_text_field( $_POST['ddwc_driver_account_holder_type'] );
	$ssn_number    			= sanitize_text_field( $_POST['ddwc_driver_ssn_number'] );
	$ssn_last4 = substr($ssn_number, -4);
	$routing_number 		= sanitize_text_field( $_POST['ddwc_driver_routing_number'] );
	$date_ofb 				= sanitize_text_field( $_POST['ddwc_driver_dob'] );
	$date  = strtotime($date_ofb);
	$day   = date('d',$date);
	$month = date('m',$date);
	$year  = date('Y',$date);	
		
		
     $user_id = $_POST['driver_id'];
	
	 $trans_type = $_POST['ddwc_driver_transportation_type'];
	 $vehical_mdl = $_POST['ddwc_driver_vehicle_model'];
	 $vehical_clr = $_POST['ddwc_driver_vehicle_color'];
	 $license_plate = $_POST['ddwc_driver_license_plate']; 
	 $driver_phone = $_POST['driver_phone']; 
	 $driver_city = $_POST['ddwc_driver_transportation_city'];
   
	if($user_id){
		
		$user_info = get_userdata( $user_id);
		$dr_email = $user_info->user_email;
		$dr_fname = $user_info->first_name;
		$dr_lname = $user_info->last_name;
	try {	  
		//$full_name = $dr_fname.' '.$dr_lname;  
		$urls = home_url('my-account');
		$stripe = new \Stripe\StripeClient($secret_key);
		$c_date = time();
		$account = $stripe->accounts->create([
		 "type" => "custom",
		 "country" => "US",
		 "email" => $dr_email,
		//"requested_capabilities" => ["card_payments", 'transfers'],
		'capabilities' => [
	   // 'card_payments' => ['requested' => true],
		'transfers' => ['requested' => true],
	  ],
		 "business_profile" => ["name" => "Love IE Local","url" => "https://loveielocal.com/", "mcc"=>'5045'],
		 "business_type" => "individual",
		 "individual" => ["first_name" => $dr_fname, "last_name" => $dr_lname, "dob"=>["day"=>$day,"month"=>$month,"year"=>$year],
		 "ssn_last_4" => $ssn_last4, "id_number" => $ssn_number, "phone" => $driver_phone,"email" => $dr_email,
		 "address" => ["city" => "Fall River", "state" => "MA" , "postal_code" => 02721, "line1" => "374 William S Canning Blvd"],
		  ],
		 "external_account" => ["object"=>"bank_account","currency"=>"usd","country"=>"US","account_number" => $account_number , "routing_number"=>$routing_number, "bank_name" => $bank_name, "account_holder_name" => $account_holder_name, "account_holder_type" => 'individual'],
		 
		  //"external_account" => ["object"=>"bank_account","currency"=>"usd","country"=>"US","account_number" => '000123456789' , "routing_number"=>'110000000', "bank_name" => 'TEST STRIPE BANK', "account_holder_name" => $account_holder_name, "account_holder_type" => 'individual'],
		 
		 "tos_acceptance" => ["date"=>$c_date,"ip"=>$_SERVER['REMOTE_ADDR']]
	]);
		
		
		$stripeAccountId = $account->id;
		
		
		if($stripeAccountId){
			
		$stripeAccountObj = $stripe->accounts->retrieve($stripeAccountId);
			
		
		
		
		} catch (Exception $e) {
			
		
			//$error->add('regerror',$e->getMessage());
			//wc_add_notice( $e->getMessage(), 'error' );
			//echo 'Caught exception: ',  $e->getMessage(), "\n";
			wp_send_json_error( __( '<strong>Stripe Error : "'.$e->getMessage().'"</strong>' ));
			exit;
		}
		
		
		
		if(!function_exists('wp_get_current_user')) {
			include(ABSPATH . "wp-includes/pluggable.php"); 
		}
		
		$user = get_userdata($user_id); 
		$user->set_role( 'driver' );	
	
		update_user_meta( $user_id, 'ddwc_driver_availability', 'on');
		
		update_user_meta( $user_id, 'phone', $driver_phone );
		update_user_meta( $user_id, 'ddwc_driver_license_plate', $license_plate );
		update_user_meta( $user_id, 'ddwc_driver_transportation_type', $trans_type );
		update_user_meta( $user_id, 'ddwc_driver_vehicle_model', $vehical_mdl );
		update_user_meta( $user_id, 'ddwc_driver_vehicle_color', $vehical_clr ); 
		update_user_meta( $user_id, 'ddwc_driver_transportation_city', $driver_city );
	 	
		header("Location: http://driver.ewtlive.in/my-account/driver-dashboard/?update=success");
		
	}else{
		header("Location: http://driver.ewtlive.in/my-account/driver-dashboard/?update=failed");
		}
	
		
		
}*/


add_action('wp_head', 'assign_driver_to_order');
function assign_driver_to_order(){
if(isset($_POST['assign_drto_order'])){
	$driver_id = $_POST['selected_driver'];
	$order_id = $_POST['order_id'];
	global $woocommerce;
	if($order_id && $driver_id){
		$order = wc_get_order($order_id);
		update_post_meta( $order_id, 'ddwc_driver_id', $driver_id );
		update_post_meta( $order_id, 'assignedORaccepted', 'assigned' );
		//$order = new WC_Order( $order_id );
		$order->update_status( 'driver-assigned' );
		$redirect_url = home_url().'/dashboard/orders';
		wp_redirect( $redirect_url );
		//exit; 
		
	}
}
}


add_action( 'wp_ajax_check_otp', 'check_otp' );
add_action( 'wp_ajax_nopriv_check_otp', 'check_otp' );
function check_otp(){
	global $woocommerce;
	$order_id = $_REQUEST['order'];
	$otp = $_REQUEST['otp'];
	$otp_existin = get_post_meta( $order_id, 'otp', true);
 	$order = new WC_Order($order_id);
	$otp_existin = get_post_meta( $order_id, 'otp', true);
	if($otp == $otp_existin )
	{
		$order->update_status('wc-completed');
		echo 'success';
	}else 
	{
		echo 'wrong';
	}
	exit;
}

add_action( 'wp_ajax_adminUpdateNote', 'adminUpdateNote' );
add_action( 'wp_ajax_nopriv_adminUpdateNote', 'adminUpdateNote' );
function adminUpdateNote(){
	global $wpdb;
	
	$message = $_REQUEST['message'];
	$row_id = $_REQUEST['row_id'];
	
	$res = $wpdb->query($wpdb->prepare("UPDATE {$wpdb->prefix}delivery_driver_commision_withdraw SET note='$message' WHERE id=$row_id"));
	
	
	if($res )
	{	
		echo json_encode(array('status'=>1, 'message'=>'Update successfully.',));
	}else 
	{
		echo json_encode(array('status'=>0, 'message'=>'Something went wrong.',));
	}
	exit;
}


function delete_stripe_account_on_delete( $user_id ) {
    global $wpdb;
    $stripe_account_id = get_user_meta($user_id, 'stripe_account_id', true);
	if($stripe_account_id){
		
		try {	
		
			$test_enable = get_option( 'ddwc_driver_test_enable');
		
			if($test_enable == 'yes'){
				$secret_key = get_option( 'ddwc_driver_test_secret_key');
			}else{
				$secret_key = get_option( 'ddwc_driver_live_secret_key');;
			}
			$stripe = new \Stripe\StripeClient($secret_key);
			$delete = $stripe->accounts->delete(
			  $stripe_account_id,
			  []
			);
		} catch (Exception $e) {
			
	
			echo wc_add_notice( $e->getMessage(), 'error' );
			update_user_meta($user_id, 'stripe_account_id', '');
		
			
		}
	}
}
