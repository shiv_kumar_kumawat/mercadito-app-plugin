<?php
/**
 * The Delivery Driver Dashboard Shortcode.
 *
 * @link       https://www.deviodigital.com
 * @since      1.0.0
 *
 * @package    DDWC
 * @subpackage DDWC/admin
 * @author     Devio Digital <contact@deviodigital.com>
 */
function ddwc_dashboard_shortcode() {
	
	
	
		

	// Check if user is logged in.
	if ( is_user_logged_in() ) {
		// Get the user ID.
		$user_id = get_current_user_id();

		// Get the order ID.
		$order_id = filter_input( INPUT_GET, 'orderid' );

		// Get the user object.
		$user_meta = get_userdata( $user_id );

		// If user_id doesn't equal zero.
		if ( 0 != $user_id ) {

			// Get all the user roles as an array.
			$user_roles = $user_meta->roles;

			// Check if the role you're interested in, is present in the array.
			if ( in_array( 'driver', $user_roles, true ) ) {

				if ( isset( $order_id ) && ( '' != $order_id ) ) {
					$driver_id = get_post_meta( $order_id, 'ddwc_driver_id', true );
				}

				// Display order info if ?orderid is set and driver is assigned.
				if ( isset( $order_id ) && ( '' != $order_id ) && ( $driver_id == $user_id ) ) {

					// The store address.
					$store_address   = get_option( 'woocommerce_store_address' );
					$store_address_2 = get_option( 'woocommerce_store_address_2' );
					$store_city      = get_option( 'woocommerce_store_city' );
					$store_postcode  = get_option( 'woocommerce_store_postcode' );

					// The store country/state.
					$store_raw_country = get_option( 'woocommerce_default_country' );

					// Split the store country/state.
					$split_country = explode( ':', $store_raw_country );

					// Check to see if State & Country are available.
					if ( false == strpos( $store_raw_country, ':' ) ) {
						// Store country only.
						$store_country = $split_country[0];
						$store_state   = '';
					} else {
						// Store country and state separated.
						$store_country = $split_country[0];
						$store_state   = $split_country[1];
					}

					// Create store address.
					$store_address = $store_address . ' ' . $store_address_2 . ' ' . $store_city . ' ' . $store_state . ' ' . $store_postcode . ' ' . $store_country;

					// Filter the store address.
					$store_address = apply_filters( 'ddwc_driver_dashboard_store_address', $store_address );

					// Get an instance of the WC_Order object
					$order = wc_get_order( $order_id );

					// Get the order data.
					$order_data = $order->get_data();

					// Specific order data.
					$order_id             = $order_data['id'];
					$order_parent_id      = $order_data['parent_id'];
					$order_status         = $order_data['status'];
					$order_currency       = $order_data['currency'];
					$order_version        = $order_data['version'];
					$order_payment_method = $order_data['payment_method'];
					$order_date_created   = $order_data['date_created']->date( apply_filters( 'ddwc_date_format', get_option( 'date_format' ) ) );
					$order_time_created   = $order_data['date_created']->date( apply_filters( 'ddwc_time_format', get_option( 'time_format' ) ) );
					$order_discount_total = $order_data['discount_total'];
					$order_discount_tax   = $order_data['discount_tax'];
					$order_shipping_total = $order_data['shipping_total'];
					$order_shipping_tax   = $order_data['shipping_tax'];
					$order_cart_tax       = $order_data['cart_tax'];
					$order_total          = $order_data['total'];
					$order_total_tax      = $order_data['total_tax'];
					$order_customer_id    = $order_data['customer_id'];
					$order_customer_note  = $order_data['customer_note'];
					$order_shipping_addr  = $order_data['shipping']['address_1'];
					$order_shipping_fname = $order_data['shipping']['first_name'];
					$order_shipping_lname = $order_data['shipping']['last_name'];
					$order_billing_fname  = $order_data['billing']['first_name'];
					$order_billing_lname  = $order_data['billing']['last_name'];
					$order_billing_phone  = $order_data['billing']['phone'];

					echo '<div class="ddwc-orders">';

					// Display order number.
					if ( isset( $order_id ) ) {
						echo '<h3 class="ddwc">' . esc_attr__( 'Order #', 'ddwc' ) . apply_filters( 'ddwc_order_number', $order_id ) . ' <span class="' . esc_attr( $order_status ) . '">' . wc_get_order_status_name( $order_status ) . '</span></h3>';
					}

					// Display a button to call the customers phone number.
					echo '<p>';

						$phone_customer = '';
						$phone_dispatch = '';

						// Call Customer button.
						if ( isset( $order_billing_phone ) ) {
							$phone_customer = '<a href="tel:' . esc_html( $order_billing_phone ) . '" class="button ddwc-button customer">' . esc_attr__( 'Call Customer', 'ddwc' ) . '</a> ';
						}

						// Call Dispatch button.
						if ( false !== get_option( 'ddwc_settings_dispatch_phone_number' ) && '' !== get_option( 'ddwc_settings_dispatch_phone_number' ) ) {
							$phone_dispatch = '<a href="tel:' . get_option( 'ddwc_settings_dispatch_phone_number' ) . '" class="button ddwc-button dispatch">' . esc_attr__( 'Call Dispatch', 'ddwc' ) . '</a> ';
						}

						// Call buttons.
						$phone_numbers = apply_filters( 'ddwc_driver_dashboard_phone_numbers', $phone_dispatch . $phone_customer );

						echo $phone_numbers;

					echo '</p>';

					echo '<h4>' . esc_attr__( 'Delivery Address', 'ddwc' ) . '</h4>';

					// Plain text delivery address.
					if ( '' == get_option( 'ddwc_settings_google_maps_api_key' ) ) {
						$plain_address = '<p>';
						if ( isset( $order_shipping_addr ) && '' !== $order_shipping_addr ) {
							add_filter( 'woocommerce_order_formatted_shipping_address' , 'ddwc_custom_order_formatted_address' );
							$plain_address   .= $order->get_formatted_shipping_address();
							$delivery_address = str_replace( '<br/>', ' ', $order->get_formatted_shipping_address() );
						} else {
							add_filter( 'woocommerce_order_formatted_billing_address' , 'ddwc_custom_order_formatted_address' );
							$plain_address   .= $order->get_formatted_billing_address();
							$delivery_address = str_replace( '<br/>', ' ', $order->get_formatted_billing_address() );
						}
						$plain_address  .= '</p>';
						$directions_link = 'https://www.google.com/maps/search/?api=1&query=' . $delivery_address;
						$directions_text = esc_attr__( 'Get Directions', 'ddwc' );
						$plain_address  .= '<p><a target="_blank" href="' . apply_filters( 'ddwc_delivery_address_directions_link', $directions_link, $delivery_address ) . '" class="button">' . apply_filters( 'ddwc_delivery_address_directions_text', $directions_text ) . '</a></p>';

						// Display the plain text delivery address.
						echo apply_filters( 'ddwc_delivery_address_plain_text', $plain_address, $delivery_address );
					}

					/**
					 * Display a Google Map with the customers address if an API key is added to
					 * the WooCommerce Settings page.
					 */
					if ( false !== get_option( 'ddwc_settings_google_maps_api_key' ) && '' !== get_option( 'ddwc_settings_google_maps_api_key' ) ) {
						// Use the Shipping address if available.
						if ( isset( $order_shipping_addr ) && '' !== $order_shipping_addr ) {
							add_filter( 'woocommerce_order_formatted_shipping_address' , 'ddwc_custom_order_formatted_address' );
							$delivery_address = str_replace( '<br/>', ' ', $order->get_formatted_shipping_address() );
						} else {
							add_filter( 'woocommerce_order_formatted_billing_address' , 'ddwc_custom_order_formatted_address' );
							$delivery_address = str_replace( '<br/>', ' ', $order->get_formatted_billing_address() );
						}

						// Check if the Google Maps Geocode checkbox has been selected.
						if ( 'yes' == get_option( 'ddwc_settings_google_maps_geocode' ) ) {
							// Google Maps Delivery Address Geocode.
							$delivery_address = ddwc_delivery_address_google_map_geocode( $delivery_address );
						}

						// Map mode, based on WooCommerce settings.
						if ( NULL !== get_option( 'ddwc_settings_google_maps_mode' ) ) {
							$mode = get_option( 'ddwc_settings_google_maps_mode' );
						} else {
							$mode = esc_attr__( 'driving', 'ddwc' );
						}

						// Map mode.
						$map_mode = '&mode=' . apply_filters( 'ddwc_delivery_address_google_map_mode', $mode );

						// Create the Google Map.
						$google_map = '<iframe width="600" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/directions?origin=' . apply_filters( 'ddwc_google_maps_origin_address', $store_address ) . '&destination=' . apply_filters( 'ddwc_google_maps_delivery_address', $delivery_address ) . '&key=' . get_option( 'ddwc_settings_google_maps_api_key' ) . $map_mode . '" allowfullscreen></iframe>';

						// Display the Google Map with delivery address.
						echo apply_filters( 'ddwc_delivery_address_google_map', $google_map, $delivery_address, $store_address );
					}

					echo '<h4>' . esc_attr__( 'Order details', 'ddwc' ) . '</h4>';

					do_action( 'ddwc_driver_dashboard_order_details_table_before' );

					// Get payment gateway details.
					$payment_gateway = wc_get_payment_gateway_by_order( $order_id );

					echo '<table class="ddwc-dashboard">';
					echo '<tbody>';

					do_action( 'ddwc_driver_dashboard_order_details_table_tbody_top' );

					// Display order date.
					if ( isset( $order_date_created ) ) {
						echo '<tr><td><strong>' . esc_attr__( 'Order date', 'ddwc' ) . '</strong></td><td>' . esc_html( $order_date_created ) . ' - ' . esc_html( $order_time_created ) . '</td></tr>';
					}

					// Display payment method details.
					if ( isset( $payment_gateway ) && FALSE !== $payment_gateway ) {
						$payment_method = '<tr><td><strong>' . esc_attr__( 'Payment method', 'ddwc' ) . '</strong></td><td>' . $payment_gateway->title . '</td></tr>';
						echo apply_filters( 'ddwc_driver_dashboard_payment_method', $payment_method );
					}

					// Display customer name.
					if ( '' !== $order_shipping_fname ) {
						echo '<tr><td><strong>' . esc_attr__( 'Customer name', 'ddwc' ) . '</strong></td><td>' . $order_shipping_fname . ' ' . $order_shipping_lname . '</td></tr>';
					} elseif ( '' !== $order_billing_fname ) {
						echo '<tr><td><strong>' . esc_attr__( 'Customer name', 'ddwc' ) . '</strong></td><td>' . $order_billing_fname . ' ' . $order_billing_lname . '</td></tr>';
					} else {
						// Do nothing.
					}
					// Display customer note.
					if ( isset( $order_customer_note ) && '' != $order_customer_note ) {
						echo '<tr><td><strong>' . esc_attr__( 'Customer note', 'ddwc' ) . '</strong></td><td>' . esc_html( $order_customer_note ) . '</td></tr>';
					}

					do_action( 'ddwc_driver_dashboard_order_details_table_tbody_bottom' );

					echo '</tbody>';
					echo '</table>';

					do_action( 'ddwc_driver_dashboard_order_details_table_after' );

					do_action( 'ddwc_driver_dashboard_order_table_before' );

					// Set up total title.
					$total_title = '<td>' . esc_attr__( 'Total', 'ddwc' ) . '</td>';

					echo '<table class="ddwc-dashboard">';
					echo '<thead><tr><td>' . esc_attr__( 'Product', 'ddwc' ) . '</td><td>' . esc_attr__( 'Qty', 'ddwc' ) . '</td>' . apply_filters( 'ddwc_driver_dashboard_total_title', $total_title ) . '</tr></thead>';
					echo '<tbody>';

					do_action( 'ddwc_driver_dashboard_order_table_tbody_top' );

					// get an instance of the WC_Order object.
					$order_items     = wc_get_order( $order_id );
					$currency_code   = $order_items->get_currency();
					$currency_symbol = get_woocommerce_currency_symbol( $currency_code );

					if ( ! empty( $order_items ) ) {
						// The loop to get the order items which are WC_Order_Item_Product objects since WC 3+
						foreach( $order_items->get_items() as $item_id=>$item_product ) {
							//Get the WC_Product object
							$product  = $item_product->get_product();
							// Get the product quantity.
							$quantity = $item_product->get_quantity();
							// Get the product details.
							$name        = $product->get_name();
							$price       = $product->get_price();
							$qtty        = $quantity;
							$qtty_price  = $qtty * $price;
							$price       = '<td>' . $currency_symbol . number_format( $qtty_price, 2 ) . '</td>';
							$total_price = apply_filters( 'ddwc_driver_dashboard_order_item_price', $price );
							/**
							 * @todo add thumbnail image next to the product name.
							 */
							echo '<tr><td>' . $name . '</td><td>' . $qtty . '</td>' . $total_price . '</tr>';
						}
					} else {
						// Do nothing.
					}

					do_action( 'ddwc_driver_dashboard_order_table_tbody_before_delivery' );

					// Delivery Total.
					$delivery_total = '<tr class="delivery-charge"><td colspan="2"><strong>' . esc_attr__( 'Delivery', 'ddwc' ) . '</strong></td><td class="total">' . $currency_symbol . number_format((float)$order_shipping_total, 2, '.', ',' ) . '</td></tr>';

					echo apply_filters( 'ddwc_driver_dashboard_delivery_total', $delivery_total );

					do_action( 'ddwc_driver_dashboard_order_table_tbody_before_total' );

					// Order total.
					$order_total = '<tr class="order-total"><td colspan="2"><strong>' . esc_attr__( 'Order total', 'ddwc' ) . '</strong></td><td class="total">' . $currency_symbol . $order_total . '</td></tr>';

					echo apply_filters( 'ddwc_driver_dashboard_order_total', $order_total );

					do_action( 'ddwc_driver_dashboard_order_table_tbody_bottom' );

					echo '</tbody>';
					echo '</table>';

					do_action( 'ddwc_driver_dashboard_order_table_after' );

					// Change status forms.
					apply_filters( 'ddwc_driver_dashboard_change_status_forms', ddwc_driver_dashboard_change_status_forms() );

					echo '</div>';

				} else {
					
					if(isset($_GET['update'])){
						if($_GET['update'] == 'success'){
							
							echo '<h2 class="message success">Congratulations your profile is update for Driver. </h2>';
							
							}else{
								echo '<h2 class="message failed">Something went wrong! </h2>';
								
								}
						
						}
					
					do_action( 'ddwc_driver_dashboard_top' );
					
					$driver_id = get_current_user_id();
					global $wpdb; 
					$result = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}driver_notification WHERE driver_id =$driver_id");
					$orders = array();
					foreach ($result as $row)
					{
						$orders[] = $row->order_id;
					}
					$order_uniqe = array_unique ( $orders ) ;
					if(get_user_meta($driver_id, 'ddwc_driver_availability', true) == 'on'){
						foreach($order_uniqe as $order_id){	
							global $woocommerce;
							$order = wc_get_order($order_id);
							if($order){
								echo '<span class="driver_ntf">New delivery notification of order #'.$order_id.' <a href="javascript: void(0);" onclick="driver_accept('.$order_id.');" class="accept">Accept</a> <a  class="reject" href="javascript: void(0);" onclick="driver_reject('.$order_id.');">Reject</a></span>';
							}
						}
					}

					/**
					 * Args for Orders with Driver ID attached
					 */
					 
					if (isset($_GET['pageno'])) {
						$paged = $_GET['pageno'];
					} else {
						$paged = 1;
					} 
					 
					if($_GET['entries']){
							$postsPerPage = $_GET['entries'];	
						}else{
							$postsPerPage = 3;
							} 
					if(isset($_GET['status'])){
						$status = $_GET['status'];
					}else{
						$status = 'all';
						}	
					
					$postOffset = ($paged-1) * $postsPerPage; 
					$args = array(
						'post_type'      => 'shop_order',
						'posts_per_page' => $postsPerPage,
						'post_status'    => $status,
						'meta_key'       => 'ddwc_driver_id',
						'meta_value'     => $user_id,
    					'offset'         => $postOffset,
					);
					
					
					$args2 = array(
						'post_type'      => 'shop_order',
						'posts_per_page' => -1,
						'post_status'    => 'all',
						'meta_key'       => 'ddwc_driver_id',
						'meta_value'     => $user_id,
					);
					
					$total_ps = get_posts( $args2 );
					$total_rows = count($total_ps); 
					$total_pages = ceil($total_rows / $postsPerPage);
					/**
					 * Get Orders with Driver ID attached
					 */
					$assigned_orders = get_posts( $args );
					
					//echo count($assigned_orders);
					
					/**
					 * If Orders have Driver ID attached
					 */
					if ( $assigned_orders ) {
						global $wpdb;
						$ruseltsal = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}drivers_earning WHERE driver_id =$user_id");
						
						?>

<div class="total_delivery">
  <!--<div class="del-earning half-bls">
    <div class="del-bl">
      <div class="del-icn"><i class="fa fa-money"></i></div>
      <div class="del-count">
        <p class="ord_count">0</p>
        <p>Delivery Earnings</p>
      </div>
      <div style="clear:both;"></div>
    </div>
  </div>-->
  <div class="del-cmpl half-bls">
    <div class="del-bl">
      <div class="del-icn"><i class="fa fa-truck">&nbsp;</i></div>
      <div class="del-count">
        <p class="ord_count"><?php echo count($ruseltsal); ?></p>
        <p>Delivered</p>
      </div>
      <div style="clear:both;"></div>
    </div>
  </div>
  <div class="del-pending half-bls">
    <div class="del-bl">
      <div class="del-icn pending"><i class="fa fa-truck">&nbsp;</i></div>
      <div class="del-count">
        <p class="ord_count"><?php echo count($total_ps) - count($ruseltsal) ; ?></p>
        <p>Pending</p>
      </div>
      <div style="clear:both;"></div>
    </div>
  </div>
</div>
<div class="filter_dr">
  <?php if(isset($_GET['entries'])){
			$entry = $_GET['entries'];
		}else{
			$entry = '';	
		}
		if(isset($_GET['status'])){
			$status = $_GET['status'];
		}else{
			$status = '';	
		}	
								?>
  <div class="filter_blc">
    <form action="" method="get" class="driver_action">
      <span>Show</span>
      <select name="entries" class="dr_showentry">
        <option value="10" <?php if($entry =='10'){echo 'selected=selected';}?> > 10 </option>
        <option value="15" <?php if($entry =='15'){echo 'selected=selected';}?>> 15 </option>
        <option value="20" <?php if($entry =='20'){echo 'selected=selected';}?>> 20 </option>
        <option value="25" <?php if($entry =='25'){echo 'selected=selected';}?>> 25 </option>
      </select>
      <span>Entries</span>
      <select name="status" class="dr_showentry">
     	<option <?php if($status =='all'){echo 'selected=selected';}?> value="all"> All </option>
        <option <?php if($status =='wc-driver-assigned'){echo 'selected=selected';}?> value="wc-driver-assigned"> Driver Assigned </option>
        <option <?php if($status =='wc-out-for-delivery'){echo 'selected=selected';}?> value="wc-out-for-delivery"> Out for Delivery </option>
        <option <?php if($status =='wc-order-returned'){echo 'selected=selected';}?> value="wc-order-returned"> Order Returned </option>
        <option <?php if($status =='wc-completed'){echo 'selected=selected';}?> value="wc-completed"> Completed </option>
      </select>
      <input type="submit" value="Filter" class="tablink dokan-btn dokan-btn-theme" />
    </form>
  </div>
</div>
<style>
						select.dr_showentry {
							padding: 6px 10px;
							border: 1px solid #a9a9a9;
							border-radius: 3px;
						}
						.filter_blc {
							margin: 8px 0px;
							display: inline-block;
							font-size: 16px;
						} 
						
						
						article.help-content-area.earning_mn button {
    margin-top: 0;
}
ul.earning_filter {
    display: inline-block;
    margin-left: 0;
    padding-left: 0;
}
ul.earning_filter li {
    display: inline-block;
    padding-right: 22px !important;
}

span.dokan-label.dokan-label-earning {
    background: #454545;
    font-size: 14px;
    font-weight: 500;
}
.del-count p {
    margin-bottom: 0;
    font-size: 15px;
}
.total_delivery .half-bls {
    width: 33.3333%;
    float: left;
    margin-bottom: 36px;
    text-align: center;
    margin-top: 10px;
}
.del-bl {
    border: 1px solid #ddd;
    width: 90%;
    margin: 0 auto;
    padding: 0px;
    border-radius: 4px;
    box-shadow: 0px 0px 12px 3px #d8d8d8;
}
.del-icn {
    width: 32%;
    float: left;
    padding: 20px;
    font-size: 26px;
    color: #fff;
    background: green;
    border-radius: 4px 0px 0px 4px;
}
.del-icn.pending {
    background: red;
}
.del-count {
    width: 68%;
    float: left;
    padding: 9px;
}
p.ord_count {
    font-size: 24px;
    color: #066fa0;
    font-weight: 600;
} 
						</style>
<?php
						do_action( 'ddwc_assigned_orders_title_before' );

						echo '<h4 class="ddwc assigned-orders">' . esc_attr__( 'Assigned Orders', 'ddwc' ) . '</h4>';

						do_action( 'ddwc_assigned_orders_table_before' );

						echo '<table class="ddwc-dashboard">';

						// Array for assigned orders table thead.
						$thead = array(
							esc_attr__( 'ID', 'ddwc' ),
							esc_attr__( 'Date', 'ddwc' ),
							esc_attr__( 'Status', 'ddwc' ),
							apply_filters( 'ddwc_driver_dashboard_assigned_orders_total_title', esc_attr__( 'Total', 'ddwc' ) ),
						);

						// Filter the thead array.
						$thead = apply_filters( 'ddwc_driver_dashboard_assigned_orders_order_table_thead', $thead );

						echo '<thead><tr>';
						// Loop through $thead.
						foreach ( $thead as $row ) {
							// Add td to thead.
							echo '<td>' . $row . '</td>';
						}
						echo '</tr></thead>';

						echo '<tbody>';
						foreach ( $assigned_orders as $driver_order ) {

							// Get an instance of the WC_Order object.
							$order = wc_get_order( $driver_order->ID );

							// Get the order data.
							$order_data           = $order->get_data();
							$currency_code        = $order_data['currency'];
							$currency_symbol      = get_woocommerce_currency_symbol( $currency_code );
							$order_id             = $order_data['id'];
							$order_parent_id      = $order_data['parent_id'];
							$order_status         = $order_data['status'];
							$order_currency       = $order_data['currency'];
							$order_version        = $order_data['version'];
							$order_payment_method = $order_data['payment_method'];
							$order_date_created   = $order_data['date_created']->date( apply_filters( 'ddwc_date_format', get_option( 'date_format' ) ) );
							$order_discount_total = $order_data['discount_total'];
							$order_discount_tax   = $order_data['discount_tax'];
							$order_shipping_total = $order_data['shipping_total'];
							$order_shipping_tax   = $order_data['shipping_tax'];
							$order_cart_tax       = $order_data['cart_tax'];
							$order_total          = $order_data['total'];
							$order_total_tax      = $order_data['total_tax'];
							$order_customer_id    = $order_data['customer_id'];

							// Statuses for driver.
							$statuses = array(
								'processing',
								'driver-assigned',
								'out-for-delivery',
								'order-returned',
								'completed'
							);

							// Filter the statuses.
							$statuses = apply_filters( 'ddwc_driver_dashboard_assigned_orders_statuses', $statuses );

							// Add orders to table if order status matches item in $statuses array.
							if ( in_array( $order_status, $statuses ) ) {
								// Order total.
								if ( isset( $order_total ) ) {
									$order_total = $currency_symbol . $order_total;
									$order_total = apply_filters( 'ddwc_driver_dashboard_assigned_orders_total', $order_total, $driver_order->ID );
								} else {
									$order_total = '-';
								}

								// Array for assigned orders table tbody.
								$tbody = array(
									'<a href="' . esc_url( apply_filters( 'ddwc_driver_dashboard_assigned_orders_order_details_url', '?orderid=' . $driver_order->ID, $driver_order->ID ) ) . '">' . esc_html( apply_filters( 'ddwc_order_number', $driver_order->ID ) ) . '</a>',
									$order_date_created,
									wc_get_order_status_name( $order_status ),
									$order_total
								);
		
								// Array for assigned orders table tbody.
								$tbody = apply_filters( 'ddwc_driver_dashboard_assigned_orders_order_table_tbody', $tbody, $order_id );

								echo '<tr>';
								// Loop through $tbody.
								foreach ( $tbody as $row ) {
									echo '<td>' . $row . '</td>';
								}
								echo '<tr>';
							}
						}
						echo '</tbody>';
						echo '</table>';
						
						
						
				echo '<div class="pagination-wrap">';
					
					
					$myaccount_page = get_option( 'woocommerce_myaccount_page_id' );
					if ( $myaccount_page ) {
					  $myaccount_page_url = get_permalink( $myaccount_page ).'driver-dashboard/';
					  }
					
					$pagenum      = isset( $_GET['pageno'] ) ? absint( $_GET['pageno'] ) : 1;
                    $base_url     = $myaccount_page_url;

                    if ( $total_pages > 1 ) {
                        echo '<div class="pagination-wrap">';
                        $page_links = paginate_links( array(
                            'current'   => $pagenum,
                            'total'     => $total_pages,
                            'base'      => $base_url. '%_%',
                            'format'    => '?pageno=%#%',
                            'add_args'  => false,
                            'type'      => 'array',
                            'prev_text' => __( '&laquo; Previous', 'dokan-lite' ),
                            'next_text' => __( 'Next &raquo;', 'dokan-lite' )
                        ) );

                        echo '<ul class="pagination"><li>';
                        echo join("</li>\n\t<li>", $page_links ); // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped
                        echo "</li>\n</ul>\n";
                        echo '</div>';
                    }
			
   
echo '</div>';		
						

						//do_action( 'ddwc_assigned_orders_table_after' );
//
//						echo '<h4 class="ddwc assigned-orders">' . esc_attr__( 'Completed Orders', 'ddwc' ) . '</h4>';
//
//						do_action( 'ddwc_completed_orders_table_before' );
//
//						$total_title = '<td>' . esc_attr__( 'Total', 'ddwc' ) . '</td>';
//
//						echo '<table class="ddwc-dashboard">';
//						echo '<thead><tr><td>' . esc_attr__( 'ID', 'ddwc' ) . '</td><td>' . esc_attr__( 'Date', 'ddwc' ) . '</td><td>' . esc_attr__( 'Status', 'ddwc' ) . '</td>' . apply_filters( 'ddwc_driver_dashboard_completed_orders_total_title', $total_title ) . '</tr></thead>';
//						echo do_action( 'ddwc_driver_dashboard_completed_orders_before_tbody' );
//						echo '<tbody>';
//						echo do_action( 'ddwc_driver_dashboard_completed_orders_tbody_top' );
//						foreach ( $assigned_orders as $driver_order ) {
//
//							// Get an instance of the WC_Order object.
//							$order = wc_get_order( $driver_order->ID );
//
//							// Get order data.
//							$order_data           = $order->get_data();
//							$currency_code        = $order_data['currency'];
//							$currency_symbol      = get_woocommerce_currency_symbol( $currency_code );
//							$order_id             = $order_data['id'];
//							$order_parent_id      = $order_data['parent_id'];
//							$order_status         = $order_data['status'];
//							$order_currency       = $order_data['currency'];
//							$order_version        = $order_data['version'];
//							$order_payment_method = $order_data['payment_method'];
//							$order_date_created   = $order_data['date_created']->date( apply_filters( 'ddwc_date_format', get_option('date_format') ) );
//							$order_discount_total = $order_data['discount_total'];
//							$order_discount_tax   = $order_data['discount_tax'];
//							$order_shipping_total = $order_data['shipping_total'];
//							$order_shipping_tax   = $order_data['shipping_tax'];
//							$order_cart_tax       = $order_data['cart_tax'];
//							$order_total          = $order_data['total'];
//							$order_total_tax      = $order_data['total_tax'];
//							$order_customer_id    = $order_data['customer_id'];
//							//echo $order_status;
//							if ( 'completed' === $order_status || 'order-returned' === $order_status ) {
//								echo '<tr>';
//								echo '<td><a href="?orderid=' . $driver_order->ID . '">' . apply_filters( 'ddwc_order_number', $driver_order->ID ) . '</a></td>';
//								echo '<td>' . $order_date_created . '</td>';
//								echo '<td>' . wc_get_order_status_name( $order_status ) . '</td>';
//
//								if ( isset( $order_total ) ) {
//									$order_total = '<td>'  . $currency_symbol . $order_total . '</td>';
//									echo apply_filters( 'ddwc_driver_dashboard_completed_orders_total', $order_total, $driver_order->ID );
//								} else {
//									echo '<td>-</td>';
//								}
//
//								echo '</tr>';
//							} else {
//								// Do nothing.
//							}
//						}
//						echo do_action( 'ddwc_driver_dashboard_completed_orders_tbody_bottom' );
//						echo '</tbody>';
//						echo do_action( 'ddwc_driver_dashboard_completed_orders_after_tbody' );
//						echo '</table>';
//
//						do_action( 'ddwc_completed_orders_table_after' );

					} else {

						do_action( 'ddwc_assigned_orders_empty_before' );

						// Message - No assigned orders.
						$empty  = '<h4 class="ddwc assigned-orders">' . esc_attr__( 'Assigned Orders', 'ddwc' ) . '</h4>';
						$empty .= '<p>' . esc_attr__( 'You do not have any assigned orders.', 'ddwc' ) . '</p>';

						echo apply_filters( 'ddwc_assigned_orders_empty', $empty );

						do_action( 'ddwc_assigned_orders_empty_after' );
					}

					do_action( 'ddwc_driver_dashboard_bottom' );

				}
			} elseif ( ddwc_check_user_roles( array( 'administrator' ) ) ) {

				do_action( 'ddwc_admin_orders_form_before' );

				/**
				 * Args for Orders with Driver ID attached
				 */
				$args = array(
					'orderby'        => 'date',
					'post_type'      => 'shop_order',
					'posts_per_page' => -1,
					'post_status'    => 'any'
				);
				?>
<h3>
  <?php _e( 'Delivery Orders', 'ddwc' ); ?>
</h3>
<form class="ddwc-order-filters" method="post" action="<?php $_SERVER['REQUEST_URI']; ?>">
  <div class="form-group">
    <label>
      <?php _e( 'From', 'ddwc' ); ?>
    </label>
    <input type="date" name="filter-from" value="<?php if ( ! empty( $_POST['filter-from'] ) ) { echo $_POST['filter-from']; } else { echo date( 'Y-m-d', strtotime( '-7 days' ) ); } ?>" />
  </div>
  <div class="form-group">
    <label>
      <?php _e( 'To', 'ddwc' ); ?>
    </label>
    <input type="date" name="filter-to" value="<?php if ( ! empty( $_POST['filter-to'] ) ) { echo $_POST['filter-to']; } else { echo date( 'Y-m-d' );  } ?>" />
  </div>
  <div class="form-group">
    <label>
      <?php _e( 'Driver', 'ddwc' ); ?>
    </label>
    <select name="filter-name">
      <option value=""></option>
      <?php
								$user_query = new WP_User_Query( array( 'role' => 'driver' ) );
								if ( ! empty( $user_query->get_results() ) ) {
									foreach ( $user_query->get_results() as $user ) {
										if ( isset( $_POST['filter-name'] ) && $user->ID == $_POST['filter-name'] ) {
											$selected = 'selected';
										} else {
											$selected = '';
										}
										echo '<option ' . $selected . ' value="' . $user->ID . '">' . $user->display_name . '</option>';
									}
								}
							?>
    </select>
  </div>
  <div class="form-group">
    <input type="submit" value="<?php _e( 'SUBMIT', 'ddwc' ); ?>" />
  </div>
</form>
<?php
				do_action( 'ddwc_admin_orders_form_after' );

				// 	Filter variables.
				if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
					// Check if the filter-from field is set.
					if ( isset( $_POST['filter-from'] ) ) {
						// Form field - from date.
						$from_time  = date( 'Y-m-d', strtotime( $_POST['filter-from'] ) );
						$from_year  = date( 'Y', strtotime( $from_time ) );
						$from_month = date( 'n', strtotime( $from_time ) );
						$from_day   = date( 'j', strtotime( $from_time ) );
					}
					// Check if the filter-to field is set.
					if ( isset( $_POST['filter-to'] ) ) {
						// Form field - to date.
						$to_time  = date( 'Y-m-d', strtotime( $_POST['filter-to'] ) );
						$to_year  = date( 'Y', strtotime( $to_time ) );
						$to_month = date( 'n', strtotime( $to_time ) );
						$to_day   = date( 'j', strtotime( $to_time ) );
					}
				} else {
					// From time (7 days ago).
					$from_time  = date( 'Y-m-d', strtotime( '-7 days' ) );
					$from_year  = date( 'Y', strtotime( $from_time ) );
					$from_month = date( 'n', strtotime( $from_time ) );
					$from_day   = date( 'j', strtotime( $from_time ) );
					// To time (now).
					$to_time  = date( 'Y-m-d' );
					$to_year  = date( 'Y', strtotime( $to_time ) );
					$to_month = date( 'n', strtotime( $to_time ) );
					$to_day   = date( 'j', strtotime( $to_time ) );
				}

				// Update the args.
				$args['date_query'] = array(
					array(
						'after'     => array(
							'year'  => $from_year,
							'month' => $from_month,
							'day'   => $from_day,
						),
						'before'    => array(
							'year'  => $to_year,
							'month' => $to_month,
							'day'   => $to_day,
						),
						'inclusive' => true,
					),
				);

				// Check if the form filter-name is set.
				if ( isset( $_POST['filter-name'] ) ) {
					// Set the Driver ID key name.
					$args['meta_key']   = 'ddwc_driver_id';
					// Set the Driver ID value.
					$args['meta_value']	= $_POST['filter-name'];
				}

				/**
				 * Get Delivery Orders
				 */
				$driver_orders = get_posts( $args );

				do_action( 'ddwc_admin_orders_table_before' );

				echo '<table class="ddwc-dashboard admin-orders">';

				// Array for admin orders table thead.
				$thead = array(
					esc_attr__( 'ID', 'ddwc' ),
					esc_attr__( 'Date', 'ddwc' ),
					esc_attr__( 'Location', 'ddwc' ),
					esc_attr__( 'Status', 'ddwc' ),
					apply_filters( 'ddwc_driver_dashboard_admin_orders_total_title', esc_attr__( 'Total', 'ddwc' ) ),
				);

				// Filter the thead array.
				$thead = apply_filters( 'ddwc_driver_dashboard_admin_orders_order_table_thead', $thead );

				echo '<thead><tr>';
				// Loop through $thead.
				foreach ( $thead as $row ) {
					// Add td to thead.
					echo '<td>' . $row . '</td>';
				}
				echo '</tr></thead>';

				echo '<tbody>';
				foreach ( $driver_orders as $driver_order ) {

					// Get an instance of the WC_Order object.
					$order = wc_get_order( $driver_order->ID );

					// Get the order data.
					$order_data           = $order->get_data();
					$currency_code        = $order_data['currency'];
					$currency_symbol      = get_woocommerce_currency_symbol( $currency_code );
					$order_id             = $order_data['id'];
					$order_parent_id      = $order_data['parent_id'];
					$order_status         = $order_data['status'];
					$order_currency       = $order_data['currency'];
					$order_version        = $order_data['version'];
					$order_payment_method = $order_data['payment_method'];
					$order_date_created   = $order_data['date_created']->date( apply_filters( 'ddwc_date_format', get_option( 'date_format' ) ) );
					$order_discount_total = $order_data['discount_total'];
					$order_discount_tax   = $order_data['discount_tax'];
					$order_shipping_total = $order_data['shipping_total'];
					$order_shipping_tax   = $order_data['shipping_tax'];
					$order_cart_tax       = $order_data['cart_tax'];
					$order_total          = $order_data['total'];
					$order_total_tax      = $order_data['total_tax'];
					$order_customer_id    = $order_data['customer_id'];

					## BILLING INFORMATION:

					$order_billing_city     = $order_data['billing']['city'];
					$order_billing_state    = $order_data['billing']['state'];
					$order_billing_postcode = $order_data['billing']['postcode'];

					## SHIPPING INFORMATION:

					$order_shipping_city     = $order_data['shipping']['city'];
					$order_shipping_state    = $order_data['shipping']['state'];
					$order_shipping_postcode = $order_data['shipping']['postcode'];

					// Create address to use in the table.
					$address = $order_billing_city . ' ' . $order_billing_state . ', ' . $order_billing_postcode;

					// Set address to shipping (if available).
					if ( isset( $order_shipping_city ) ) {
						$address = $order_shipping_city . ' ' . $order_shipping_state . ', ' . $order_shipping_postcode;
					}

					// Order total.
					if ( isset( $order_total ) ) {
						$order_total = $currency_symbol . $order_total;
						$order_total = apply_filters( 'ddwc_driver_dashboard_admin_orders_total', $order_total, $driver_order->ID );
					} else {
						$order_total = '-';
					}

					// Array for admin orders table tbody.
					$tbody = array(
						'<a href="' . esc_url( apply_filters( 'ddwc_driver_dashboard_admin_orders_order_details_url', admin_url( 'post.php?post=' . $driver_order->ID . '&action=edit' ), $driver_order->ID ) ) . '">' . esc_html( apply_filters( 'ddwc_order_number', $driver_order->ID ) ) . '</a>',
						$order_date_created,
						apply_filters( 'ddwc_driver_dashboard_admin_orders_orders_table_address', $address ),
						wc_get_order_status_name( $order_status ),
						$order_total
					);

					// Array for admin orders table tbody.
					$tbody = apply_filters( 'ddwc_driver_dashboard_admin_orders_order_table_tbody', $tbody, $order_id );

					// Statuses for driver.
					$statuses = array(
						'driver-assigned',
						'out-for-delivery',
						'order-returned'
					);

					// Filter the statuses.
					$statuses = apply_filters( 'ddwc_driver_dashboard_orders_table_statuses', $statuses );

					// Add orders to table if order status matches item in $statuses array.
					if ( in_array( $order_status, $statuses ) ) {
						echo '<tr>';
						// Loop through $tbody.
						foreach ( $tbody as $row ) {
							echo '<td>' . $row . '</td>';
						}
						echo '<tr>';
					}
				}
				echo '</tbody>';
				echo '</table>';

				do_action( 'ddwc_admin_orders_table_after' );

				// Add the Delivery Drivers table.
				ddwc_driver_dashboard_admin_drivers_table();

			} else {
				
				$profileuser = wp_get_current_user();
				
				
    // Get driver picture.
    $ddwc_driver_picture = get_user_meta( $profileuser->ID, 'ddwc_driver_picture', true );
    // Get user data.
    $user = get_userdata( $profileuser->ID );
	
   
    // If the user is a DRIVER, display the driver fields.
  //  if ( in_array( 'driver', (array) $user->roles ) ) {
    ?>
<h2 class="reg_title">
  <?php esc_html_e( 'Become a Driver', 'ddwc' ); ?>
</h2>
<div class="dokan-ajax-response"></div>
<!-- <form action="" method="post"  id="dr_register" class="dr_register">-->
<form method="post" id="settings-form1" action="" class="dokan-form-horizontal dr_register" autocomplete="off" novalidate="validate">
  <input type="hidden" name="driver_id" id="driver_id" value="<?php echo $profileuser->ID; ?>">
  <label> <span>
    <?php esc_html_e( 'Driver Phone', 'ddwc' ); ?>
    </span>
    <?php $phone = get_user_meta( $user->ID, 'phone', TRUE );?>
    <input type="text" name="driver_phone" class="driver_phone" id="driver_phone" value="<?php echo $phone; ?>" required>
  </label>
  <label> <span>
    <?php esc_html_e( 'Date of Birth', 'ddwc' ); ?>
    </span>
    <?php $ddwc_driver_dob = get_user_meta( $user->ID, 'ddwc_driver_dob', TRUE );?>
    <input type="date" name="ddwc_driver_dob" class="ddwc_driver_dob"  id="ddwc_driver_dob" value="<?php echo $ddwc_driver_dob; ?>">
  </label>
  <label> <span>
    <?php esc_html_e( 'Transportation Type', 'ddwc' ); ?>
    </span>
    <?php
                // Transportation types.
                $transportation_types = apply_filters( 'ddwc_woocommerce_edit_account_transportation_types', array( esc_attr__( 'Bicycle', 'ddwc' ), esc_attr__( 'Tricycle', 'ddwc' ), esc_attr__( 'Motorcycle', 'ddwc' ), esc_attr__( 'Car', 'ddwc' ),esc_attr__( 'Van/Minivan', 'ddwc' ), esc_attr__( 'SUV', 'ddwc' ), esc_attr__( 'Truck', 'ddwc' ) ) );
                // Loop through types.
                if ( $transportation_types ) {
                    printf( '<select name="ddwc_driver_transportation_type" id="ddwc_driver_transportation_type">', get_user_meta( $user->ID, 'ddwc_driver_transportation_type', TRUE ) );
                    echo '<option value="">--</option>';
                    foreach ( $transportation_types as $type ) {
                        if ( $type != get_user_meta( $user->ID, 'ddwc_driver_transportation_type', TRUE ) ) {
                            $selected = '';
                        } else {
                            $selected = 'selected="selected"';
                        }
                        printf( '<option value="%s" ' . esc_html( $selected ) . '>%s</option>', esc_html( $type ), esc_html( $type ) );
                    }
                    print( '</select>' );
                }
            ?>
  </label>
  <label> <span>
    <?php esc_html_e( 'Delivery Cities', 'ddwc' ); ?>
    </span>
    <?php
                
				$cities_list = get_option( 'ddwc_driver_citys_list' );
				$citylist_array = explode(', ', $cities_list);
				$exist_ct = get_user_meta( $user->ID, 'ddwc_driver_transportation_city', true );
                // Loop through types.
                if ( $citylist_array ) {
                    printf( '<select name="ddwc_driver_transportation_city[]" id="ddwc_driver_transportation_city" multiple>');
                   // echo '<option value="">--</option>';
                    foreach ( $citylist_array as $key => $val ) {
                        if ( in_array($val, $exist_ct) ) {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        printf( '<option value="%s" '.$selected.'>%s</option>', esc_html( $val ), esc_html( $val ) );
                    }
                    print( '</select>' );
                }
            ?>
  </label>
  <label><span>
    <?php
                    if ( '' != get_user_meta( $user->ID, 'ddwc_driver_transportation_type', TRUE ) ) {
                        echo get_user_meta( $user->ID, 'ddwc_driver_transportation_type', TRUE ) . ' Model';
                    } else {
                        esc_html_e( 'Vehicle Model', 'ddwc' );
                    }
                ?>
    </span>
    <input class="regular-text" type="text" name="ddwc_driver_vehicle_model"  id="ddwc_driver_vehicle_model" value="<?php echo esc_html( get_user_meta( $profileuser->ID, 'ddwc_driver_vehicle_model', true ) ); ?>" />
  </label>
  <label><span>
    <?php
                    if ( '' != get_user_meta( $user->ID, 'ddwc_driver_transportation_type', TRUE ) ) {
                        echo get_user_meta( $user->ID, 'ddwc_driver_transportation_type', TRUE ) . ' Color';
                    } else {
                        esc_html_e( 'Vehicle Color', 'ddwc' );
                    }
                ?>
    </span>
    <input class="regular-text" type="text" name="ddwc_driver_vehicle_color" id="ddwc_driver_vehicle_color" value="<?php echo esc_html( get_user_meta( $profileuser->ID, 'ddwc_driver_vehicle_color', true ) ); ?>" />
  </label>
  <?php if ( 'Bicycle' != get_user_meta( $user->ID, 'ddwc_driver_transportation_type', TRUE ) ) { ?>
  <label><span>
    <?php esc_html_e( 'License Plate Number', 'ddwc' ); ?>
    </span>
    <input class="regular-text" type="text" name="ddwc_driver_license_plate" value="<?php echo esc_html( get_user_meta( $profileuser->ID, 'ddwc_driver_license_plate', true ) ); ?>" />
  </label>
  <?php } ?>
  <div id="postimagediv" class="postbox" style="float:left;width:100%">
    <h4 class="dr_shippingoption" ><span>Driver license Images</span></h4>
    <?php 
			  	
		$front_img = get_user_meta( $user->ID, 'ddwc_driver_licence_front', true );
		$back_img = get_user_meta( $user->ID, 'ddwc_driver_licence_back', true );
			  ?>
    <label>
    <span>
    <?php esc_html_e( 'Driver license Front', 'ddwc' ); ?>
    </span>
    <?php $ddwc_driver_account_number = get_user_meta( $user->ID, 'ddwc_driver_account_number', TRUE );?>
    <div class="inside" style="float:right;width:66%">
      <p class="hide-if-no-js"><a style="float:left;cursor:pointer" id="image_url_btn">Upload image<img style="width: 100px; float:left" width="100" height="75" src="<?php echo $front_img; ?>" id="productimage"></a></p>
      <input type="hidden" name="licence_img_front" id="attach_id" placeholder="Item Image">
    </div>
    </label>
    <label style="margin-top:20px;">
    <span>
    <?php esc_html_e( 'Driver license Back', 'ddwc' ); ?>
    </span>
    <?php $ddwc_driver_account_number = get_user_meta( $user->ID, 'ddwc_driver_account_number', TRUE );?>
    <div class="inside" style="float:right;width:66%">
      <p class="hide-if-no-js"><a style="float:left;cursor:pointer" id="image_url_btn_back">Upload image<img style="width: 100px; float:left" width="100" height="75" src="<?php echo $back_img; ?>" id="productimage_back"></a></p>
      <input type="hidden" name="licence_img_back" id="attach_id_back" placeholder="Item Image">
    </div>
    </label>
  </div>
  <div style="clear:both;"></div>
  <?php
   // }

				$stripe_account_id = get_user_meta($user->ID, 'stripe_account_id', true);
				if(empty($stripe_account_id)){
					
					?>
  <h4 class="dr_shippingoption" style="text-align:left;">Enter Bank Details For Stripe Account</h4>
  <label> <span>
    <?php esc_html_e( 'Account Number', 'ddwc' ); ?>
    </span>
    <?php $ddwc_driver_account_number = get_user_meta( $user->ID, 'ddwc_driver_account_number', TRUE );?>
    <input type="text" name="ddwc_driver_account_number" class="ddwc_driver_account_number" id="ddwc_driver_account_number" value="<?php echo $ddwc_driver_account_number; ?>">
  </label>
  <label> <span>
    <?php esc_html_e( 'Bank Name', 'ddwc' ); ?>
    </span>
    <?php $ddwc_driver_bank_name = get_user_meta( $user->ID, 'ddwc_driver_bank_name', TRUE );?>
    <input type="text" name="ddwc_driver_bank_name" class="ddwc_driver_bank_name" id="ddwc_driver_bank_name" value="<?php echo $ddwc_driver_bank_name; ?>">
  </label>
  <label> <span>
    <?php esc_html_e( 'Account Holder Name', 'ddwc' ); ?>
    </span>
    <?php $ddwc_driver_account_holder_name = get_user_meta( $user->ID, 'ddwc_driver_account_holder_name', TRUE );?>
    <input type="text" name="ddwc_driver_account_holder_name" class="ddwc_driver_account_holder_name" id="ddwc_driver_account_holder_name" value="<?php echo $ddwc_driver_account_holder_name; ?>">
  </label>
  <label> <span>
    <?php esc_html_e( 'Social Security Number', 'ddwc' ); ?>
    </span>
    <?php $ddwc_driver_ssn_number = get_user_meta( $user->ID, 'ddwc_driver_ssn_number', TRUE );?>
    <input type="text" name="ddwc_driver_ssn_number" class="ddwc_driver_ssn_number" id="ddwc_driver_ssn_number" value="<?php echo $ddwc_driver_ssn_number; ?>">
  </label>
  <label> <span>
    <?php esc_html_e( 'Routing Number', 'ddwc' ); ?>
    </span>
    <?php $ddwc_driver_routing_number = get_user_meta( $user->ID, 'ddwc_driver_routing_number', TRUE );?>
    <input type="text" name="ddwc_driver_routing_number" class="ddwc_driver_routing_number"  id="ddwc_driver_routing_number" value="<?php echo $ddwc_driver_routing_number; ?>">
  </label>
  <?php
				}else{
						
				}
				
				?>
  <input type="submit" name="user_registeration" id="driver_rgstr" value="Become a Driver" onclick="validateForm(this)"/>
</form>
<?php
				// Set the Access Denied page text.
				//$access_denied = '<h3 class="ddwc access-denied">' . esc_attr__( 'Access Denied', 'ddwc' ) . '</h3><p>' . esc_attr__( 'Sorry, but you are not able to view this page.', 'ddwc' ) . '</p>';

				// Return the Access Denied text, filtered.
				//return apply_filters( 'ddwc_access_denied', $access_denied );
			}

		} else {
			// Do nothing.
		}
	} else {
		// Display login form.
		apply_filters( 'ddwc_dashboard_login_form', wp_login_form() );
	}
}
add_shortcode( 'ddwc_dashboard', 'ddwc_dashboard_shortcode' );

