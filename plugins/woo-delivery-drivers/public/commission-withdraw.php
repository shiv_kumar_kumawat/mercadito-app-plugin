<?php

if(!function_exists('wp_get_current_user')) {
	include(ABSPATH . "wp-includes/pluggable.php"); 
}
$user = wp_get_current_user();
$roles = ( array ) $user->roles;

if((!in_array($roles[0], array('driver', 'seller')))){
	return;
}


// ------------------
// 1. Register new endpoint to use for My Account page
// Note: Resave Permalinks or it will give 404 error
  
function bbloomer_add_commission_withdraw_endpoint() {
	global $wp_rewrite;
    add_rewrite_endpoint( 'commission-withdraw', EP_ROOT | EP_PAGES );
	$wp_rewrite->flush_rules();
}
  
add_action( 'init', 'bbloomer_add_commission_withdraw_endpoint' );
  
  
// ------------------
// 2. Add new query var
  
function bbloomer_commission_withdraw_query_vars( $vars ) {
    $vars[] = 'commission-withdraw';
    return $vars;
}
  
add_filter( 'query_vars', 'bbloomer_commission_withdraw_query_vars', 0 );
  
  
// ------------------
// 3. Insert the new endpoint into the My Account menu
  
function bbloomer_add_commission_withdraw_link_my_account( $items ) {
    $items['commission-withdraw'] = 'Commission Withdraw';
    return $items;
}
  
add_filter( 'woocommerce_account_menu_items', 'bbloomer_add_commission_withdraw_link_my_account' );
  
  
// ------------------
// 4. Add content to the new endpoint
  
function bbloomer_commission_withdraw_content() {
	?>
    
    <div class="dokan-dashboard-content dokan-withdraw-content">
  <article class="dokan-withdraw-area">
    <header class="dokan-dashboard-header" style="position: relative;">
      <h1 class="entry-title">Withdraw</h1>
      
      <?php 
	  $user = wp_get_current_user();
	  $roles = ( array ) $user->roles;

		if((in_array($roles[0], array('seller')))){
			
				echo '<a href="'.home_url('dashboard').'" class="tablink dokan-btn dokan-btn-theme" style="position: absolute; right: 0; top: 0;"> Back to Vendor Dashboard</a>'; 
			
		}
	  ?>
      
    </header>
    <!-- .entry-header -->
    
    
    <?php 
	global $wpdb;
	global  $woocommerce;
	$driver_id = get_current_user_id();
	//$today = date("Y-m-d H:i:s");
	$on_date = date("Y-m-d H:i:s");
	$result = $wpdb->get_row( $wpdb->prepare(
                        "SELECT SUM(debit) as earnings,
                        ( SELECT SUM(credit) FROM {$wpdb->prefix}delivery_driver_commision WHERE driver_id = %d AND DATE(created) <= %s ) as withdraw
                        from {$wpdb->prefix}delivery_driver_commision
                        WHERE driver_id = %d AND DATE(created) <= %s",
                    $driver_id, $on_date, $driver_id, $on_date ) );	  
	$current_bl = $result->earnings - $result->withdraw;
	
	$earning = number_format((float)$current_bl, 2, '.', '');
	
	$pending = $wpdb->get_row( $wpdb->prepare("SELECT id FROM {$wpdb->prefix}delivery_driver_commision_withdraw WHERE driver_id = $driver_id AND status = 0") );	  

   	// echo get_woocommerce_currency_symbol();
	// $formeted_er = get_woocommerce_currency_symbol().' '.$earning;
	

	
	?>
    
    
    <div class="entry-content">
      <div class="dokan-alert dokan-alert-warning"> <strong>Current Balance: <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol();?></span><?php echo $earning; ?></span> <br>
        Minimum Withdraw amount: <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol();?></span>10.00</span> </strong> </div>
      <ul class="list-inline subsubsub">
        <li class="<?php if(empty($_GET['type'])){echo 'active';}?>"> <a href="<?php echo home_url('mi-cuenta/commission-withdraw');?>">Withdraw Request</a> </li>
        <li class="<?php if($_GET['type']=='approved'){echo 'active';}?>"> <a href="<?php echo home_url('mi-cuenta/commission-withdraw');?>?type=approved">Approved Requests</a> </li>
        <li class="<?php if($_GET['type']=='cancelled'){echo 'active';}?>"> <a href="<?php echo home_url('mi-cuenta/commission-withdraw');?>?type=cancelled">Cancelled Requests</a> </li>
      </ul>
      
      <?php 
	  if($_GET['type'] == 'approved'){
		  $driver_id = get_current_user_id();
		  $approve_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}delivery_driver_commision_withdraw WHERE driver_id =$driver_id AND status = 1");
		  
		  if($approve_data){
		  
		  ?>
		   <table class="dokan-table dokan-table-striped">
        <tbody>
          <tr>
            <th>Amount</th>
            <th>Date</th>
            <th>Status</th>
          </tr>
        
        <?php foreach($approve_data as $approve) {?>  
          <tr>
            <td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol();?></span><?php echo number_format((float)$approve->amount, 2, '.', ''); ?></span></td>
            <td><?php echo $approve->date ; ?></td>
            <td>
            <?php $status = $approve->status; 
			if($status == 0){
				echo '<span class="label label-danger">Pending Review</span></td>';
				}
			else if($status == 2){
				echo '<span class="label label-danger">Cancel</span></td>';
				}
			else{
				echo '<span class="label label-green">Approved</span></td>';
				}		
			
			?>
            
            
            
          </tr>
        <?php } ?>
        
        </tbody>
      </table>
		  <?php
		  }else{
			  ?>
              <div class="dokan-alert dokan-alert-warning">
        		<strong>Sorry, no transactions were found!</strong>
			</div>
			  <?php
			  }
		  }else if($_GET['type'] == 'cancelled'){
		  $driver_id = get_current_user_id();
		  $approve_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}delivery_driver_commision_withdraw WHERE driver_id =$driver_id AND status = 2");
		  
		  if($approve_data){
		  
		  ?>
		   <table class="dokan-table dokan-table-striped">
        <tbody>
          <tr>
            <th>Amount</th>
            <th>Date</th>
            <th>Status</th>
          </tr>
        
        <?php foreach($approve_data as $approve) {?>  
          <tr>
            <td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol();?></span><?php echo number_format((float)$approve->amount, 2, '.', ''); ?></span></td>
            <td><?php echo $approve->date ; ?></td>
            <td>
            <?php $status = $approve->status; 
			if($status == 0){
				echo '<span class="label label-danger">Pending Review</span></td>';
				}
			else if($status == 2){
				echo '<span class="label label-danger">Cancel</span></td>';
				}
			else{
				echo '<span class="label label-green">Approved</span></td>';
				}		
			
			?>
            
            
            
          </tr>
        <?php } ?>
        
        </tbody>
      </table>
		  <?php
		  }else{
			  ?>
              <div class="dokan-alert dokan-alert-warning">
        		<strong>Sorry, no transactions were found!</strong>
			</div>
			  <?php
			  }
		  }else{
	   ?>
      
      
      
      <?php if($pending){
		  $pending_id = $pending->id;
		  $pending_data =  $wpdb->get_results("SELECT * FROM {$wpdb->prefix}delivery_driver_commision_withdraw WHERE id =$pending_id");
		/* echo '<pre>';
		 print_r($pending_data);
		 echo '</pre>'; */
		  
		  ?>
     	
        <?php if($_GET['status'] == 'success'){?>
        
        <div class="dokan-alert dokan-alert-success">
            <button type="button" class="dokan-close" data-dismiss="alert">×</button>
        <strong>Your request has been received successfully and being reviewed!</strong>
</div>
<?php }else {?>
        
      <div class="dokan-alert dokan-alert-danger"> <strong>
        <p>You already have pending withdraw request(s).</p>
        <p>Please submit your request after approval or cancellation of your previous request.</p>
        </strong> </div>
  <?php }?>      
        
        
      <table class="dokan-table dokan-table-striped">
        <tbody>
          <tr>
            <th>Amount</th>
            <th>Date</th>
            <th>Cancel</th>
            <th>Status</th>
          </tr>
        
        <?php foreach($pending_data as $pending) {?>  
          <tr>
            <td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol();?></span><?php echo number_format((float)$pending->amount, 2, '.', ''); ?></span></td>
            <td><?php echo $pending->date ; ?></td>
            <td><a href="javascript:void(0);" data-id="<?php echo $pending->id; ?>" onclick="remove_widthdrow_req(<?php echo $pending->id; ?>);"> Cancel </a></td>
            <td>
            <?php $status = $pending->status; 
			if($status == 0){
				echo '<span class="label label-danger">Pending Review</span></td>';
				}
			else if($status == 2){
				echo '<span class="label label-danger">Cancel</span></td>';
				}
			else{
				echo '<span class="label label-green">Approved</span></td>';
				}		
			
			?>
            
            
            
          </tr>
        <?php } ?>
        
        </tbody>
      </table>
      <?php }else{ ?>
     		
            
           
            
       <?php 
	   if($_GET['status'] == 'invalid_amount'){
			?>
			 <div class="dokan-alert dokan-alert-danger"> <strong>
        <p>Your enterd amount is not correct. Please add a valid amount.</p>
        </strong> </div>
        <?php
		}
			
		if($earning < 10){
			?>
            <div class="dokan-alert dokan-alert-danger">
        <strong>You don't have sufficient balance for a withdraw request!</strong>
</div>
          <?php  
		}else{	
		?> 
        
        
<form class="dokan-form-horizontal dr_withdraw" role="form" method="post">
  <div class="dokan-form-group">
    <label for="withdraw-amount" class="dokan-w3 dokan-control-label"> Withdraw Amount </label>
    <div class="dokan-w5 dokan-text-left">
      <div class="dokan-input-group"> <span class="dokan-input-group-addon"><?php echo get_woocommerce_currency_symbol();?></span>
      <!--  <input name="drwithdraw_amount" required min="10" max="<?php //echo $earning; ?>" class="wc_input_price dokan-form-control" id="drwithdraw-amount" type="number" placeholder="0.00" value="">-->
        <input type="number" id="drwithdraw_amount" name="drwithdraw_amount" min="10" max="<?php echo $earning; ?>" class="wc_input_price dokan-form-control" required>
      </div>
    </div>
  </div>
  <!--<div class="dokan-form-group">
    <label for="withdraw-method" class="dokan-w3 dokan-control-label"> Payment Method </label>
    <div class="dokan-w5 dokan-text-left">
      <select class="dokan-form-control" required="" name="withdraw_method" id="withdraw-method">
        <option value="paypal">PayPal</option>
      </select>
    </div>
  </div>-->
  <div class="dokan-form-group">
    <div class="dokan-w3 ajax_prev" style="margin-left:19%; width: 200px;">
     
      <input type="hidden" name="_wp_http_referer" value="/dashboard/withdraw/">
      <input type="submit" class="dokan-btn dokan-btn-theme" name="submit_widthdrow" value="Submit Request">
      <input type="hidden" name="driver_id" value="<?php echo $driver_id;?>">
    </div>
  </div>
</form>
      
      <?php }
	  		 } 
		  }
	  ?>
      
    </div>
    <!-- .entry-content --> 
    
  </article>
</div>
    
   <?php  
   
if(isset($_POST['submit_widthdrow'])){
	
	
	global $wpdb;
	global  $woocommerce;
	$driver_id = $_POST['driver_id'];
	$amount    = $_POST['drwithdraw_amount'];
	
	$on_date = date("Y-m-d H:i:s");
	$result = $wpdb->get_row( $wpdb->prepare(
                        "SELECT SUM(debit) as earnings,
                        ( SELECT SUM(credit) FROM {$wpdb->prefix}delivery_driver_commision WHERE driver_id = %d AND DATE(created) <= %s ) as withdraw
                        from {$wpdb->prefix}delivery_driver_commision
                        WHERE driver_id = %d AND DATE(created) <= %s",
                    $driver_id, $on_date, $driver_id, $on_date ) );	  
	$current_bl = $result->earnings - $result->withdraw;
	
	$rem_earning = number_format((float)$current_bl, 2, '.', '');
	
	
	if($amount <= $rem_earning ){
	
	$status = 0 ;
	if(!empty($driver_id) && !empty($amount) ){
		global $wpdb;
		
			$wpdb->insert("{$wpdb->prefix}delivery_driver_commision_withdraw", array(
					'driver_id' 			=> $driver_id,   
					'amount' 				=> $amount,
					'status'           		=> $status,    
				)
			); 
			
			
	}
		$url = home_url('mi-cuenta/commission-withdraw/?status=success');
		header("Location: $url");
	}else{
		$url = home_url('mi-cuenta/commission-withdraw/?status=invalid_amount');
		header("Location: $url");
		}
} 
}
  
add_action( 'woocommerce_account_commission-withdraw_endpoint', 'bbloomer_commission_withdraw_content' );
// Note: add_action must follow 'woocommerce_account_{your-endpoint-slug}_endpoint' format

add_action( 'wp_ajax_remove_widthdrow_req', 'remove_widthdrow_req' );
add_action( 'wp_ajax_nopriv_remove_widthdrow_req', 'remove_widthdrow_req' );
function remove_widthdrow_req(){
 
	$req_id = $_POST['req_id'];
	
	global $wpdb;
	if($req_id){
		
		 $wpdb->delete("{$wpdb->prefix}delivery_driver_commision_withdraw", array(
                'id' => $req_id
            ));
		
		$status = 1;
		$message = 'Request Deleted successfully.';
		echo json_encode(array('status'=>$status, 'message'=>$message,));
	exit; 
}
}