<?php
if ( isset($_GET['action'] ) && $_GET['action'] == 'download_csv_file' )
{
	global $wpdb;
	$table_name = $wpdb->prefix . 'delivery_driver_commision_withdraw';
	$sql='SELECT * FROM ' . $table_name . ' where isdelete="0" ORDER BY id DESC';
	
	if(isset($_GET['to']) and !empty($_GET['to']) and isset($_GET['from']) and !empty($_GET['from'])){
		$from	=	$_GET['from'];
		$to		=	$_GET['to'];
		$sql='SELECT * FROM ' . $table_name . ' where (date BETWEEN "'.$from.'" and "'.$to.'") and isdelete="0" ORDER BY id DESC';
	}
	$results = $wpdb->get_results($sql);
	$wp_filename = "dataentry_".date("d-m-y").".csv";
	ob_end_clean ();
	$wp_file = fopen($wp_filename,"w");
	$wp_array = array(
		'ID' => 'ID',
		'Email' =>'Email',
		'Date/time' =>'Date/time',
		'Marketing Flag' => 'Marketing Flag',
		'Title' => 'Title',
		'Company' => 'Company',
		'First name' => 'First name',
		'Last name' => 'Last name',
		'Address Line 1' => 'Address Line 1',
		'Address Line 2' => 'Address Line 2' ,
		'Postcode' =>'Postcode',
		'City' => 'City',
		'Country' => 'Country',
		'Telephone number' => 'Telephone number',
		'Landing page' => 'Landing page',
		'Sign up Page' =>'Sign up Page',
		'User journey' => 'User journey',
		'Referrer' => 'Referrer',
		'File downloaded' => 'File downloaded',
		'Calculator' => 'Calculator',
		'Form Name' => 'Form Name',
	);
	fputcsv($wp_file,$wp_array);
	foreach ($results as $row)
	{
		$wp_array = array(
			'id' => $row->id,
			'Email' => $row->email,
			//'Date/time' =>  date('Y-m-d h:i:sa',strtotime($row->date)),
			'Date/time' =>  date('Y-m-d',strtotime($row->date)),
			'Consent to marketing flag' => $row->marketingflag,
			'Title' => $row->title,
			'Company' => $row->company,
			'First name' => $row->firstname,
			'Last name' => $row->lastname,
			'Address Line 1' => $row->address1,
			'Address Line 2' => $row->address2,
			'Postcode' => $row->postcode,
			'City' => $row->city,
			'Country' => $row->country,
			'Telephone number' => $row->telephone,
			'Landing page' => $row->landing_page,
			'Sign up Page' => $row->sign_up_page,
			'User journey' => $row->user_journey,
			'Referrer' => $row->referrer,
			'File downloaded' => $row->file_downloaded,
			'Calculator' => $row->calculator,
			'Form Name' => $row->formname,
		);
		fputcsv($wp_file,$wp_array);
	}
	fclose($wp_file);
	header("Content-Description: Data Entry Csv");
	header("Content-Disposition: attachment; filename=".$wp_filename);
	header("Content-Type: application/csv;");
	readfile($wp_filename);
	exit;
}
$fromsql=$tosql='';
if(isset($_GET['from'])){
	$from	=	$_GET['from'];
	$fromsql= 	'&from='.$from;
}else{
	$from	=	date('Y-m-d', strtotime('- 4 day'));
}
if(isset($_GET['to'])){
	$to		=	$_GET['to'];
	$tosql	=	'&to='.$to;
}else{
	$to	=	date('Y-m-d');
}
$product_list_table = new Data_Entry_List_Table();?>
<div class="wrap">
	<div class="withdrow_note"></div>

	<h1 class="wp-heading-inline">Driver Commission Withdraw Request List</h1>
	<?php $product_list_table->prepare_items(); ?>
    
  
	<form method="get">
		<p class="search-box">
			<input type="hidden" name="page" value="driver_commission_entry">
		<!--	<a class="button" href="<?php //echo get_site_url();?>/wp-admin/admin.php?page=driver_commission_entry&action=download_csv_file<?php //echo $fromsql.$tosql;?>">Download Data Entry</a>-->
			<!--<input type="date" id="post-search-input" value="<?php //echo $from;?>" name="from">
			<input type="date" id="post-search-input" value="<?php //echo $to;?>" name="to" >
			<input type="submit" id="search-submit" class="button" value="Search Entries">
			<a class="button" href="<?php //echo get_site_url();?>/wp-admin/admin.php?page=driver_commission_entry">Reset</a>-->
		</p>
		<?php  $product_list_table->display(); ?>	
	</form>
	<script>
	function myFunction() {
	  document.getElementById("myForm").reset();
	}
	</script>
</div>
<style>

</style>