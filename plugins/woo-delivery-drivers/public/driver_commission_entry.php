<?php 

if (!class_exists('WP_List_Table')){

    require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');

}



class Data_Entry_List_Table extends WP_List_Table

{



    public $found_data = array();



    function example_data(){

        global $wpdb;

        $table_name = 	$wpdb->prefix . 'delivery_driver_commision_withdraw';

		

		$sql		=	'SELECT * FROM ' . $table_name;

		

		/* if(isset($_GET['from']) and !empty($_GET['from']) and !isset($_GET['to']) and empty($_GET['to'])){

			$from	=	$_GET['from'];

		}

		if(isset($_GET['to']) and !empty($_GET['to']) and !isset($_GET['from']) and empty($_GET['from'])){

			$to		=	$_GET['to'];

		} */

		

		if(isset($_GET['to']) and !empty($_GET['to']) and isset($_GET['from']) and !empty($_GET['from'])){

			$from	=	$_GET['from'];

			$to		=	$_GET['to'];

			$sql	=	'SELECT * FROM ' . $table_name . ' where (date BETWEEN "'.$from.'" and "'.$to.'") and isdelete="0" ORDER BY id DESC';

		}

	   

        $results = $wpdb->get_results($sql);

      

		

		$ps = array();

        foreach ($results as $row){

				if($row->status == 0){

					$statusdt = '<span class="pending" style="color: #965607; font-weight: 600; font-size: 14px; text-transform: uppercase;">Pending</span>';

				}else if($row->status == 1){

					$statusdt = '<span class="approved"  style="color: #008000; font-weight: 600; font-size: 14px; text-transform: uppercase;">Approved</span>';

				}else{

					$statusdt = '<span class="deny" style="color: #ff0000; font-weight: 600; font-size: 14px; text-transform: uppercase;">Deny</span>';

					}

			

            $entries = array(

                'id' => $row->id,

                'driver_id' => $row->driver_id,

				'date' =>  date('Y-m-d',strtotime($row->date)),

				//'date' =>  date('Y-m-d h:i:sa',strtotime($row->date)),

				'amount' => number_format((float)$row->amount, 2, '.', ''),

				'status' => $statusdt,

				/*'name' => $row->firstname .' '. $row->lastname,

				'address' => $row->address1.'<br>'.$row->address2.'<br>'.$row->postcode.','.$row->city.','.$row->country,

				 'postcode' => $row->postcode,

				'city' => $row->city,

				'country' => $row->country, */

				'note' => $row->note

            );

			array_push($ps, $entries);

        }

        return $ps;

    }



    function no_items(){

        _e('No Entry found, dude.');

    }



    function column_default($item, $column_name){

        switch ($column_name)

        {

            case 'id':

            case 'driver_id':

            case 'date':

            case 'amount':

            case 'status':

            case 'note':

            case 'action':

            /*case 'city':

            case 'country': 

            case 'telephone':*/

                return $item[$column_name];

            default:

                return print_r($item, true);

        }

    }



    function get_sortable_columns(){

        $sortable_columns = array(

            'id' => array( 'id', true ) ,

          /*  'driver_id' => array( 'driver_id', false) ,*/

			'date' => array( 'date', false) ,

			'amount' => array( 'amount', false) ,

			/*'company' => array( 'company', false) ,*/

			/*'status' => array( 'status', false) ,

			'note' => array( 'note', false) ,

			 'postcode' => array( 'postcode', false) ,

			'city' => array( 'city', false) ,

			'country' => array( 'country', false) , 

			'telephone' => array( 'telephone', false)*/

		);

        return $sortable_columns;

    }



    function get_columns(){

        $columns = array(

            'cb' => '<input type="checkbox" />', /* 'id'  => 'Id', */

            'driver_id' => 'Driver ID',

            'date' => 'Date',

            'amount' => 'Amount',

            'status' => 'Status',

            'note' => 'Note',

        	'action' => 'Actions',

        	 /*'city' => 'City',

        	'country' => 'Country', 

         	'telephone' => 'Telephone'*/

        );



        return $columns;

    }





    function process_bulk_action(){

        global $wpdb;

        $table_name = $wpdb->prefix . 'delivery_driver_commision_withdraw';



        if ('delete' === $this->current_action()){

            $ids = $_GET['id'];

            $ids = implode(',', $ids);

			$modified=date("Y-m-d h:i:s");			

            $wpdb->get_results("update " . $table_name . " set isdelete='1', modified='".$modified."' WHERE id IN($ids)");

        }

        if ('delete-single' === $this->current_action()){

            $id = $_GET['entry'];

			$modified=date("Y-m-d h:i:s");			

            $wpdb->get_results("update " . $table_name . " set isdelete='1' , modified='".$modified."' WHERE id='$id'");

        }

    }



    function column_driver_id($item){

		

		

		$driver_name = get_user_meta($item['driver_id'], 'first_name', true).' '.get_user_meta($item['driver_id'], 'last_name', true);

		if(empty($driver_name)){

			$viewdata = '<a href="'.home_url().'/wp-admin/user-edit.php?user_id='.$item['driver_id'].'" target="_blank">'.$item['driver_id'].'</a>';	

		}else{

			$viewdata = '<a href="'.home_url().'/wp-admin/user-edit.php?user_id='.$item['driver_id'].'" target="_blank">'.$driver_name.'</a>';	

			}

		

		

        sprintf('<input type="checkbox" name="id[]" value="%s" />', $item['id']);

		$actions = array();

		

		if($item['status'] == '<span class="pending" style="color: #965607; font-weight: 600; font-size: 14px; text-transform: uppercase;">Pending</span>'){

			$actions = array(



           // 'view' 		=> sprintf('<a href="?page=%s&action=%s&entry_id=%s&driver_id=%s">Approve</a>', $_REQUEST['page'], 'view', $item['id'], $item['driver_id']) ,



			'deny' 			=> sprintf('<a href="?page=%s&action=%s&entry_id=%s">Deny</a>', $_REQUEST['page'], 'deny', $item['id']) ,



		//	'delete' 		=> sprintf('<a href="?page=%s&action=%s&entry=%s">Delete</a>', $_REQUEST['page'], 'delete-single', $item['id']) ,

		



        );

		

		return sprintf('%1$s %2$s',$viewdata, $this->row_actions($actions));

		}else{

		 

       // return sprintf('%1$s %2$s',$viewdata, $this->row_actions($actions));

		





		$actions = array();

		return sprintf('%1$s %2$s',$viewdata, $this->row_actions($actions));

		}

        



    }



    function column_cb($item){

        return sprintf('<input type="checkbox" name="id[]" value="%s" />', $item['id']);

    }

	function column_action($item){

		

		if($item['status'] == '<span class="pending" style="color: #965607; font-weight: 600; font-size: 14px; text-transform: uppercase;">Pending</span>'){

		

        return sprintf('<div class="button-group"><a class="button button-small" href="?page=%s&action=%s&entry_id=%s&driver_id=%s"><span class="dashicons dashicons-yes"></span></a> <button data-rowid="%s"title="Add Note" class="button button-small add_note"><span class="dashicons dashicons-testimonial"></span></button></div>', $_REQUEST['page'],'view', $item['id'], $item['driver_id'],$item['id']);

		}else{

			 return sprintf('<div class="button-group"><button data-rowid="%s"title="Add Note" class="button button-small add_note"><span class="dashicons dashicons-testimonial"></span></button></div>',$item['id']);

			}

    }

	

	

	

   /* function get_bulk_actions(){

        $actions = array(

            'delete' => 'Delete'

        );

		return $actions;

    }*/



    function usort_reorder($a, $b)

    {

        $orderby = (!empty($_GET['orderby'])) ? $_GET['orderby'] : 'name';

        $order = (!empty($_GET['order'])) ? $_GET['order'] : 'asc';

        $result = strcmp($a[$orderby], $b[$orderby]);

        return ($order === 'asc') ? $result : -$result;

    }



    function prepare_items(){



        $columns = $this->get_columns();

        $hidden = array();

        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array( $columns, $hidden, $sortable );

        $this->process_bulk_action();

        $data = $this->example_data();

        usort($data, array(&$this, 'usort_reorder' ));

        $per_page = 20;

        $current_page = $this->get_pagenum();

        $total_items = count($data);

        $this->found_data = array_slice($data, (($current_page - 1) * $per_page) , $per_page);

        $this->items = $this->found_data;

        $this->set_pagination_args(array( 'total_items' => $total_items, 'per_page' => $per_page ));



    }



}



include 'driver_commission_table.php';



if (!empty($_GET['entry_id']) and isset($_GET['entry_id']) and ($_GET['action'] == 'view')){

	

	$row_id = $_GET['entry_id'];

	$driver_id = $_GET['driver_id'];

	$stripe_account_id = get_user_meta($driver_id, 'stripe_account_id', true);

	

		

	

		$driver_cmper     = get_option( 'ddwc_driver_commision_per' );	

		$driver_cmfix     = get_option( 'ddwc_driver_commision_fix' ); 

		

		if($driver_cmper){

			$per_commision = ($driver_cmper / 100) * $shipping_total;

		}

		

		if($driver_cmfix){

			$fix_commision  = $driver_cmfix;

		}

		

		$dr_commision     = $per_commision + $fix_commision;

	

		global $wpdb;

		$mid = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}delivery_driver_commision_withdraw WHERE id = $row_id AND driver_id =$driver_id");

		if($mid){

			foreach($mid as $result){

				$check = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}delivery_driver_commision WHERE order_id = $row_id AND driver_id =$driver_id");

				

				if(empty($check)){

					

					$test_enable = get_option( 'ddwc_driver_test_enable');

					if($test_enable == 'yes'){

						$secret_key = get_option( 'ddwc_driver_test_secret_key');

					}else{

						$secret_key = get_option( 'ddwc_driver_live_secret_key');;

						}

					

					/*try {		

					

					$stripe = new \Stripe\StripeClient($secret_key);

					

					$dr_group = 'Trnasfer-'.$driver_id;

				

				

				

					$charge = $stripe->transfers->create([

					  'amount' => $result->amount * 100,

					  'currency' => 'usd',

					  'destination' => $stripe_account_id,

					  'transfer_group' => $dr_group,

					]);*/

					

					

					$setdt = $wpdb->insert("{$wpdb->prefix}delivery_driver_commision", array(

							'driver_id' 			=> $driver_id,   

							'order_id' 				=> $row_id,

							'order_total'           => '',   

							'order_shipping' 		=> '',

							'order_shipping_method' => '',   

							'driver_cmper' 			=> $driver_cmper,

							'driver_cmfix'	 		=> $driver_cmfix,   

							'debit' 				=> 0,   

							'credit' 				=> $result->amount,

							'status'  				=> 1,

						)

					); 

				

					if($setdt){

						$wpdb->query($wpdb->prepare("UPDATE {$wpdb->prefix}delivery_driver_commision_withdraw SET status='1' WHERE id=$row_id"));

					}

					

					

					

					/*} catch (Exception $e) {

						//echo 'Caught exception: ',  $e->getMessage(), "\n";

						echo $e->getMessage();

						

						exit;

					}*/

					

					

					

				}

				

			}

		}

   $rd_url = home_url().'/wp-admin/admin.php?page=driver_commission_entry&status=success';	

   header("Location: $rd_url");

	

}else if (!empty($_GET['entry_id']) and isset($_GET['entry_id']) and ($_GET['action'] == 'deny')){

	

	$row_id = $_GET['entry_id'];

	$driver_id = $_GET['driver_id'];

	global $wpdb;

	$wpdb->query($wpdb->prepare("UPDATE {$wpdb->prefix}delivery_driver_commision_withdraw SET status='2' WHERE id=$row_id"));

	

}

