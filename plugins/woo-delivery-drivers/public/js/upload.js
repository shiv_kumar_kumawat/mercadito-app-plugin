jQuery(document).ready(function(e) {
	jQuery("#image_url_btn").click(function(e) {
		e.preventDefault();
		var r = wp.media({
			title: "Upload Image",
			multiple: !1
		}).open().on("select", function() {
			var e = r.state().get("selection").first(),
				a = e.toJSON().url,
				t = e.toJSON().id;
			console.log(e.toJSON().id), jQuery("#attach_id").val(t), jQuery("#productimage").attr("src", a),
			jQuery("#productimage").css("display", "block")
		})
	})
	
	
	jQuery("#image_url_btn_back").click(function(e) {
		e.preventDefault();
		var r = wp.media({
			title: "Upload Image",
			multiple: !1
		}).open().on("select", function() {
			var e = r.state().get("selection").first(),
				a = e.toJSON().url,
				t = e.toJSON().id;
			console.log(e.toJSON().id), jQuery("#attach_id_back").val(t), jQuery("#productimage_back").attr("src", a),
			jQuery("#productimage_back").css("display", "block")
		})
	})
});