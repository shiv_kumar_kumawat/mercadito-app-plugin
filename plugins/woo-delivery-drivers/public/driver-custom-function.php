<?php 

function callFirebaseAPInew($method, $url, $data){
	   $curl = curl_init($url);

	   switch ($method){
		  case "POST":
			 curl_setopt($curl, CURLOPT_POST, 1);
			 if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			 break;
		  case "PUT":
			 curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			 if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
			 break;
				 
		  default:
			 if ($data)
				$url = sprintf("%s?%s", $url, http_build_query($data));
	   }

	   // OPTIONS:
	   curl_setopt($curl, CURLOPT_URL, $url);
	   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		  'APIKEY: 111111111111111111111',
		  'Content-Type: application/json',
	   ));
	   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	   // EXECUTE:
	   $result = curl_exec($curl);
	   if(!$result){die("Connection Failure");}
	   curl_close($curl);
	   return $result;
	}



add_action( 'wp_ajax_show_drivers', 'show_drivers' );
add_action( 'wp_ajax_nopriv_show_drivers', 'show_drivers' );
function show_drivers(){
 
	$order_id = $_POST['order_id'];
	global $woocommerce;
	if($order_id){
		$order = wc_get_order($order_id);
		//update_post_meta( $order_id, 'ddwc_driver_id', $driver_id );
		//update_post_meta( $order_id, 'assignedORaccepted', 'assigned' );
		//$order = new WC_Order( $order_id );
		$order->update_status( 'ready-to-ship' );
		update_post_meta($order_id, 'delivery_request', 'send');
		$order->save();
		$status = 1;
		$message = 'Request sends to all drivers successfully.';
		echo json_encode(array('status'=>$status, 'message'=>$message,));
	exit; 
}
   
   
  /* $seller_id = dokan_get_seller_id_by_order( $order_id );
   
   $args = array(
		'role'    => 'driver',
		'orderby' => 'user_nicename',
		'order'   => 'ASC',
		'meta_key' => 'driver_vendor', 
		'meta_value' => $vendor_id,
		'meta_compare' => "LIKE",
	);
	$users = get_users( $args );
	$available_drivers = array();
	foreach ( $users as $user ) {
		if ( get_user_meta( $user->ID, 'ddwc_driver_availability', true ) ) {
		// Add driver to availabile list.
			$available_drivers[$user->ID] = $user->display_name;
		
		}
	}
	
	if($available_drivers){
		
		 global $wpdb; 
		 $table_name = $wpdb->prefix . 'driver_notification';
		 
		 foreach($available_drivers as $key => $val ){
		 $inserted = $wpdb->insert( $wpdb->prefix . 'driver_notification',
				array(
					'id'     => '',
					'order_id'    => $order_id,
					'driver_id'  => $key,
				),
				array(
					'%d',
					'%d',
					'%d',
			   
				)
			);
		 }
		if ( $inserted !== 1 ) {
			
				$status = 0;
				$message = 'Notification not send!';
		}else{
				update_post_meta($order_id, 'delivery_request', 'send');
				$status = 1;
				$message = 'Notification send to all drivers.';

			}	
							
	}else{
			
			$status = 1;
			$message = 'No drivers found!';
			
		}*/	
	
	echo json_encode(array('status'=>$status, 'message'=>$message,));
	exit;	
};

function send_notification_to_alldrivers($order_id){
	$order = wc_get_order( $order_id );

	$shipping_city = $order->get_shipping_city();
	$shipping_city =  get_post_meta($order_id,'_shipping_neighbourhood_1',true);
	
	$args = array(
					'role'    => 'driver',
					'orderby' => 'user_nicename',
					'order'   => 'ASC',
					'meta_query'  => array(
						'relation' => 'AND',
							array(
								'key'     =>'ddwc_driver_availability',
								'value'   => 'on',
								'compare' => '='
							),
							array(
								'key'     =>'ddwc_driver_transportation_city',
								'value'   => $shipping_city,
								'compare' => 'LIKE'
							)
						),
				);
		$users	=	get_users( $args );
		
		$avalable_driver = array();
		foreach ( $users as $user ) {
			$avalable_driver[] = $user->ID;
		}
	if($avalable_driver){
		$dr_List = implode(',', $avalable_driver); 
	}else{
		$dr_List ='null';
		}
	$fb_response = callFirebaseAPInew('POST', get_site_url().'/wp-json/driverorderapi/notify_driver', json_encode(array(
				'order_id'=>$order_id,
				'message' => 'New Order Placed',
				'title'   => '#'.$order_id.' Order Placed. Please Accept', 
				'drivers_id' => $dr_List
		)));
				
}

function calculate_driver_commision_on_complete($order_id){
	$driver_id = get_post_meta($order_id, 'ddwc_driver_id', true);
	$druser = get_userdata( $driver_id );
	$drdis_name = $druser->display_name;
	//$payment_tp = get_option( 'ddwc_driver_payment_type' );
	$delivery_earning = get_user_meta($driver_id, 'delivery_earning', true);
	global $wpdb;
	
	$seller_id  = dokan_get_seller_id_by_order( $order_id );
		if($delivery_earning =='vendor'){
				$commision_user_id = $seller_id;
			}else{
				$commision_user_id = $driver_id;	
			}
	
	if($driver_id){
		
		$order = wc_get_order( $order_id );
		$order_total      = $order->get_total();
		$shipping_total   = $order->get_shipping_total();	
		$shipping_method  = $order->get_shipping_method(); 
		$driver_cmper     = get_option( 'ddwc_driver_commision_per' );	
		$driver_cmfix     = get_option( 'ddwc_driver_commision_fix' ); 
		
		if($driver_cmper){
			$per_commision = ($driver_cmper / 100) * $shipping_total;
		}
		
		if($driver_cmfix){
			$fix_commision  = $driver_cmfix;
		}
		
		$dr_commision     = $per_commision + $fix_commision;
		//echo get_option( 'ddwc_driver_payment_type' );
		//if('stripe' == get_option( 'ddwc_driver_payment_type' )){
			global $wpdb;
			$mid = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}delivery_driver_commision WHERE order_id = $order_id");
			if(empty($mid)){
					$wpdb->insert("{$wpdb->prefix}delivery_driver_commision", array(
							'driver_id' 			=> $commision_user_id,   
							'order_id' 				=> $order_id,
							'order_total'           => $order_total,   
							'order_shipping' 		=> $shipping_total,
							'order_shipping_method' => $shipping_method,   
							'driver_cmper' 			=> $driver_cmper,
							'driver_cmfix'	 		=> $driver_cmfix,   
							'debit' 				=> round($dr_commision,2),   
							'credit' 				=> 0, 
						)
					); 
					
					$ord_cm = array(
										'payment_type'=> 'Bank Transfer',
										'status' => 'paid',
								);
					
				}
			/*}else{
				if (class_exists('WooWallet'))
	    		{
					global $wpdb;
					
					$credit_amount = array_sum(wp_list_pluck( get_wallet_transactions( array( 'user_id' =>$commision_user_id, 'where' => array( array( 'key' => 'type', 'value' => 'credit' ) ) ) ), 'amount' ) );
					$debit_amount = array_sum(wp_list_pluck( get_wallet_transactions( array( 'user_id' =>$commision_user_id, 'where' => array( array( 'key' => 'type', 'value' => 'debit' ) ) ) ), 'amount' ) );
					
							$amount = round($dr_commision,2);   
							$balance = $credit_amount - $debit_amount;
							$details = "Delivery fee of order #".$order_id;     
							$update = $wpdb->insert( "{$wpdb->base_prefix}woo_wallet_transactions", apply_filters( 'woo_wallet_transactions_args', array( 'blog_id' => 1, 'user_id' =>$commision_user_id, 'type' =>'credit', 'amount' =>$amount, 'balance' => $balance, 'currency' => get_woocommerce_currency(), 'details' => $details, 'date' => current_time('mysql'), 'created_by' =>$commision_user_id), array( '%d', '%d', '%s', '%f', '%f', '%s', '%s', '%s', '%d' ) ) );
						
							
							
							if ($update) {
								$transaction_id = $wpdb->insert_id;
								update_user_meta($commision_user_id, '_current_woo_wallet_balance', $balance);
								
								clear_woo_wallet_cache($commision_user_id);
								do_action( 'woo_wallet_transaction_recorded', $transaction_id,$commision_user_id, $amount, 'credit');
								$email_admin = WC()->mailer()->emails['Woo_Wallet_Email_New_Transaction'];
								if( !is_null( $email_admin ) && apply_filters( 'is_enable_email_notification_for_transaction', true, $transaction_id ) ){
									$email_admin->trigger( $transaction_id );
								}
								$ord_cm = array(
										'payment_type'=> get_option( 'ddwc_driver_payment_type' ),
										'status' => 'paid',
								);
							}
					}
			}*/
			
			
			$pmid = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}drivers_earning WHERE order_id = $order_id");
			if(empty($pmid)){
					$wpdb->insert("{$wpdb->prefix}drivers_earning", array(
							'driver_id' 			=> $driver_id,   
							'order_id' 				=> $order_id,
							'driver_name'           => $drdis_name, 
							'delivery_fee' 			=> round($dr_commision,2),   
							'vendor_id' 			=> $seller_id, 
							//'payment_type'          => $payment_tp,
							'payment_reciver'       => $delivery_earning,
						)
					); 
					
					$ord_cm = array(
										'payment_type'=> 'Bank Transfer',
										'status' => 'paid',
								);
					
				}
			
			
			$note = __("Driver commission ".round($dr_commision,2)." has been created for Driver ".get_user_meta($driver_id, 'first_name', true)." ".get_user_meta($driver_id, 'last_name', true).", And transfer to  ".get_user_meta($driver_id, 'first_name', true)." ".get_user_meta($driver_id, 'last_name', true).". ");
			$order->add_order_note( $note );
			update_post_meta($order_id, 'order_commission', $ord_cm);
			
		}
				
}



add_action( 'wp_ajax_driver_accept', 'driver_accept' );
add_action( 'wp_ajax_nopriv_driver_accept', 'driver_accept' );
function driver_accept(){
 	
	$user_id = get_current_user_id();
	$order_id = $_POST['order_id'];
	global $woocommerce;
	global $wpdb;
	update_post_meta($order_id, 'ddwc_driver_id', $user_id);
	$order = wc_get_order($order_id);
	$order->update_status( 'driver-assigned' );
	//update_post_meta($order_id, 'delivery_request', 'send');
	$order->save(); 
	
	$wpdb->delete("{$wpdb->prefix}driver_notification", array(
		'order_id' => $order_id
	));

	//update_post_meta($order_id, 'assignedORaccepted', 'accepted');
	$status = "success";
	$message = 'Delivery Driver Assign To Order:#' . $order_id;
	
	
	echo json_encode(array('status'=>$status, 'message'=>$message,));
	exit;			
}


add_action( 'wp_ajax_driver_reject', 'driver_reject' );
add_action( 'wp_ajax_nopriv_driver_reject', 'driver_reject' );
function driver_reject(){
 
	$order_id = $_POST['order_id'];
	$user_id = get_current_user_id();
	global $wpdb;
	$wpdb->delete("{$wpdb->prefix}driver_notification", array(
		'order_id' => $order_id,
		'driver_id' => $user_id
	));

	//update_post_meta($order_id, 'assignedORaccepted', 'accepted');
	$status = "success";
	$message = 'Order:#' . $order_id . ' Rejected By You.';
	
	
	echo json_encode(array('status'=>$status, 'message'=>$message,));
	exit;			
}


/*if (class_exists('WooWallet') && !class_exists('wallet_withdrawal')){
add_action( 'woo_wallet_menu_items', 'woo_wallet_withdrawal' );
function woo_wallet_withdrawal(){
		$url = "<a href='https://woowallet.in/product/woo-wallet-withdrawal/' target='_blank'>";
		$class = "class='withdrow_popup'";
		echo '<li class="card"><a id="withdrow_pp" href="javascript:void(0);" ><span class="dashicons dashicons-list-view"></span><p>Withdrawal</p></a></li>';
		print_r( '<script>jQuery("#withdrow_pp").click(function(){
    	jQuery(".woo-wallet-my-wallet-container").append("<div '.$class.'>Withdrawal requires Wallet Withdrawal Plugin to be installed and active. '.$url.' Wallet Withdrawal</a></div>" );}); </script>' );
		print_r('<style>.withdrow_popup {
    position: absolute;
    width: 400px;
    margin: 0 auto;
    text-align: center;
    background: #ccc;
    padding: 20px;
    color: red;
    background: #fff;
    border-radius: 2px;
    text-align: center;
    -webkit-transition: all .3s cubic-bezier(.25,.8,.25,1);
    transition: all .3s cubic-bezier(.25,.8,.25,1);
    cursor: pointer;
    box-shadow: 0 15px 28px rgb(0 0 0 / 25%), 0 10px 10px rgb(0 0 0 / 22%);
    top: 30%;
    left: 52%;
}</style>');
		
}
}*/

add_action( 'wp_ajax_show_earning_fields', 'show_earning_fields' );
add_action( 'wp_ajax_nopriv_show_earning_fields', 'show_earning_fields' );
function show_earning_fields(){
 

	$earning_selected = $_POST['earning_selected'];
	$vendor_id = $_POST['vendor_id'];
	$user_id = get_current_user_id();
	/*if('stripe' == get_option( 'ddwc_driver_payment_type' ) && $earning_selected == 'driver'){*/
	if($earning_selected == 'driver'){
			$status = "success";
			$stripebank_dt = "yes";
		/*}else if('stripe' == get_option( 'ddwc_driver_payment_type' ) && $earning_selected == 'vendor'){*/
		}else if($earning_selected == 'vendor'){
			
			if(empty(get_user_meta($vendor_id, 'stripe_account_id', true))){
				$status = "success";
				$stripebank_dt = "yes";
			}else{
				$status = "success";
				$stripebank_dt = "no";
				}
		}else{
			$status = "success";
			$stripebank_dt = "no";
			}
	echo json_encode(array('status'=>$status, 'show_stripe'=>$stripebank_dt,));
	exit;			
}

