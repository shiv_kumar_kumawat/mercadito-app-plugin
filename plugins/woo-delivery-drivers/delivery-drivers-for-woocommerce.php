<?php
/**
 * The plugin bootstrap file
 *
 * @link              https://expertwebtechnologies.com/
 * @since             1.0.0
 * @package           DDWC
 *
 * @wordpress-plugin
 * Plugin Name:       Woo Delivery Drivers
 * Plugin URI:        https://expertwebtechnologies.com/
 * Description:       Streamline your mobile workforce and increase your bottom line.
 * Version:           3.0
 * Author:            Expert Web Technologies
 * Author URI:        https://expertwebtechnologies.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ddwc
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC'))
{
    die;
}

/**
 * Current plugin version.
 */
define('DDWC_VERSION', '3.0');

function remove_plugin_update_notifications($value)
{

    if (isset($value) && is_object($value))
    {
        unset($value->response[plugin_basename(__FILE__) ]);
    }

    return $value;
}
add_filter('site_transient_update_plugins', 'remove_plugin_update_notifications');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ddwc-activator.php
 */
function activate_ddwc()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-ddwc-activator.php';
    Delivery_Drivers_Activator::activate();
    add_filter('site_transient_update_plugins', 'remove_plugin_update_notifications');
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ddwc-deactivator.php
 */
function deactivate_ddwc()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-ddwc-deactivator.php';
    Delivery_Drivers_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_ddwc');
register_deactivation_hook(__FILE__, 'deactivate_ddwc');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-ddwc.php';

// Plugin init hook.
add_action('plugins_loaded', 'wc_distance_rate_init', 1);

/**
 * Initialize plugin.
 */
function wc_distance_rate_init()
{

    if (!class_exists('WooCommerce'))
    {
        add_action('admin_notices', 'wc_distance_rate_woocommerce_deactivated');
        return;
    }

    require_once __DIR__ . '/includes/class-wc-distance-rate.php';
    new WC_Distance_Rate();

}

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ddwc()
{

    $plugin = new DDWC();
    $plugin->run();

}
run_ddwc();

// Create variable for settings link filter.
$plugin_name = plugin_basename(__FILE__);

/**
 * Add settings link on plugin page
 *
 * @since 1.0.3
 * @param array $links an array of links related to the plugin.
 * @return array updatead array of links related to the plugin.
 */
function ddwc_settings_link($links)
{
    // Pro link.
    //$pro_link = '<a href="https://deviodigital.com/product/delivery-drivers-for-woocommerce-pro" target="_blank" style="font-weight:700;">' . esc_attr__( 'Go Pro', 'ddwc' ) . '</a>';
    // Settings link.
    $settings_link = '<a href="admin.php?page=wc-settings&tab=ddwc">' . esc_attr__('Settings', 'ddwc') . '</a>';

    array_unshift($links, $settings_link);
    if (!function_exists('ddwc_pro_all_settings'))
    {
        array_unshift($links);
    }
    return $links;
}
add_filter("plugin_action_links_$plugin_name", 'ddwc_settings_link');

/**
 * Check DDWC Pro version number.
 *
 * If the DDWC Pro version number is less than what's defined below, there will
 * be a notice added to the admin screen letting the user know there's a new
 * version of the DDWC Pro plugin available.
 *
 * @since 2.9
 */
function ddwc_check_pro_version()
{
    // Only run if DDWC Pro is active.
    if (function_exists('ddwc_pro_all_settings'))
    {
        // Check if DDWC Pro version is defined.
        if (!defined('DDWC_PRO_VERSION'))
        {
            define('DDWC_PRO_VERSION', 0); // default to zero.
            
        }
        // Set pro version number.
        $pro_version = DDWC_PRO_VERSION;
        if ('0' == $pro_version || $pro_version < '1.7')
        {
            add_action('admin_notices', 'ddwc_update_ddwc_pro_notice');
        }
    }
}
add_action('admin_init', 'ddwc_check_pro_version');

/**
 * Error notice - Runs if DDWC Pro is out of date.
 *
 * @see ddwc_check_pro_version()
 * @since 2.9
 */
function ddwc_update_ddwc_pro_notice()
{
    $ddwc_orders = '<a href="https://www.deviodigital.com/my-account/orders/" target="_blank">' . __('Orders', 'ddwc') . '</a>';
    $error = sprintf(esc_html__('There is a new version of DDWC Pro available. Download your copy from the %1$s page on Devio Digital.', 'ddwc') , $ddwc_orders);
    echo '<div class="notice notice-info"><p>' . $error . '</p></div>';
}

function ddwc_pro_add_email_actions($email_actions)
{
    $email_actions[] = 'woocommerce_order_status_driver-assigned';
    $email_actions[] = 'woocommerce_order_status_out-for-delivery';
    $email_actions[] = 'woocommerce_order_status_processing_to_driver-assigned';
    $email_actions[] = 'woocommerce_order_status_driver-assigned_to_out-for-delivery';
    $email_actions[] = 'woocommerce_order_status_out-for-delivery_to_completed';

    return $email_actions;
}

function WooWalletNotEnabled() {
	/* translators: %s: WooCommerce link */
	echo '<div class="notice notice-error"><p>' . sprintf( esc_html__( 'Enabling TeraWallet requires %s to be installed and active.', 'woocommerce-distance-rate-shipping' ), '<a href="https://wordpress.org/plugins/woo-wallet/" target="_blank">TeraWallet – For WooCommerce</a>' ) . '</p></div>';
}

 	//$class = 'notice notice-error';
   // $message = __( 'Opps! An error has occurred. For enabling TeraWallet please add this plugin before <a href="https://wordpress.org/plugins/woo-wallet/">TeraWallet – For WooCommerce</a>', 'sample-text-domain' );
 
   // printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 


add_filter('woocommerce_email_actions', 'ddwc_pro_add_email_actions');

/**
 * Class Delivery_Drivers_WC_Emails
 *
 * @since 1.1
 */
class Delivery_Drivers_WC_Emails
{
    /**
     * Delivery_Drivers_WC_Emails constructor.
     */
    public function __construct()
    {
        // Filtering the emails and adding our own email.
        add_filter('woocommerce_email_classes', array(
            $this,
            'register_emails'
        ) , 15, 1);
        // Absolute path to the plugin folder.
        define('DDWC_PRO_EMAIL_PATH', plugin_dir_path(__FILE__));
    }

    /**
     * Register emails
     *
     * @param array $emails
     *
     * @return array
     */
    public function register_emails($emails)
    {
        // Email classes.
        require_once 'templates/emails/class-wc-order-driver-assigned.php';
        require_once 'templates/emails/class-wc-order-out-for-delivery.php';
        require_once 'templates/emails/class-wc-order-completed.php';

        // Register emails.
        $emails['WC_Order_Driver_Assigned'] = new WC_Order_Driver_Assigned();
        $emails['WC_Order_Out_For_Delivery'] = new WC_Order_Out_For_Delivery();
        $emails['WC_Order_Completed'] = new WC_Order_Completed();

        return $emails;
    }

}
new Delivery_Drivers_WC_Emails();

register_activation_hook(__FILE__, 'buyback_install');

function buyback_install()
{
    require_once (ABSPATH . 'wp-admin/includes/upgrade.php');

    global $wpdb;

    $table_name = $wpdb->prefix . 'driver_notification';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name (
id mediumint(9) NOT NULL AUTO_INCREMENT,
order_id int NOT NULL,
driver_id int NOT NULL,
PRIMARY KEY (id)
) $charset_collate;";
    dbDelta($sql);
	
	
	$table_erning = $wpdb->prefix . 'drivers_earning';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_erning (
id mediumint(9) NOT NULL AUTO_INCREMENT,
order_id int NOT NULL,
driver_id int NOT NULL,
driver_name varchar(30),
delivery_fee decimal(19,4) NOT NULL,
vendor_id int NOT NULL,
payment_type varchar(30),
payment_reciver varchar(30),
dat_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (id)
) $charset_collate;";
    dbDelta($sql);
	

    $table_name2 = $wpdb->prefix . 'delivery_driver_commision';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name2 (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	driver_id int NOT NULL,
	order_id int NOT NULL,
	order_total int NOT NULL,
	order_shipping int,
	order_shipping_method text,
	driver_cmper float ,
	driver_cmfix float ,
	debit float,
    credit float,
	status varchar(30),
	created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	PRIMARY KEY  (id)
) $charset_collate;";
    dbDelta($sql);

    $table_name3 = $wpdb->prefix . 'delivery_driver_commision_withdraw';
    $charset_collate = $wpdb->get_charset_collate();
    $sql2 = "CREATE TABLE $table_name3 (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	driver_id int NOT NULL,
	amount decimal(19,4) NOT NULL,
	status int(1) NOT NULL,
	note text,
	date TIMESTAMP NOT NULL,
	PRIMARY KEY  (id)
) $charset_collate;";
    dbDelta($sql2);

}

// Twilio.


include_once (dirname(__FILE__) . '/vendors/twilio/Twilio/autoload.php');

include_once (dirname(__FILE__) . '/public/driver-custom-function.php');
require_once (dirname(__FILE__) . '/stripe-connect.php');
require_once (dirname(__FILE__) . '/customer-order-bar.php');
require_once ('config.php');
//require_once('./vendor/autoload.php');
/* @include_once dirname( __FILE__ ) . "/api/userapi.php";
@include_once dirname( __FILE__ ) . "/api/orderapi.php";
@include_once dirname( __FILE__ ) . "/api/firebaseapi.php"; */
// Dokan settings.
if (in_array('dokan-lite/dokan.php', apply_filters('active_plugins', get_option('active_plugins')) , true))
{
    include_once (dirname(__FILE__) . '/driver-dokan-settings.php');
    global $woocommerce;
	 //if ( 'stripe' == get_option( 'ddwc_driver_payment_type' ) ) {
    	include_once (dirname(__FILE__) . '/public/commission-withdraw.php');
	 //}

    //Add Extra tab to Vendor dashboard for driver
    add_filter('dokan_query_var_filter', 'driver_dokan_load_document_menu');
    add_filter('dokan_get_dashboard_nav', 'driver_dokan_add_help_menu');
    add_action('dokan_load_custom_template', 'driver_dokan_load_template');
    add_action('wp_ajax_dokan_settings', 'driver_prefix_save_about_settings', 5);
    add_action('delete_user', 'delete_stripe_account_on_delete');

    add_action('woocommerce_admin_order_actions_start', 'action_woocommerce_admin_order_actions_start', 10, 1);
   	add_filter('dokan_settings_form_bottom', 'driver_extra_fields', 10, 2);
   	add_action('dokan_store_profile_saved', 'driver_save_extra_fields', 15);
    add_action('woocommerce_order_status_ready-to-ship', 'send_notification_to_alldrivers');
    add_action('woocommerce_order_status_completed', 'calculate_driver_commision_on_complete');
    add_action('woocommerce_order_status_order-returned', 'calculate_driver_commision_on_complete');
    add_action('rest_api_init', 'JsonStripUserApi');
}

function JsonStripUserApi()
{
    register_rest_route('driverapi', '/stripe_account_create', array(
        'methods' => 'POST',
        'callback' => 'stripe_account_create',
    ));
}

function stripe_account_create($request)
{
    $parameters = $request->get_json_params();
    extract($parameters);
    if (!$cookie)
    {
        $response['status'] = "error";
        $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
        return new WP_REST_Response($response, 200);
    }
    $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
    if (!$user_id)
    {
        $response['status'] = "error";
        $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
        return new WP_REST_Response($response, 200);
    }
    if (!$meta)
    {
        $response['status'] = "error";
        $response['message'] = "You must include a 'meta' var in your request , wchich cantain array of metakeys and value which need to update.";
        return new WP_REST_Response($response, 200);
    }

    if (empty($meta['ddwc_driver_account_number']))
    {
        $response['status'] = "error";
        $response['message'] = "You must include a 'ddwc_driver_account_number' var in your meta request.";
        return new WP_REST_Response($response, 200);
    }
    if (empty($meta['ddwc_driver_bank_name']))
    {
        $response['status'] = "error";
        $response['message'] = "You must include a 'ddwc_driver_bank_name' var in your meta request.";
        return new WP_REST_Response($response, 200);
    }
    if (empty($meta['ddwc_driver_account_holder_name']))
    {
        $response['status'] = "error";
        $response['message'] = "You must include a 'ddwc_driver_account_holder_name' var in your meta request.";
        return new WP_REST_Response($response, 200);
    }
    if (empty($meta['ddwc_driver_ssn_number']))
    {
        $response['status'] = "error";
        $response['message'] = "You must include a 'ddwc_driver_ssn_number' var in your meta request.";
        return new WP_REST_Response($response, 200);
    }
    if (empty($meta['ddwc_driver_routing_number']))
    {
        $response['status'] = "error";
        $response['message'] = "You must include a 'ddwc_driver_routing_number' var in your meta request.";
        return new WP_REST_Response($response, 200);
    }
   
    $test_enable = get_option('ddwc_driver_test_enable');

    if ($test_enable == 'yes')
    {
        $secret_key = get_option('ddwc_driver_test_secret_key');
    }
    else
    {
        $secret_key = get_option('ddwc_driver_live_secret_key');;
    }
	$user = get_userdata($user_id);
	
    $date = strtotime(get_user_meta($user_id, 'ddwc_driver_dob', true));
	
	$ssn_number = sanitize_text_field($meta['ddwc_driver_ssn_number']);
    $ssn_last4 = substr($ssn_number, -4);
	
    $day = date('d', $date);
    $month = date('m', $date);
    $year = date('Y', $date);
	
	foreach(get_user_meta($user->ID) as $k =>$v){
		$fields[$k]	=	$v[0];
	}
    try
    {
        $full_name = $dr_fname . ' ' . $dr_lname;
        $urls = home_url('my-account');
        $stripe = new \Stripe\StripeClient($secret_key);
        $c_date = time();
        $account = $stripe
            ->accounts
            ->create([
						"type" => "custom",
						"country" => "US",
						"email" =>$user->user_email,
								'capabilities' => ['transfers' => ['requested' => true],],
						"business_profile" => 	[
							"name" => "Love IE Local",
							"url" => get_site_url(),
							"mcc" => '5045'
						],
						"business_type" =>"individual",
						"individual" => [
							"first_name" => $user->user_firstname,
							"last_name" =>$user->last_name,
							"dob" => [
								"day" => $day,
								"month" => $month,
								"year" => $year
							],
							"ssn_last_4" => $ssn_last4,
							"id_number" => $ssn_number,
							"phone" => $fields['phone'],
							"email" => $user->user_email,
							"address" => [
								"city" => "Fall River",
								"state" => "MA",
								"postal_code" => 02721,
								"line1" => "374 William S Canning Blvd"
							],
						],
						"external_account" => [
							"object" => "bank_account",
							"currency" => "usd",
							"country" => "US",
							"account_number" => $meta['ddwc_driver_account_number'],
							"routing_number" =>$meta['ddwc_driver_routing_number'],
							"bank_name" =>$meta['ddwc_driver_bank_name'],
							"account_holder_name" => $meta['ddwc_driver_account_holder_name'],
							"account_holder_type" => 'individual'
						],
						"tos_acceptance" => [
							"date" => $c_date,
							"ip" => $_SERVER['REMOTE_ADDR']
						]
					]);

        $stripeAccountId = $account->id;

        if ($stripeAccountId)
        {

            $stripeAccountObj = $stripe
                ->accounts
                ->retrieve($stripeAccountId);
            $caps = $stripe
                ->accounts
                ->allCapabilities($stripeAccountId, []);

        }

    }
    catch(Exception $e)
    {
		$data['status']		= "error";
        $data['message']	= 'Stripe Error : "' . $e->getMessage() . '"';
		return $data;
    }
	update_user_meta($user_id, 'stripe_account_id', $stripeAccountId);
	update_user_meta($user_id, 'ddwc_driver_account_number', $meta['ddwc_driver_account_number']);
	update_user_meta($user_id, 'ddwc_driver_bank_name', $meta['ddwc_driver_bank_name']);
	update_user_meta($user_id, 'ddwc_driver_account_holder_name', $meta['ddwc_driver_account_holder_name']);
	update_user_meta($user_id, 'ddwc_driver_ssn_number', $meta['ddwc_driver_ssn_number']);
	update_user_meta($user_id, 'ddwc_driver_routing_number',$meta['ddwc_driver_routing_number']);
	
	$data['status']	= "success";
	$data['message']= "Your stripe account successfully created.";
	$data['stripe_account_id']	= $stripeAccountId;
	foreach ($meta as $k => $v)
    {
        $data[$k] = $v ;
    }

    return $data;
}

add_action('admin_menu', 'admin_menu_fnction');
function admin_menu_fnction()
{
    //if('stripe' == get_option( 'ddwc_driver_payment_type' )){
		add_submenu_page('woocommerce', 'Driver Withdraw', 'Driver Withdraw', 'manage_options', 'driver_commission_entry', 'driver_commission_entry');
	//}
}

function driver_commission_entry()
{
    include_once (dirname(__FILE__) . '/public/driver_commission_entry.php');
}

// Add extra field in seller settings
function driver_extra_fields($current_user, $profile_info)
{
    $vendor_city = isset($profile_info['vendor_city']) ? $profile_info['vendor_city'] : '';
?>

 <div class="gregcustom dokan-form-group">
        <label class="dokan-w3 dokan-control-label" for="setting_address">
            <?php _e('Store Location (city)', 'dokan'); ?>
        </label>
         <div class="dokan-w5">
        <?php 
		 $cities_list = get_option( 'ddwc_driver_citys_list' );
		 $citylist_array = explode(', ', $cities_list);
		
		if ( $citylist_array ) {
									echo  '<select name="vendor_city" id="vendor_city" class="dokan-form-control">';
								    echo '<option value="">-Select City-</option>';
									foreach ( $citylist_array as $key => $val ) {
									
											if($vendor_city == $val){
												echo '<option value="'.$val.'" selected="selected">'.$val.'</option>';
											
												}else{
												echo '<option value="'.$val.'">'.$val.'</option>';
												
												}
											
									
									}
									echo '</select>';
								}
		
		?>
    
        </div>
    </div>
    
    <?php
}

function driver_save_extra_fields($store_id)
{
    $dokan_settings = dokan_get_store_info($store_id);
    if (isset($_POST['vendor_city']))
    {
        $dokan_settings['vendor_city'] = $_POST['vendor_city'];
    }
    update_user_meta($store_id, 'dokan_profile_settings', $dokan_settings);
}

// define the woocommerce_admin_order_actions_start callback
function action_woocommerce_admin_order_actions_start($order)
{
    $order_id = $order->get_id();

    $vendor_id = dokan_get_current_user_id();
   
	
		
		
		
    $delivery_request = get_post_meta($order_id, 'delivery_request', true);
    if ($delivery_request == 'send')
    {
        echo '<a class="dokan-btn dokan-btn-default dokan-btn-sm tips" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ready to Ship" onclick="show_drivers_allready(' . $order_id . ');" style="margin: 0 5px;"><i class="fa fa-truck">&nbsp;</i></a>';
    }
    else
    {
        echo '<a class="dokan-btn dokan-btn-default dokan-btn-sm tips" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ready to Ship" onclick="show_drivers(' . $order_id . ');" style="margin: 0 5px;"><i class="fa fa-truck">&nbsp;</i></a>';
    }

    /*?> if($available_drivers) {?>
     
     					<div id="drpopup-<?php echo dokan_get_prop( $order, 'id' ); ?>" class="driver-popup" style="display: none;"> 
            			<div class="inner_blck">
                        <form action="" method="post" id="assign_ord_driver" class="assign_ord_driver">
                        <span class="close_icn">+</span>
                        <select name="selected_driver" class="driver_list dokan-form-control dokan-select2 select2-hidden-accessible" data-orderid="<?php echo dokan_get_prop( $order, 'id' ); ?>">
                        <option>-Select Driver-</option>
                        
                        <?php 
    		foreach ( $available_drivers as $key => $val ) {
    			if(get_post_meta( $order_id, 'ddwc_driver_id', true ) == $key){
    					echo '<option value="' . $key . '" selected="selected"> ' . $val . '</option>';
    				}else{
    								echo '<option value="' . $key . '"> ' . $val . '</option>';
    			}
    		}
    		
    		?>
                        </select>
                        	<input type="hidden" name="order_id" value="<?php echo dokan_get_prop( $order, 'id' ); ?>">
                        	<input name="assign_drto_order" type="submit" value="Assign" class="dokan-btn dokan-btn-theme assign_drvr">
                        
                        </form>
                        </div>
                        
             </div>
                        
                        <?php } <?php */

};

function driver_dokan_add_help_menu($urls)
{
    $urls['vendor-driver'] = array(
        'title' => __('Drivers', 'dokan') ,
        'icon' => '<i class="fa fa-truck" aria-hidden="true"></i>',
        'url' => dokan_get_navigation_url('vendor-driver') ,
        'pos' => 51
    );
	
	 $urls['delivery-earnings'] = array(
        'title' => __('Delivery Details', 'dokan') ,
        'icon' => '<i class="fa fa-money" aria-hidden="true"></i>',
        'url' => dokan_get_navigation_url('delivery-earnings') ,
        'pos' => 51
    );
	
    return $urls;
}

function driver_dokan_load_document_menu($query_vars)
{
    $query_vars['vendor-driver'] = 'vendor-driver';
	$query_vars['delivery-earnings'] = 'delivery-earnings';
    return $query_vars;
}

function driver_dokan_load_template($query_vars)
{
    if (isset($query_vars['vendor-driver']))
    {
        require_once dirname(__FILE__) . '/vendor-driver.php';
    }
	if (isset($query_vars['delivery-earnings']))
    {
        require_once dirname(__FILE__) . '/delivery-earnings.php';
    }
}

function driver_prefix_save_about_settings()
{
	
	
	// if('stripe' == get_option( 'ddwc_driver_payment_type' )){
	
	// 	$test_enable = get_option('ddwc_driver_test_enable');
	
	// 	if ($test_enable == 'yes')
	// 	{
	// 		$secret_key = get_option('ddwc_driver_test_secret_key');
	// 	}
	// 	else
	// 	{
	// 		$secret_key = get_option('ddwc_driver_live_secret_key');;
	// 	}
	
	// 	$vendor_id = dokan_get_current_user_id();
	
	// 	$post_data = wp_unslash($_POST);
	
	// 	$licence_imgfront = sanitize_text_field($post_data['licence_img_front']);
	// 	$licence_img_back = sanitize_text_field($post_data['licence_img_back']);
	// 	$l_front = wp_get_attachment_image_src($licence_imgfront);
	// 	$l_back = wp_get_attachment_image_src($licence_img_back);
	
	// 	$blog_name = get_bloginfo('name');
	
	// 	$nonce = isset($post_data['_wpnonce']) ? $post_data['_wpnonce'] : '';
	// 	// Bail if another settings tab is being saved
	// 	if (!wp_verify_nonce($nonce, 'dokan_about_settings_nonce'))
	// 	{
	// 		return;
	// 	}
	
	// 	$dr_fname = sanitize_text_field($post_data['dr_fname']);
	// 	$dr_lname = sanitize_text_field($post_data['dr_lname']);
	// 	$dr_email = sanitize_text_field($post_data['dr_email']);
	// 	//$dr_phone_cd    = sanitize_text_field( $post_data['dr_phone_cd'] );
	// 	$dr_phone = sanitize_text_field($post_data['dr_phone']);
	
	// 	$account_number = sanitize_text_field($post_data['ddwc_driver_account_number']);
	// 	$bank_name = sanitize_text_field($post_data['ddwc_driver_bank_name']);
	// 	$account_holder_name = sanitize_text_field($post_data['ddwc_driver_account_holder_name']);
	// 	$account_holder_type = sanitize_text_field($post_data['ddwc_driver_account_holder_type']);
	// 	$ssn_number = sanitize_text_field($post_data['ddwc_driver_ssn_number']);
	// 	$ssn_last4 = substr($ssn_number, -4);
	// 	$routing_number = sanitize_text_field($post_data['ddwc_driver_routing_number']);
	// 	$date_ofb = sanitize_text_field($post_data['ddwc_driver_dob']);
	// 	$date = strtotime($date_ofb);
	// 	$day = date('d', $date);
	// 	$month = date('m', $date);
	// 	$year = date('Y', $date);
	
	// 	$dr_citys = $post_data['ddwc_driver_transportation_city'];
	// 	$neighbourhood_city = $post_data['ddwc_neighbourhood_city'];
	
	// 	$license_plate = sanitize_text_field($post_data['ddwc_driver_license_plate']);
	// 	$trans_type = sanitize_text_field($post_data['ddwc_driver_transportation_type']);
	// 	$vehical_mdl = sanitize_text_field($post_data['ddwc_driver_vehicle_model']);
	// 	$vehical_clr = sanitize_text_field($post_data['ddwc_driver_vehicle_color']);
	// 	$licance_num = sanitize_text_field($post_data['ddwc_driver_license_number']);
	// 	$delivery_earning = sanitize_text_field($post_data['delivery_earning']);
		
	// 	$vendor_stripe = get_user_meta($vendor_id, 'stripe_account_id', true); 
		 
	// 	if(($delivery_earning == 'vendor' && empty($vendor_stripe)) || ($delivery_earning == 'driver') ){
		
	// 		if (empty($dr_fname) || empty($dr_lname) || empty($dr_email) || empty($dr_phone) || empty($dr_citys) || empty($account_number) || empty($bank_name) || empty($account_holder_name) || empty($ssn_number) || empty($routing_number) || empty($date_ofb))
	// 		{
	// 			wp_send_json_error(__('All Required Fields should not be blank!'));
	// 		}
	// 		if (4 > strlen($dr_fname))
	// 		{
	// 			wp_send_json_error(__('At least 4 characters is required'));
	// 		}
			
	// 		if (9 > strlen($ssn_number))
	// 		{
	// 			wp_send_json_error(__('SSN should be 9 characters.'));
	// 		}
		
	// 		if (!is_email($dr_email))
	// 		{
	// 			wp_send_json_error(__('Email is not valid'));
	// 		}
		
	// 		if (email_exists($dr_email))
	// 		{
	// 			wp_send_json_error(__('Email Already in use'));
	// 		}
		
	// 		/*$username = preg_replace('/([^@]*).', '$1', $dr_email);*/
	// 		if (username_exists($username))
	// 		{
	// 			$username = $username . '1';
	// 		}
	// 	}else{
			
	// 		if (empty($dr_fname) || empty($dr_lname) || empty($dr_email) || empty($dr_phone) || empty($dr_citys) || empty($date_ofb))
	// 		{
	// 			wp_send_json_error(__('All Required Fields should not be blank!'));
	// 		}
	// 		if (4 > strlen($dr_fname))
	// 		{
	// 			wp_send_json_error(__('At least 4 characters is required'));
	// 		}
			
	// 		if (!is_email($dr_email))
	// 		{
	// 			wp_send_json_error(__('Email is not valid'));
	// 		}
		
	// 		if (email_exists($dr_email))
	// 		{
	// 			wp_send_json_error(__('Email Already in use'));
	// 		}
		
	// 		$username = preg_replace('/([^@]*).*/', '$1', $dr_email);
	// 		if (username_exists($username))
	// 		{
	// 			$username = $username . '1';
	// 		}
				
	// 	}
		
	// 	if(($delivery_earning == 'vendor' && empty($vendor_stripe)) || ($delivery_earning == 'driver')){
		
	// 		try
	// 		{	
	// 			//$vendor_id
	// 			if($delivery_earning == 'vendor'){
					
	// 					$user_info = get_userdata( $vendor_id );
	// 					$profile = get_user_meta(3, 'dokan_profile_settings', true);
	// 					$stripe_phone = $profile['phone'];
						
	// 					$full_name = get_user_meta($vendor_id, 'first_name', true) . ' ' . get_user_meta($vendor_id, 'last_name', true);
	// 					$stripe_email = $user_info->user_email;;
	// 					$stripef_name = get_user_meta($vendor_id, 'first_name', true);
	// 					$stripel_name = get_user_meta($vendor_id, 'last_name', true);
						
	// 			}else{
					
	// 					$full_name = $dr_fname . ' ' . $dr_lname;
	// 					$stripe_email = $dr_email;
	// 					$stripef_name = $dr_fname;
	// 					$stripel_name = $dr_lname;
	// 					$stripe_phone = $dr_phone;
						
	// 				}
			
	// 			//$full_name = $dr_fname . ' ' . $dr_lname;
	// 			$urls = home_url('my-account');
	// 			$stripe = new \Stripe\StripeClient($secret_key);
	// 			$c_date = time();
	// 			$account = $stripe
	// 				->accounts
	// 				->create(["type" => "custom", "country" => "US", "email" => $stripe_email,
	// 			//"requested_capabilities" => ["card_payments", 'transfers'],
	// 			'capabilities' => [
	// 			// 'card_payments' => ['requested' => true],
	// 			'transfers' => ['requested' => true], ], "business_profile" => ["name" => "Woo Delivery", "url" => "https://demo.woodeliverydriver.com/", "mcc" => '5045'], "business_type" => "individual", "individual" => ["first_name" => $stripef_name, "last_name" => $stripel_name, "dob" => ["day" => $day, "month" => $month, "year" => $year], "ssn_last_4" => $ssn_last4, "id_number" => $ssn_number, "phone" => $stripe_phone, "email" => $stripe_email, "address" => ["city" => "Fall River", "state" => "MA", "postal_code" => 02721, "line1" => "374 William S Canning Blvd"], ], "external_account" => ["object" => "bank_account", "currency" => "usd", "country" => "US", "account_number" => $account_number, "routing_number" => $routing_number, "bank_name" => $bank_name, "account_holder_name" => $account_holder_name, "account_holder_type" => 'individual'],
		
	// 			//"external_account" => ["object"=>"bank_account","currency"=>"usd","country"=>"US","account_number" => '000123456789' , "routing_number"=>'110000000', "bank_name" => 'TEST STRIPE BANK', "account_holder_name" => $account_holder_name, "account_holder_type" => 'individual'],
	// 			"tos_acceptance" => ["date" => $c_date, "ip" => $_SERVER['REMOTE_ADDR']]]);
		
	// 			$stripeAccountId = $account->id;
		
	// 			if ($stripeAccountId)
	// 			{
		
	// 				$stripeAccountObj = $stripe
	// 					->accounts
	// 					->retrieve($stripeAccountId);
	// 				$caps = $stripe
	// 					->accounts
	// 					->allCapabilities($stripeAccountId, []);
		
	// 			}
		
	// 		}
	// 		catch(Exception $e)
	// 		{
	// 			//echo 'Caught exception: ',  $e->getMessage(), "\n";
	// 			wp_send_json_error(__('<strong>Stripe Error : "' . $e->getMessage() . '"</strong>'));
	// 			exit;
	// 		}
	// 	}
		
	
	// 	$password = wp_generate_password(12, true);
	
	// 	$userdata = array(
	// 		'user_login' => $username,
	// 		'user_email' => $dr_email,
	// 		'user_pass' => $password,
	// 		'display_name' => $dr_fname . ' ' . $dr_lname,
	
	// 	);
	// 	$user_id = wp_insert_user($userdata);
	// 	if ($user_id)
	// 	{
	
	// 	   if($delivery_earning == 'vendor' && empty($vendor_stripe)){
	// 			update_user_meta($vendor_id, 'stripe_account_id', $stripeAccountId);
	// 			update_user_meta($vendor_id, 'vendor_account_number', $account_number);
	// 			update_user_meta($vendor_id, 'vendor_bank_name', $bank_name);
	// 			update_user_meta($vendor_id, 'vendor_account_holder_name', $account_holder_name);
	// 			update_user_meta($vendor_id, 'vendor_account_holder_type', $account_holder_type);
	// 			update_user_meta($vendor_id, 'vendor_ssn_number', $dr_city);
	// 			update_user_meta($vendor_id, 'vendor_ssn_number', $ssn_number);
	// 			update_user_meta($vendor_id, 'vendor_routing_number', $routing_number);
	// 	   }else{
	// 			update_user_meta($user_id, 'stripe_account_id', $stripeAccountId);
	// 			update_user_meta($user_id, 'ddwc_driver_account_number', $account_number);
	// 			update_user_meta($user_id, 'ddwc_driver_bank_name', $bank_name);
	// 			update_user_meta($user_id, 'ddwc_driver_account_holder_name', $account_holder_name);
	// 			update_user_meta($user_id, 'ddwc_driver_account_holder_type', $account_holder_type);
	// 			update_user_meta($user_id, 'ddwc_driver_ssn_number', $dr_city);
	// 			update_user_meta($user_id, 'ddwc_driver_ssn_number', $ssn_number);
	// 			update_user_meta($user_id, 'ddwc_driver_routing_number', $routing_number);		   
	// 		}
	// 		update_user_meta($user_id, 'ddwc_driver_availability', 'on');
	// 		update_user_meta($user_id, 'phone', $dr_phone);
	// 		update_user_meta($user_id, 'billing_phone', $full_num);
	// 		update_user_meta($user_id, 'first_name', $dr_fname);
	// 		update_user_meta($user_id, 'last_name', $dr_lname);
	// 		update_user_meta($user_id, 'ddwc_driver_license_plate', $license_plate);
	// 		update_user_meta($user_id, 'ddwc_driver_transportation_type', $trans_type);
	// 		update_user_meta($user_id, 'ddwc_driver_vehicle_model', $vehical_mdl);
	// 		update_user_meta($user_id, 'ddwc_driver_vehicle_color', $vehical_clr);
	// 		update_user_meta($user_id, 'ddwc_driver_license_number', $licance_num);
	
	// 		update_user_meta($user_id, 'ddwc_driver_transportation_city',implode(',',$dr_citys));
	// 		update_user_meta($user_id, 'ddwc_neighbourhood_city',$neighbourhood_city);
	// 		update_user_meta($user_id, 'ddwc_driver_dob', $date_ofb);
	
	// 		update_user_meta($user_id, 'ddwc_driver_licence_front', $l_front[0]);
	// 		update_user_meta($user_id, 'ddwc_driver_licence_back', $l_back[0]);
	// 		update_user_meta($user_id, 'delivery_earning', $delivery_earning);
	
	// 		/*update_user_meta( $user_id, 'billing_country', $drvr_country );
	// 		update_user_meta( $user_id, 'billing_address_1', $dr_street );
	// 		update_user_meta( $user_id, 'billing_address_2', $dr_street2 );
	// 		update_user_meta( $user_id, 'billing_city', $dr_city );
	// 		update_user_meta( $user_id, 'billing_state', $dr_state );
	// 		update_user_meta( $user_id, 'billing_postcode', $dr_zips );*/
	
	// 		$existing = get_user_meta($vendor_id, 'my_drivers', true);
	// 		if ($existing)
	// 		{
	// 			array_push($existing, $user_id);
	// 		}
	// 		else
	// 		{
	// 			$existing = array(
	// 				$user_id
	// 			);
	// 		}
	// 		$result = array_unique($existing);
	
	// 		update_user_meta($vendor_id, 'my_drivers', $result);
	
	// 		$existing_vendor = get_user_meta($user_id, 'driver_vendor', true);
	// 		if ($existing_vendor)
	// 		{
	// 			array_push($existing_vendor, $vendor_id);
	// 		}
	// 		else
	// 		{
	// 			$existing_vendor = array(
	// 				$vendor_id
	// 			);
	// 		}
	// 		$result_vendor = array_unique($existing_vendor);
	// 		update_user_meta($user_id, 'driver_vendor', $result_vendor);
	
	// 		wp_update_user(array(
	// 			'ID' => $user_id,
	// 			'role' => 'driver'
	// 		));
	
	// 		add_filter('wp_mail_content_type', 'set_html_content_type');
	
	// 		//$headers = "Content-Type: text/html\r\n";
	// 		$message .= '<div style="background-color: rgb(255,255,255); overflow: hidden; border-style: solid; border-width: 1.0px; border-color: rgb(222,222,222); border-radius: 3.0px; padding: 0 20px; max-width: 90%; margin: 20px auto;">';
	
	// 		$message .= '<p>Hi ' . $dr_fname . ' ' . $dr_lname . ',';
	
	// 		$message .= '<p>Your email is registered as ' . $blog_name . ' Driver. Your account details are shown below for your reference. Please login using username and password.</p>';
	
	// 		$message .= '<p><strong>Username : ' . $username . ' </strong></p> ';
	
	// 		$message .= '<p><strong>Password : ' . $password . ' </strong></p> ';
	
	// 		$message .= '<p>Please Login using below link. </p> ';
	
	// 		$message .= '<div class=""><a href="' . home_url('my-account') . '" style="background-color: #66ae3d;font-size: 18px;color: #fff;font-weight: 600;padding: 10px;width: 110px;display: block;text-align: center;border-radius: 4px;text-decoration: none;">LOGIN</a></div> ';
	
	// 		$message .= '<p>Thanks for connecting with ' . $blog_name . '.</p> ';
	
	// 		$message .= '<div style="padding: 0;border: 0; text-align: center;font-size: 12px;font-family: Helvetica Neue, Helvetica, Roboto, Arial, sans-serif; font-weight: 400;color: #555555;padding-top: 0px;padding-bottom: 0px">
	// 														<p style="color: #555555">© 2020 ' . $blog_name . '. All Rights Reserved</p>
	// 													</div>';
	// 		$message .= '</div>';
	
	// 		$email = wp_mail($dr_email, $blog_name . ' driver account created', $message);
	
	// 		if ($email != 1)
	// 		{
	// 			wp_send_json_error(__('Email not send for ' . $username . ',  Plase send Account password Mannually Password : ' . $password));
	// 		}
	
	// 		remove_filter('wp_mail_content_type', 'set_html_content_type');
	
	// 	}
	
	   
	// 	wp_send_json_success(array(
	// 		'msg' => __('Driver Registered successfully!') ,
	// 	));
		
	// }else{
		
		//Register Driver if Tera Wallet Selected
		
		
		$vendor_id = dokan_get_current_user_id();
	
		$post_data = wp_unslash($_POST);
	
		$licence_imgfront = sanitize_text_field($post_data['licence_img_front']);
		$licence_img_back = sanitize_text_field($post_data['licence_img_back']);
		$l_front = wp_get_attachment_image_src($licence_imgfront);
		$l_back = wp_get_attachment_image_src($licence_img_back);
	
		$blog_name = get_bloginfo('name');
	
		$nonce = isset($post_data['_wpnonce']) ? $post_data['_wpnonce'] : '';
		// Bail if another settings tab is being saved
		if (!wp_verify_nonce($nonce, 'dokan_about_settings_nonce'))
		{
			return;
		}
	
		$dr_fname = sanitize_text_field($post_data['dr_fname']);
		$dr_lname = sanitize_text_field($post_data['dr_lname']);
		$dr_email = sanitize_text_field($post_data['dr_email']);
		//$dr_phone_cd    = sanitize_text_field( $post_data['dr_phone_cd'] );
		$dr_phone = sanitize_text_field($post_data['dr_phone']);
	
		$date_ofb = sanitize_text_field($post_data['ddwc_driver_dob']);
		$date = strtotime($date_ofb);
		
		$dr_citys = $post_data['ddwc_driver_transportation_city'];
	
		$license_plate = sanitize_text_field($post_data['ddwc_driver_license_plate']);
		$trans_type = sanitize_text_field($post_data['ddwc_driver_transportation_type']);
		$vehical_mdl = sanitize_text_field($post_data['ddwc_driver_vehicle_model']);
		$vehical_clr = sanitize_text_field($post_data['ddwc_driver_vehicle_color']);
		$licance_num = sanitize_text_field($post_data['ddwc_driver_license_number']);
		$delivery_earning = sanitize_text_field($post_data['delivery_earning']);
		
			if (empty($dr_fname) || empty($dr_lname) || empty($dr_email) || empty($dr_phone) || empty($dr_citys) || empty($date_ofb))
			{
				wp_send_json_error(__('All Required Fields should not be blank!'));
			}
			if (4 > strlen($dr_fname))
			{
				wp_send_json_error(__('At least 4 characters is required'));
			}
			
			if (!is_email($dr_email))
			{
				wp_send_json_error(__('Email is not valid'));
			}
		
			if (email_exists($dr_email))
			{
				wp_send_json_error(__('Email Already in use'));
			}
		
			$username = preg_replace('/([^@]*).*/', '$1', $dr_email);
			if (username_exists($username))
			{
				$username = $username . '1';
			}
				
		$password = wp_generate_password(12, true);
	
		$userdata = array(
			'user_login' => $username,
			'user_email' => $dr_email,
			'user_pass' => $password,
			'display_name' => $dr_fname . ' ' . $dr_lname,
	
		);
		$user_id = wp_insert_user($userdata);
		if ($user_id)
		{
	
		 	update_user_meta($user_id, 'ddwc_driver_availability', 'on');
			update_user_meta($user_id, 'phone', $dr_phone);
			update_user_meta($user_id, 'billing_phone', $full_num);
			update_user_meta($user_id, 'first_name', $dr_fname);
			update_user_meta($user_id, 'last_name', $dr_lname);
			update_user_meta($user_id, 'ddwc_driver_license_plate', $license_plate);
			update_user_meta($user_id, 'ddwc_driver_transportation_type', $trans_type);
			update_user_meta($user_id, 'ddwc_driver_vehicle_model', $vehical_mdl);
			update_user_meta($user_id, 'ddwc_driver_vehicle_color', $vehical_clr);
			update_user_meta($user_id, 'ddwc_driver_license_number', $licance_num);
	
			update_user_meta($user_id, 'ddwc_driver_transportation_city', $dr_citys);
			update_user_meta($user_id, 'ddwc_driver_dob', $date_ofb);
	
			update_user_meta($user_id, 'ddwc_driver_licence_front', $l_front[0]);
			update_user_meta($user_id, 'ddwc_driver_licence_back', $l_back[0]);
			update_user_meta($user_id, 'delivery_earning', $delivery_earning);
	
			$existing = get_user_meta($vendor_id, 'my_drivers', true);
			if ($existing)
			{
				array_push($existing, $user_id);
			}
			else
			{
				$existing = array(
					$user_id
				);
			}
			$result = array_unique($existing);
	
			update_user_meta($vendor_id, 'my_drivers', $result);
	
			$existing_vendor = get_user_meta($user_id, 'driver_vendor', true);
			if ($existing_vendor)
			{
				array_push($existing_vendor, $vendor_id);
			}
			else
			{
				$existing_vendor = array(
					$vendor_id
				);
			}
			$result_vendor = array_unique($existing_vendor);
			update_user_meta($user_id, 'driver_vendor', $result_vendor);
	
			wp_update_user(array(
				'ID' => $user_id,
				'role' => 'driver'
			));
	
			add_filter('wp_mail_content_type', 'set_html_content_type');
	
			//$headers = "Content-Type: text/html\r\n";
			$message .= '<div style="background-color: rgb(255,255,255); overflow: hidden; border-style: solid; border-width: 1.0px; border-color: rgb(222,222,222); border-radius: 3.0px; padding: 0 20px; max-width: 90%; margin: 20px auto;">';
	
			$message .= '<p>Hi ' . $dr_fname . ' ' . $dr_lname . ',';
	
			$message .= '<p>Your email is registered as ' . $blog_name . ' Driver. Your account details are shown below for your reference. Please login using username and password.</p>';
	
			$message .= '<p><strong>Username : ' . $username . ' </strong></p> ';
	
			$message .= '<p><strong>Password : ' . $password . ' </strong></p> ';
	
			$message .= '<p>Please Login using below link. </p> ';
	
			$message .= '<div class=""><a href="' . home_url('my-account') . '" style="background-color: #66ae3d;font-size: 18px;color: #fff;font-weight: 600;padding: 10px;width: 110px;display: block;text-align: center;border-radius: 4px;text-decoration: none;">LOGIN</a></div> ';
	
			$message .= '<p>Thanks for connecting with ' . $blog_name . '.</p> ';
	
			$message .= '<div style="padding: 0;border: 0; text-align: center;font-size: 12px;font-family: Helvetica Neue, Helvetica, Roboto, Arial, sans-serif; font-weight: 400;color: #555555;padding-top: 0px;padding-bottom: 0px">
															<p style="color: #555555">© 2020 ' . $blog_name . '. All Rights Reserved</p>
														</div>';
			$message .= '</div>';
	
			$email = wp_mail($dr_email, $blog_name . ' driver account created', $message);
	
			if ($email != 1)
			{
				wp_send_json_error(__('Email not send for ' . $username . ',  Plase send Account password Mannually Password : ' . $password));
			}
	
			remove_filter('wp_mail_content_type', 'set_html_content_type');
	
		}
	
	   
		wp_send_json_success(array(
			'msg' => __('Driver Registered successfully!') ,
		));
		
			
	//}
}

function set_html_content_type()
{
    return 'text/html';
}

add_action('admin_init', 'allow_contributor_uploads');
function allow_contributor_uploads()
{
    $customer = get_role('customer');
    $customer->add_cap('upload_files');
    $customer->add_cap('unfiltered_upload');
}

add_action('wp_enqueue_scripts', 'buyback_enqueued_assets');
function buyback_enqueued_assets()
{
    wp_enqueue_media();
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script('buyback-upload-js', plugin_dir_url(__FILE__) . 'public/js/upload.js');
}

function action_shutdown_new_testew()
{
	/*echo '<pre>';
	print_r(get_user_meta(8, 'delivery_earning', true));
	echo '</pre>';
	echo '<pre>';
	print_r(update_user_meta(9, 'delivery_earning', 'vendor'));
	echo '</pre>';
	echo '<pre>';
	print_r(get_user_meta(10, 'delivery_earning', true));
	echo '</pre>';
	echo '<pre>';
	print_r(get_user_meta(11, 'delivery_earning', true));
	echo '</pre>';*/
};
//add_action( 'shutdown', 'action_shutdown_new_testew');


add_action('wp_ajax_sraech_drivers', 'sraech_drivers');
add_action('wp_ajax_nopriv_sraech_drivers', 'sraech_drivers');
function sraech_drivers()
{

    $vendor_id = $_POST['vendor_id'];
    $search_txt = $_POST['drtext'];
    $users_query = new WP_User_Query(array(
        'search' => "*{$search_txt}*",
        'search_columns' => array(
            'user_login',
            'user_email',
        ) ,
        'fields' => 'all_with_meta',
        'role' => 'driver',
        'orderby' => 'display_name',

        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key' => 'ddwc_driver_availability',
                'value' => 'on',
                'compare' => 'LIKE',
            ) ,
        )

    ));
    $results = $users_query->get_results();
    $error_msg = '';
    if ($results)
    {
        $res .= '<select name="dr_selected">';
        foreach ($results as $user => $data)
        {
            $dr_id = $data
                ->data->ID;
            $existing = get_user_meta($dr_id, 'driver_vendor', true);
            if (in_array($vendor_id, $existing))
            {
                $error_msg = '<span>Driver already added.</span>';
            }
            else
            {
                $dr_name = $data
                    ->data->display_name;
                $res .= '<option value="' . $dr_id . '">' . $dr_name . '</option>';
            }

        }
        $res .= '</select>';
    }
    else
    {
        $res = '<span>No Driver Found! </span>';
    }
    if (empty($error_msg))
    {
        echo $res;
    }
    else
    {
        echo $error_msg;
    }

    exit;

};

if (isset($_POST['dokan_add_driver']))
{
    if (!isset($_POST['dr_selected']))
    {
        return;
    }
    $new_dr = $_POST['dr_selected'];

    if (!function_exists('get_user_by'))
    {
        include (ABSPATH . "wp-includes/pluggable.php");
    }
    $vendor_id = $_POST['currentvendor_id'];

    $existing = get_user_meta($vendor_id, 'my_drivers', true);

    $existing_vendor = get_user_meta($new_dr, 'driver_vendor', true);

    if ($existing_vendor)
    {
        array_push($existing_vendor, $vendor_id);
    }
    else
    {
        $existing_vendor = array(
            $vendor_id
        );
    }
    $result_dr = array_unique($existing_vendor);

    if ($existing)
    {
        array_push($existing, $new_dr);
    }
    else
    {
        $existing = array(
            $new_dr
        );
    }
    $result = array_unique($existing);

    update_user_meta($vendor_id, 'my_drivers', $result);
    update_user_meta($new_dr, 'driver_vendor', $result_dr);

}

add_action('wp_ajax_show_driver_details', 'show_driver_details');
add_action('wp_ajax_nopriv_show_driver_details', 'show_driver_details');
function show_driver_details()
{

    $driver_id = $_POST['driver_id'];

    $user = get_userdata($driver_id);
    $driver_image = get_user_meta($driver_id, 'ddwc_driver_picture', true);

    if (empty($driver_image))
    {
        $driver_image_url = plugin_dir_url(__FILE__) . '/images/default-user-icon.jpg';
    }
    else
    {
        $driver_image_url = $driver_image['url'];
    }

    $userdata = get_user_meta($driver_id);

    if ($user)
    {
        $res .= '<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_popup();"><i class="fa fa-times">&nbsp;</i></button>
        <h4 class="modal-title" id="myModalLabel">More About ' . $user
            ->data->display_name . '</h4>
      </div>
      <div class="modal-body">
        <center>
          <img src="' . $driver_image_url . '" name="aboutme" width="140" height="140" border="0" class="img-circle">
          <h3 class="media-heading">' . $userdata['first_name'][0] . ' ' . $userdata['last_name'][0] . '</h3>
		</center>  
        <div style="text-align: left;"><span><strong>License Plate Number : </strong></span><span class="label label-success">' . $userdata['ddwc_driver_license_plate'][0] . '</span></div>
		<div style="text-align: left;"><span><strong>Transportation Type : </strong></span><span class="label label-success">' . $userdata['ddwc_driver_transportation_type'][0] . '</span></div>
		<div style="text-align: left;"><span><strong>Vehicle Model : </strong></span><span class="label label-success">' . $userdata['ddwc_driver_vehicle_model'][0] . '</span></div>
		<div style="text-align: left;"><span><strong>Vehicle Color : </strong></span><span class="label label-success">' . $userdata['ddwc_driver_vehicle_color'][0] . '</span></div>
        
        <hr>
        <!--<center>
          <p class="text-left"><strong>Bio: </strong><br>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sem dui, tempor sit amet commodo a, vulputate vel tellus.</p>
          <br>
        </center>-->
      </div>
      <!--<div class="modal-footer">
        <center>
          <button type="button" class="btn btn-default" data-dismiss="modal">I\'ve heard enough about Joe</button>
        </center>
      </div>-->
    </div>';
    }
    else
    {
        $res = 'No Drivers details Found!';
    }
    echo $res;

    exit;

};

add_action('wp_ajax_removeDriverFromList', 'removeDriverFromList');
add_action('wp_ajax_nopriv_removeDriverFromList', 'removeDriverFromList');
function removeDriverFromList()
{

    $driver_id = $_POST['driver_id'];
    $vendor_id = get_current_user_id();

    $existing = get_user_meta($vendor_id, 'my_drivers', true);
    $existing_vendors = get_user_meta($driver_id, 'driver_vendor', true);

    if ($driver_id)
    {
        if (in_array($driver_id, $existing))
        {
            unset($existing[array_search($driver_id, $existing) ]);
        }
        update_user_meta($vendor_id, 'my_drivers', $existing);
        if (in_array($vendor_id, $existing_vendors))
        {
            unset($existing_vendors[array_search($vendor_id, $existing_vendors) ]);
        }
        update_user_meta($driver_id, 'driver_vendor', $existing_vendors);
    }

    echo 'Driver removed from list!';
    exit;
}



add_action('wp_ajax_get_neighbourhood_citys', 'get_neighbourhood_citys');
add_action('wp_ajax_nopriv_get_neighbourhood_citys', 'get_neighbourhood_citys');

function get_neighbourhood_citys(){
	$arg = array(
				'taxonomy' => 'area',
				'parent' => $_POST['parent'],
				'hide_empty' => false
			);
	$all_parent = get_terms($arg);
	ob_start();
	foreach ($all_parent as $key => $term) {
		?>
		<option value="<?php echo $term->name;?>"> <?php echo $term->name;?></option>
		<?php 
	}
	$html =ob_get_clean();
	$response = json_encode(array(
		   'city' =>$html,
	));
	header("Content-Type: application/json");
	$response;
	echo $response;
	exit;
}
add_action('wp_footer', 'get_neighbourhood_citys_ajax');
function get_neighbourhood_citys_ajax(){
	?>
	<script>
	jQuery('#ddwc_neighbourhood_city').change(function () {
		jQuery('#ddwc_driver_transportation_city').html('<option disable>Loading...</option>');
		var id = jQuery(this).val();
		jQuery.ajax({
			type: 'POST',
			url: "<?php echo admin_url( 'admin-ajax.php' );?>",
			data: {
				'action': 'get_neighbourhood_citys',
				'parent': id,
			},
			success: function (data) {
				jQuery('#ddwc_driver_transportation_city').html(data.city);
				console.log(data.city);
			}
		});

	});
	
	</script>
	<?php
}
require_once ('config.php');

