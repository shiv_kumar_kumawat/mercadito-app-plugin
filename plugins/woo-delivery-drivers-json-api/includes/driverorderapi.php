<?php
include_once (ABSPATH . 'wp-admin/includes/plugin.php');
if (!is_plugin_active('woocommerce/woocommerce.php'))
{
    return;
}
class WPJsonDriverOrderApi
{
    public function __construct()
    {
        add_action('rest_api_init', array(
            $this,
            'JsonDriverOrderApi'
        ));
    }

    public function JsonDriverOrderApi()
    {
        /* Action APi */
        register_rest_route('driverorderapi', '/accept_order', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'accept_order'
            ) ,
        ));
        register_rest_route('driverorderapi', '/reject_order', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'reject_order'
            ) ,
        ));
        register_rest_route('driverorderapi', '/out_for_delivery', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'out_for_delivery'
            ) ,
        ));
        register_rest_route('driverorderapi', '/resend_otp', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'resend_otp'
            ) ,
        ));
        register_rest_route('driverorderapi', '/order_return', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'order_return'
            ) ,
        ));

        register_rest_route('driverorderapi', '/order_complete', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'order_complete'
            ) ,
        ));

        /* Listing api */
        register_rest_route('driverorderapi', '/get_pickup_request', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'get_pickup_request'
            ) ,
        ));
        register_rest_route('driverorderapi', '/get_accepted_orders', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'get_accepted_orders'
            ) ,
        ));
        register_rest_route('driverorderapi', '/get_out_for_delivery_orders', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'get_out_for_delivery_orders'
            ) ,
        ));
        register_rest_route('driverorderapi', '/get_returned_orders', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'get_returned_orders'
            ) ,
        ));
        register_rest_route('driverorderapi', '/get_completed_orders', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'get_completed_orders'
            ) ,
        ));
        register_rest_route('driverorderapi', '/get_earnings', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'get_earnings'
            ) ,
        ));
        register_rest_route('driverorderapi', '/withdraw_request', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'withdraw_request'
            ) ,
        ));
        register_rest_route('driverorderapi', '/single_order', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'single_order'
            ) ,
        ));
    }

    public function withdraw_request($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $driver_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$driver_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
		
		/*$payment_type  =  get_option( 'ddwc_driver_payment_type' );

		if($payment_type == 'terawallet'){
			$response['status'] = "error";
			$response['message'] = apply_filters('terawallet_withdraw_msg',"Go to your dashboard to withdraw your balance.");
			return new WP_REST_Response($response, 200);
		}*/
		
        if (!$amount)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'amount' var in your request.";
            $response['message'] = "Please enter amount.";

            return new WP_REST_Response($response, 200);
        }

        global $wpdb;

        $previous = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}delivery_driver_commision_withdraw WHERE driver_id =$driver_id AND status = 0");

        if (count($previous))
        {
            $response['status'] = "error";
            $response['canapply'] = 0;
            $response['pending request'] = $previous->amount;
            $response['message'] = "You already have pending request of amount $previous->amount.";
            return new WP_REST_Response($response, 200);
        }
        $response['canapply'] = 1;
        $on_date = date("Y-m-d H:i:s");
        $result = $wpdb->get_row($wpdb->prepare("SELECT SUM(debit) as earnings,
		( SELECT SUM(credit) FROM {$wpdb->prefix}delivery_driver_commision WHERE driver_id = %d AND DATE(created) <= %s ) as withdraw
		from {$wpdb->prefix}delivery_driver_commision
		WHERE driver_id = %d AND DATE(created) <= %s", $driver_id, $on_date, $driver_id, $on_date));
        $balance = $result->earnings - $result->withdraw;

        if ($amount <= $balance)
        {
            $status = 0;
            $wpdb->insert("{$wpdb->prefix}delivery_driver_commision_withdraw", array(
                'driver_id' => $driver_id,
                'amount' => $amount,
                'status' => $status,
            ));
            $response['message'] = "Withdrawal request successfully placed for amount $amount.";
            $response['status'] = "success";
        }
        else
        {
            $response['status'] = "error";
            $response['message'] = "Insufficient balance.";
        }

        return new WP_REST_Response($response, 200);

    }

    public function get_earnings($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $driver_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$driver_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }

        global $wpdb;
        $on_date = date("Y-m-d H:i:s");

        $result = $wpdb->get_results("SELECT order_id FROM {$wpdb->prefix}delivery_driver_commision WHERE driver_id =$driver_id AND credit = 0");

        $response['total delivery'] = count($result);

        $result2 = $wpdb->get_row($wpdb->prepare("SELECT SUM(debit) as earnings,
		( SELECT SUM(credit) FROM {$wpdb->prefix}delivery_driver_commision WHERE driver_id = %d AND DATE(created) <= %s ) as withdraw
		from {$wpdb->prefix}delivery_driver_commision
		WHERE driver_id = %d AND DATE(created) <= %s", $driver_id, $on_date, $driver_id, $on_date));
        $current_balance = $result2->earnings - $result2->withdraw;
        if (0 <= $result2->earnings)
        {
            $earnings = round($result2->earnings, 2);
        }
        else
        {
            $earnings = 0;
        }
        if (0 <= $result2->withdraw)
        {
            $withdraw = round($result2->withdraw, 2);
        }
        else
        {
            $withdraw = 0.00;
        }
        if ($current_balance > 0)
        {
            $current_balance = round($current_balance, 2);
        }
        else
        {
            $current_balance = 0;
        }
        $response['total_earning'] = $earnings;
        $response['total_withdraw'] = $withdraw;
        $response['total_remaning'] = $current_balance;
        $response['canapply'] = 1;
        $response['button'] = 1;

        $response['message'] = "Here Is Your Earning data.";

        $result3 = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}delivery_driver_commision_withdraw WHERE driver_id =$driver_id AND status = 0");
        if (!$current_balance)
        {
            $response['canapply'] = 0;
            $response['message'] = "You have no balance in account.";

        }
        if (count($result3))
        {
            $response['canapply'] = 0;
            $response['pending request'] = round($result3->amount,2);
            $response['message'] = "You already have pending request of amount $result3->amount.";

        }

        $response['status'] = "success";

        $payment_type = get_option('ddwc_driver_payment_type');
        if ($payment_type == 'terawallet')
        {
            $response['payment_menu'] = 'no';
            $response['canapply'] = 0;
            $response['button'] = 0;
            $response['message'] = apply_filters('terawallet_withdraw_msg',"Go to your dashboard to withdraw your balance.");

        }
        if ($payment_type == 'stripe')
        {
            $response['payment_menu'] = 'yes';
        }

        return new WP_REST_Response($response, 200);

    }
    public function accept_order($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
		
		$payment_type  =  get_option( 'ddwc_driver_payment_type' );
	
		if($payment_type == 'stripe'){
			
			$response['driver-type'] = "external";
			$checkstripe = true;
			
			/* Check if inhouse or external */
			
			if( !empty( get_user_meta($user_id, 'driver_vendor', true) ) ){
				
				/* Driver is inhouse */
				$response['earning-goes-to'] = "driver";
				$response['driver-type'] = "in house";
				
				if( get_user_meta($user_id, 'delivery_earning', true) == 'vendor'){
					
					/* Driver earning will go to vendor */
					$response['earning-goes-to'] = "vendor";
					$checkstripe = false;
					
				}
				
			}
			
			if($checkstripe){
				
				if (!get_user_meta($user_id, 'stripe_account_id', true))
				{
					$response['status'] = "error";
					$response['message'] = "Please enter account details first for accepting orders.";
					return new WP_REST_Response($response, 200);
				}
				$response['Stripe Account ID'] = get_user_meta($user_id, 'stripe_account_id', true);

			}
			
		}
		
		if (get_option('ddwc_police_check_enable') == 'yes')
        {
			if (empty( get_user_meta($user_id, 'police_check_id', true) ) ){
				
				$user_info = get_userdata($user_id);
				$pc_response = self::callWpDriverAPI('POST', get_site_url().'/wp-json/driver_police_check/create_check', json_encode(
					array(
						'first_name' => $user_info->user_firstname,
						'last_name' => $user_info->last_name,
						'email' => $user_info->user_email,
						'type' => 'EMPLOYMENT',
						'reason' => 'Delivery Driver',
					)
				));
				
				$arr =	json_decode($pc_response);
				
				update_user_meta($user_id,'police_check_id',$arr->id);
				update_user_meta($user_id,'police_check_continue_url',$arr->continue_url);
				
				$response['message'] = "Your details are sent to police check verification";
				$response['status'] = "error";
				return new WP_REST_Response($response, 200);	
				
			}else{
				
				if ( !empty( get_user_meta($user_id, 'police_check_status', true) ) ){
					$policestatus	=	get_user_meta($user_id, 'police_check_status', true);
					
					/* if($policestatus	==	'id-verify-done'){
						$response['message'] = ".";
					} */
					/* if($policestatus 	==	'service-result'){
						$response['message'] = ".";
					} */
					if($policestatus	==	'id-verify-applicant'){
						$response['message'] = "Current Police check status is applicant verified";
						$response['status'] = "error";
						return new WP_REST_Response($response, 200);	
					}
					
					if($policestatus 	==	'id-verify-qa'){
						$response['message'] = "Current Police check status is QA verified";
						$response['status'] = "error";
						return new WP_REST_Response($response, 200);	
					}
					
					
				}else{
					
					$response['status'] = "error";
					$response['message'] = "Police check still not verify your details.";
					return new WP_REST_Response($response, 200);
					
				}
				
			}
           
        }
		
        if (empty($order_id))
        {
            $response['status'] = "error";
            $response['message'] = "Missing Order ID.";
            return new WP_REST_Response($response, 200);
        }
        $driverstatus = get_post_meta($order_id, 'ddwc_driver_id', true);
        global $wpdb;
        global $woocommerce;
        if (empty($driverstatus) or $driverstatus < 1)
        {

            update_post_meta($order_id, 'ddwc_driver_id', $user_id);

            $wpdb->delete("{$wpdb->prefix}driver_notification", array(
                'order_id' => $order_id
            ));
            $order = wc_get_order($order_id);
            $order->update_status('driver-assigned');
            $order->save();

            //update_post_meta($order_id, 'assignedORaccepted', 'accepted');
            $response['status'] = "success";
            $response['message'] = 'Delivery Driver Assign To Order:#' . $order_id;
        }
        else
        {
            $wpdb->delete("{$wpdb->prefix}driver_notification", array(
                'order_id' => $order_id
            ));
            $response['status'] = "error";
            $response['message'] = 'Order:#' . $order_id . ' Already Assigned To Other Delivery Driver. Please refresh.';
        }
        return new WP_REST_Response($response, 200);
    }

    public function reject_order($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
        if (empty($order_id))
        {
            $response['status'] = "error";
            $response['message'] = "Missing Order ID.";
            return new WP_REST_Response($response, 200);
        }

        global $wpdb;
        $wpdb->delete("{$wpdb->prefix}driver_notification", array(
            'order_id' => $order_id,
            'driver_id' => $user_id
        ));

        $response['order_id'] = $order_id;
        $response['user_id'] = $user_id;
        $response['status'] = "success";
        $response['message'] = 'Order:#' . $order_id . ' Rejected By You.';

        return new WP_REST_Response($response, 200);
    }

    public function out_for_delivery($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
        if (empty($order_id))
        {
            $response['status'] = "error";
            $response['message'] = "Missing Order ID.";
            return new WP_REST_Response($response, 200);
        }
        if (get_post_meta($order_id, 'ddwc_driver_id', true) != $user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid driver.";
            return new WP_REST_Response($response, 200);
        }

        global $woocommerce;

        $order = wc_get_order($order_id);
        $order->update_status('out-for-delivery');
        $order->save();

        $response['status'] = "success";
        $response['message'] = 'Order:#' . $order_id . ' Is Out For delivery.';

        return new WP_REST_Response($response, 200);
    }

    public function resend_otp($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
        if (empty($order_id))
        {
            $response['status'] = "error";
            $response['message'] = "Missing Order ID.";
            return new WP_REST_Response($response, 200);
        }
        if (get_post_meta($order_id, 'ddwc_driver_id', true) != $user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid driver.";
            return new WP_REST_Response($response, 200);
        }

        global $woocommerce;

        $order = wc_get_order($order_id);
        $order->update_status('driver-assigned');
        $order->save();

        $order = wc_get_order($order_id);
        $order->update_status('out-for-delivery');
        $order->save();

        $response['status'] = "success";
        $response['message'] = 'Otp Send to Registered Mobile Number ' . $order->get_billing_phone() . ' of Order #' . $order_id;

        return new WP_REST_Response($response, 200);
    }

    public function order_return($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
        if (empty($order_id))
        {
            $response['status'] = "error";
            $response['message'] = "Missing Order ID.";
            return new WP_REST_Response($response, 200);
        }
        if (get_post_meta($order_id, 'ddwc_driver_id', true) != $user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid driver.";
            return new WP_REST_Response($response, 200);
        }

        global $woocommerce;
        $order = wc_get_order($order_id);
        $order->update_status('order-returned');
        $order->save();

        $response['status'] = "success";
        $response['message'] = 'Order:#' . $order_id . ' Is Returned.';

        return new WP_REST_Response($response, 200);
    }

    public function order_complete($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
        if (empty($order_id))
        {
            $response['status'] = "error";
            $response['message'] = "Missing Order ID.";
            return new WP_REST_Response($response, 200);
        }
        if (empty($otp))
        {
            $response['status'] = "error";
            $response['message'] = "Missing Otp.";
            return new WP_REST_Response($response, 200);
        }
        if (get_post_meta($order_id, 'ddwc_driver_id', true) != $user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid driver.";
            return new WP_REST_Response($response, 200);
        }

        if (get_post_meta($order_id, 'otp', true) != $otp)
        {
            $response['status'] = "error";
            $response['message'] = "Otp Is Not Correct.";
            return new WP_REST_Response($response, 200);
        }

        global $woocommerce;
        $order = wc_get_order($order_id);
        $order->update_status('completed');
        $order->save();

        $response['status'] = "success";
        $response['message'] = 'Order:#' . $order_id . ' Is Delivered.';

        return new WP_REST_Response($response, 200);
    }

    public function get_pickup_request($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }

        $completed_orders = wc_get_orders(array(
            'limit' => - 1,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_key' => 'ddwc_driver_id',
            'meta_value' => $user_id,
            'meta_compare' => '=',
            'status' => 'wc-completed',
        ));
        $completed_orders = count($completed_orders);
		$ourfordelivery = wc_get_orders(array(
            'limit' => - 1,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_key' => 'ddwc_driver_id',
            'meta_value' => $user_id,
            'meta_compare' => '=',
            'status' => 'wc-out-for-delivery',
        ));
        $driverassigned = wc_get_orders(array(
            'limit' => - 1,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_key' => 'ddwc_driver_id',
            'meta_value' => $user_id,
            'meta_compare' => '=',
            'status' => 'wc-driver-assigned',
        ));
        $pending_orders = count($ourfordelivery)+count($driverassigned);

        $response['completed orders'] = $completed_orders;
        $response['Pending orders'] = $pending_orders;
        
		global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}driver_notification WHERE driver_id =$user_id");
		$current_user_name=get_user_meta($user_id,'nickname',true);
		
        $orders_pre = array();
        foreach ($result as $row)
        {
            $orders_pre[] = $row->order_id;
        }
		

        if (count($orders_pre))
        {
			$limit=2;
			if($page){
				$paged=$page;
			}else{
				$paged=1;
			}
			$response['page'] = $paged;

            $orders = wc_get_orders(array(
                'limit' => $limit,
				'paged' => $paged,
                'orderby' => 'date',
                'order' => 'DESC',
                'post__in' => $orders_pre,
            ));
			if (count($orders))
			{
				$orderdetails = array();

				foreach ($orders as $order)
				{
					$id = $order->get_id();

					/* $orderdetails[$id]['billing_first_name'] = $order->get_billing_first_name();
					$orderdetails[$id]['billing_last_name'] = $order->get_billing_last_name();
					$orderdetails[$id]['billing_company'] = $order->get_billing_company();
					$orderdetails[$id]['billing_address_1'] = $order->get_billing_address_1();
					$orderdetails[$id]['billing_address_2'] = $order->get_billing_address_2();
					$orderdetails[$id]['billing_city'] = $order->get_billing_city();
					$orderdetails[$id]['billing_state'] = $order->get_billing_state();
					$orderdetails[$id]['billing_postcode'] = $order->get_billing_postcode();
					$orderdetails[$id]['billing_country'] = $order->get_billing_country();
					$orderdetails[$id]['billing_email'] = $order->get_billing_email();
					$orderdetails[$id]['billing_phone'] = $order->get_billing_phone();
					$orderdetails[$id]['shipping_first_name'] = $order->get_shipping_first_name();
					$orderdetails[$id]['shipping_last_name'] = $order->get_shipping_last_name();
					$orderdetails[$id]['shipping_company'] = $order->get_shipping_company();
					$orderdetails[$id]['shipping_address_1'] = $order->get_shipping_address_1();
					$orderdetails[$id]['shipping_address_2'] = $order->get_shipping_address_2();
					$orderdetails[$id]['shipping_city'] = $order->get_shipping_city();
					$orderdetails[$id]['shipping_state'] = $order->get_shipping_state();
					$orderdetails[$id]['get_order_key'] = $order->get_shipping_postcode();
					$orderdetails[$id]['shipping_postcode'] = $order->get_shipping_country();
					$orderdetails[$id]['address'] = $order->get_address();
					$orderdetails[$id]['shipping_address_map_url'] = $order->get_shipping_address_map_url();
					$orderdetails[$id]['formatted_billing_full_name'] = $order->get_formatted_billing_full_name();
					$orderdetails[$id]['formatted_shipping_full_name'] = $order->get_formatted_shipping_full_name();
					$orderdetails[$id]['formatted_billing_address'] = $order->get_formatted_billing_address();
					$orderdetails[$id]['formatted_shipping_address'] = $order->get_formatted_shipping_address();
					
					$orderdetails[$id]['fees'] = $order->get_fees();
					$orderdetails[$id]['subtotal'] = $order->get_subtotal();
					$orderdetails[$id]['discount'] = $order->get_total_discount();
					$orderdetails[$id]['tax'] = $order->get_taxes();
					$orderdetails[$id]['total'] = $order->get_total();
					
					$orderdetails[$id]['shipping_method'] = $order->get_shipping_method();
					$orderdetails[$id]['shipping_methods'] = $order->get_shipping_methods();
					$orderdetails[$id]['shipping_to_display'] = $order->get_shipping_to_display();
					$orderdetails[$id]['status'] = $order->get_status();;
					*/
					$seller_id = $seller = dokan_get_seller_id_by_order($id);
					$profile_info = dokan_get_store_info($seller_id);

					$pickup_address = '';
					$pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

					if (empty(str_replace(",", "", str_replace(" ", "", $pickup_address))))
					{
						$orderdetails[$id]['pickup_address'] = null;
						$orderdetails[$id]['pickup_full_address'] = null;
					}
					else
					{
						$orderdetails[$id]['pickup_address'] = $pickup_address;
						$orderdetails[$id]['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['zip'] . ',' . $profile_info['address']['city'] . ',' . $profile_info['address']['state'] . ',' . $profile_info['address']['country'];
						$orderdetails[$id]['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];
					}
					$orderdetails[$id]['stornename'] = $profile_info['store_name'];

					$map_pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

					$orderdetails[$id]['pickup_phone'] = $profile_info['phone'];
					$orderdetails[$id]['pickup_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode(implode(', ', $profile_info['address']));
					$orderdetails[$id]['pickup_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($map_pickup_address);

					$delivery_address = '';
					$delivery_address = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country();

					if (empty(str_replace(",", "", str_replace(" ", "", $delivery_address))))
					{
						$orderdetails[$id]['delivery_address'] = null;
						$orderdetails[$id]['delivery_full_address'] = null;
					}
					else
					{
						$orderdetails[$id]['delivery_address'] = $delivery_address;
						$orderdetails[$id]['delivery_full_address'] = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ' \n' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country() . ' \n' . $order->get_billing_email();
					}

					$orderdetails[$id]['customername'] = $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name();

					$orderdetails[$id]['delivery_phone'] = $order->get_billing_phone();;
					$orderdetails[$id]['delivery_map'] = $order->get_shipping_address_map_url();
					$orderdetails[$id]['delivery_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country());

					$orderdetails[$id]['billing_full_address'] = $order->get_billing_address_1() . ' ' . $order->get_billing_address_2() . ' \n' . $order->get_billing_city() . ' ' . $order->get_billing_state() . ' ' . $order->get_billing_postcode() . ',' . $order->get_billing_country() . ' \n' . $order->get_billing_email();

					$orderdetails[$id]['customer_note'] = $order->get_customer_note();
					$orderdetails[$id]['payment_method'] = $order->get_payment_method();
					$orderdetails[$id]['payment_method_title'] = $order->get_payment_method_title();
					$orderdetails[$id]['total'] = $order->get_currency() . ' ' . $order->get_total();

					$orderdetails[$id]['date_created'] = $order->get_date_created();
					$orderdetails[$id]['date_completed'] = $order->get_date_completed();
					$orderdetails[$id]['date_paid'] = $order->get_date_paid();

					$orderdetails[$id]['get_customer_id'] = $order->get_customer_id();
					$orderdetails[$id]['get_user_id'] = $order->get_user_id();

					$orderdetails[$id]['status'] = $order->get_status();;
				}
				$response['user_id'] = $user_id;
				$response['user_name'] =$current_user_name;
				$response['orders'] = $orderdetails;
				$response['status'] = "success";
				return new WP_REST_Response($response, 200);
			}else{
				$response['user_id'] = $user_id;
				$response['orders'] = [];
				$response['message'] = "No More Orders";

				$response['status'] = "success";
				return new WP_REST_Response($response, 200);
			}
        }
        else
        {
            $response['user_id'] = $user_id;
            $response['orders'] = [];
			$response['message'] = "No More Orders";

            $response['status'] = "success";
            return new WP_REST_Response($response, 200);
        }
    }


    public function get_accepted_orders($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');

        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
		$limit=2;
		if($page){
			$paged=$page;
		}else{
			$paged=1;
		}
		$response['page'] = $paged;
        $orders = wc_get_orders(array(
			'limit' => $limit,
			'paged' => $paged,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_key' => 'ddwc_driver_id',
            'meta_value' => $user_id,
            'meta_compare' => '=',
            'status' => 'wc-driver-assigned',
        ));
        if (count($orders))
        {
            $orderdetails = array();

            foreach ($orders as $order)
            {
                $id = $order->get_id();

                /* $orderdetails[$id]['billing_first_name'] = $order->get_billing_first_name();
                $orderdetails[$id]['billing_last_name'] = $order->get_billing_last_name();
                $orderdetails[$id]['billing_company'] = $order->get_billing_company();
                $orderdetails[$id]['billing_address_1'] = $order->get_billing_address_1();
                $orderdetails[$id]['billing_address_2'] = $order->get_billing_address_2();
                $orderdetails[$id]['billing_city'] = $order->get_billing_city();
                $orderdetails[$id]['billing_state'] = $order->get_billing_state();
                $orderdetails[$id]['billing_postcode'] = $order->get_billing_postcode();
                $orderdetails[$id]['billing_country'] = $order->get_billing_country();
                $orderdetails[$id]['billing_email'] = $order->get_billing_email();
                $orderdetails[$id]['billing_phone'] = $order->get_billing_phone();
                $orderdetails[$id]['shipping_first_name'] = $order->get_shipping_first_name();
                $orderdetails[$id]['shipping_last_name'] = $order->get_shipping_last_name();
                $orderdetails[$id]['shipping_company'] = $order->get_shipping_company();
                $orderdetails[$id]['shipping_address_1'] = $order->get_shipping_address_1();
                $orderdetails[$id]['shipping_address_2'] = $order->get_shipping_address_2();
                $orderdetails[$id]['shipping_city'] = $order->get_shipping_city();
                $orderdetails[$id]['shipping_state'] = $order->get_shipping_state();
                $orderdetails[$id]['get_order_key'] = $order->get_shipping_postcode();
                $orderdetails[$id]['shipping_postcode'] = $order->get_shipping_country();
                $orderdetails[$id]['address'] = $order->get_address();
                $orderdetails[$id]['shipping_address_map_url'] = $order->get_shipping_address_map_url();
                $orderdetails[$id]['formatted_billing_full_name'] = $order->get_formatted_billing_full_name();
                $orderdetails[$id]['formatted_shipping_full_name'] = $order->get_formatted_shipping_full_name();
                $orderdetails[$id]['formatted_billing_address'] = $order->get_formatted_billing_address();
                $orderdetails[$id]['formatted_shipping_address'] = $order->get_formatted_shipping_address();
                
                $orderdetails[$id]['fees'] = $order->get_fees();
                $orderdetails[$id]['subtotal'] = $order->get_subtotal();
                $orderdetails[$id]['discount'] = $order->get_total_discount();
                $orderdetails[$id]['tax'] = $order->get_taxes();
                $orderdetails[$id]['total'] = $order->get_total();
                
                $orderdetails[$id]['shipping_method'] = $order->get_shipping_method();
                $orderdetails[$id]['shipping_methods'] = $order->get_shipping_methods();
                $orderdetails[$id]['shipping_to_display'] = $order->get_shipping_to_display();
                $orderdetails[$id]['status'] = $order->get_status();;
                */
                $seller_id = $seller = dokan_get_seller_id_by_order($id);
                $profile_info = dokan_get_store_info($seller_id);

                $pickup_address = '';
                $pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

                if (empty(str_replace(",", "", str_replace(" ", "", $pickup_address))))
                {
                    $orderdetails[$id]['pickup_address'] = null;
                    $orderdetails[$id]['pickup_full_address'] = null;
                }
                else
                {
                    $orderdetails[$id]['pickup_address'] = $pickup_address;
                    $orderdetails[$id]['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['zip'] . ',' . $profile_info['address']['city'] . ',' . $profile_info['address']['state'] . ',' . $profile_info['address']['country'];
                    $orderdetails[$id]['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];
                }
                $orderdetails[$id]['stornename'] = $profile_info['store_name'];

                $map_pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

                $orderdetails[$id]['pickup_phone'] = $profile_info['phone'];
                $orderdetails[$id]['pickup_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode(implode(', ', $profile_info['address']));
                $orderdetails[$id]['pickup_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($map_pickup_address);

                $delivery_address = '';
                $delivery_address = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country();

                if (empty(str_replace(",", "", str_replace(" ", "", $delivery_address))))
                {
                    $orderdetails[$id]['delivery_address'] = null;
                    $orderdetails[$id]['delivery_full_address'] = null;
                }
                else
                {
                    $orderdetails[$id]['delivery_address'] = $delivery_address;
                    $orderdetails[$id]['delivery_full_address'] = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ' \n' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country() . ' \n' . $order->get_billing_email();
                }

                $orderdetails[$id]['customername'] = $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name();

                $orderdetails[$id]['delivery_phone'] = $order->get_billing_phone();;
                $orderdetails[$id]['delivery_map'] = $order->get_shipping_address_map_url();
                $orderdetails[$id]['delivery_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country());

                $orderdetails[$id]['billing_full_address'] = $order->get_billing_address_1() . ' ' . $order->get_billing_address_2() . ' \n' . $order->get_billing_city() . ' ' . $order->get_billing_state() . ' ' . $order->get_billing_postcode() . ',' . $order->get_billing_country() . ' \n' . $order->get_billing_email();

                $orderdetails[$id]['customer_note'] = $order->get_customer_note();
                $orderdetails[$id]['payment_method'] = $order->get_payment_method();
                $orderdetails[$id]['payment_method_title'] = $order->get_payment_method_title();
                $orderdetails[$id]['total'] = $order->get_currency() . ' ' . $order->get_total();

                $orderdetails[$id]['date_created'] = $order->get_date_created();
                $orderdetails[$id]['date_completed'] = $order->get_date_completed();
                $orderdetails[$id]['date_paid'] = $order->get_date_paid();

                $orderdetails[$id]['get_customer_id'] = $order->get_customer_id();
                $orderdetails[$id]['get_user_id'] = $order->get_user_id();

                $orderdetails[$id]['status'] = $order->get_status();;
            }

            $response['status'] = "success";
            $response['orders'] = $orderdetails;
            return new WP_REST_Response($response, 200);
        }
        else
        {
            $response['status'] = "success";
			$response['message'] = "No More Orders";
            $response['orders'] = [];
            return new WP_REST_Response($response, 200);
        }
    }

    public function get_out_for_delivery_orders($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
		$limit=2;
		if($page){
			$paged=$page;
		}else{
			$paged=1;
		}
		$response['page'] = $paged;
        $orders = wc_get_orders(array(
            'limit' => $limit,
			'paged' => $paged,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_key' => 'ddwc_driver_id',
            'meta_value' => $user_id,
            'meta_compare' => '=',
            'status' => 'wc-out-for-delivery',
        ));
        if (count($orders))
        {
            $orderdetails = array();

            foreach ($orders as $order)
            {
                $id = $order->get_id();

                /* $orderdetails[$id]['billing_first_name'] = $order->get_billing_first_name();
                $orderdetails[$id]['billing_last_name'] = $order->get_billing_last_name();
                $orderdetails[$id]['billing_company'] = $order->get_billing_company();
                $orderdetails[$id]['billing_address_1'] = $order->get_billing_address_1();
                $orderdetails[$id]['billing_address_2'] = $order->get_billing_address_2();
                $orderdetails[$id]['billing_city'] = $order->get_billing_city();
                $orderdetails[$id]['billing_state'] = $order->get_billing_state();
                $orderdetails[$id]['billing_postcode'] = $order->get_billing_postcode();
                $orderdetails[$id]['billing_country'] = $order->get_billing_country();
                $orderdetails[$id]['billing_email'] = $order->get_billing_email();
                $orderdetails[$id]['billing_phone'] = $order->get_billing_phone();
                $orderdetails[$id]['shipping_first_name'] = $order->get_shipping_first_name();
                $orderdetails[$id]['shipping_last_name'] = $order->get_shipping_last_name();
                $orderdetails[$id]['shipping_company'] = $order->get_shipping_company();
                $orderdetails[$id]['shipping_address_1'] = $order->get_shipping_address_1();
                $orderdetails[$id]['shipping_address_2'] = $order->get_shipping_address_2();
                $orderdetails[$id]['shipping_city'] = $order->get_shipping_city();
                $orderdetails[$id]['shipping_state'] = $order->get_shipping_state();
                $orderdetails[$id]['get_order_key'] = $order->get_shipping_postcode();
                $orderdetails[$id]['shipping_postcode'] = $order->get_shipping_country();
                $orderdetails[$id]['address'] = $order->get_address();
                $orderdetails[$id]['shipping_address_map_url'] = $order->get_shipping_address_map_url();
                $orderdetails[$id]['formatted_billing_full_name'] = $order->get_formatted_billing_full_name();
                $orderdetails[$id]['formatted_shipping_full_name'] = $order->get_formatted_shipping_full_name();
                $orderdetails[$id]['formatted_billing_address'] = $order->get_formatted_billing_address();
                $orderdetails[$id]['formatted_shipping_address'] = $order->get_formatted_shipping_address();
                
                $orderdetails[$id]['fees'] = $order->get_fees();
                $orderdetails[$id]['subtotal'] = $order->get_subtotal();
                $orderdetails[$id]['discount'] = $order->get_total_discount();
                $orderdetails[$id]['tax'] = $order->get_taxes();
                $orderdetails[$id]['total'] = $order->get_total();
                
                $orderdetails[$id]['shipping_method'] = $order->get_shipping_method();
                $orderdetails[$id]['shipping_methods'] = $order->get_shipping_methods();
                $orderdetails[$id]['shipping_to_display'] = $order->get_shipping_to_display();
                $orderdetails[$id]['status'] = $order->get_status();;
                */
                $seller_id = $seller = dokan_get_seller_id_by_order($id);
                $profile_info = dokan_get_store_info($seller_id);

                $pickup_address = '';
                $pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

                if (empty(str_replace(",", "", str_replace(" ", "", $pickup_address))))
                {
                    $orderdetails[$id]['pickup_address'] = null;
                    $orderdetails[$id]['pickup_full_address'] = null;
                }
                else
                {
                    $orderdetails[$id]['pickup_address'] = $pickup_address;
                    $orderdetails[$id]['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['zip'] . ',' . $profile_info['address']['city'] . ',' . $profile_info['address']['state'] . ',' . $profile_info['address']['country'];
                    $orderdetails[$id]['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];
                }
                $orderdetails[$id]['stornename'] = $profile_info['store_name'];

                $map_pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

                $orderdetails[$id]['pickup_phone'] = $profile_info['phone'];
                $orderdetails[$id]['pickup_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode(implode(', ', $profile_info['address']));
                $orderdetails[$id]['pickup_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($map_pickup_address);

                $delivery_address = '';
                $delivery_address = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country();

                if (empty(str_replace(",", "", str_replace(" ", "", $delivery_address))))
                {
                    $orderdetails[$id]['delivery_address'] = null;
                    $orderdetails[$id]['delivery_full_address'] = null;
                }
                else
                {
                    $orderdetails[$id]['delivery_address'] = $delivery_address;
                    $orderdetails[$id]['delivery_full_address'] = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ' \n' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country() . ' \n' . $order->get_billing_email();
                }

                $orderdetails[$id]['customername'] = $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name();

                $orderdetails[$id]['delivery_phone'] = $order->get_billing_phone();;
                $orderdetails[$id]['delivery_map'] = $order->get_shipping_address_map_url();
                $orderdetails[$id]['delivery_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country());

                $orderdetails[$id]['billing_full_address'] = $order->get_billing_address_1() . ' ' . $order->get_billing_address_2() . ' \n' . $order->get_billing_city() . ' ' . $order->get_billing_state() . ' ' . $order->get_billing_postcode() . ',' . $order->get_billing_country() . ' \n' . $order->get_billing_email();

                $orderdetails[$id]['customer_note'] = $order->get_customer_note();
                $orderdetails[$id]['payment_method'] = $order->get_payment_method();
                $orderdetails[$id]['payment_method_title'] = $order->get_payment_method_title();
                $orderdetails[$id]['total'] = $order->get_currency() . ' ' . $order->get_total();

                $orderdetails[$id]['date_created'] = $order->get_date_created();
                $orderdetails[$id]['date_completed'] = $order->get_date_completed();
                $orderdetails[$id]['date_paid'] = $order->get_date_paid();

                $orderdetails[$id]['get_customer_id'] = $order->get_customer_id();
                $orderdetails[$id]['get_user_id'] = $order->get_user_id();

                $orderdetails[$id]['status'] = $order->get_status();;
            }

            $response['status'] = "success";
            $response['orders'] = $orderdetails;
            return new WP_REST_Response($response, 200);
        }
        else
        {
            $response['status'] = "success";
            $response['orders'] = [];
            return new WP_REST_Response($response, 200);
        }
    }

    public function get_returned_orders($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
		$limit=2;
		if($page){
			$paged=$page;
		}else{
			$paged=1;
		}
		$response['page'] = $paged;
        $orders = wc_get_orders(array(
            'limit' => $limit,
			'paged' => $paged,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_key' => 'ddwc_driver_id',
            'meta_value' => $user_id,
            'meta_compare' => '=',
            'status' => 'wc-order-returned',
        ));
        if (count($orders))
        {
            $orderdetails = array();

            foreach ($orders as $order)
            {
                $id = $order->get_id();

                /* $orderdetails[$id]['billing_first_name'] = $order->get_billing_first_name();
                $orderdetails[$id]['billing_last_name'] = $order->get_billing_last_name();
                $orderdetails[$id]['billing_company'] = $order->get_billing_company();
                $orderdetails[$id]['billing_address_1'] = $order->get_billing_address_1();
                $orderdetails[$id]['billing_address_2'] = $order->get_billing_address_2();
                $orderdetails[$id]['billing_city'] = $order->get_billing_city();
                $orderdetails[$id]['billing_state'] = $order->get_billing_state();
                $orderdetails[$id]['billing_postcode'] = $order->get_billing_postcode();
                $orderdetails[$id]['billing_country'] = $order->get_billing_country();
                $orderdetails[$id]['billing_email'] = $order->get_billing_email();
                $orderdetails[$id]['billing_phone'] = $order->get_billing_phone();
                $orderdetails[$id]['shipping_first_name'] = $order->get_shipping_first_name();
                $orderdetails[$id]['shipping_last_name'] = $order->get_shipping_last_name();
                $orderdetails[$id]['shipping_company'] = $order->get_shipping_company();
                $orderdetails[$id]['shipping_address_1'] = $order->get_shipping_address_1();
                $orderdetails[$id]['shipping_address_2'] = $order->get_shipping_address_2();
                $orderdetails[$id]['shipping_city'] = $order->get_shipping_city();
                $orderdetails[$id]['shipping_state'] = $order->get_shipping_state();
                $orderdetails[$id]['get_order_key'] = $order->get_shipping_postcode();
                $orderdetails[$id]['shipping_postcode'] = $order->get_shipping_country();
                $orderdetails[$id]['address'] = $order->get_address();
                $orderdetails[$id]['shipping_address_map_url'] = $order->get_shipping_address_map_url();
                $orderdetails[$id]['formatted_billing_full_name'] = $order->get_formatted_billing_full_name();
                $orderdetails[$id]['formatted_shipping_full_name'] = $order->get_formatted_shipping_full_name();
                $orderdetails[$id]['formatted_billing_address'] = $order->get_formatted_billing_address();
                $orderdetails[$id]['formatted_shipping_address'] = $order->get_formatted_shipping_address();
                
                $orderdetails[$id]['fees'] = $order->get_fees();
                $orderdetails[$id]['subtotal'] = $order->get_subtotal();
                $orderdetails[$id]['discount'] = $order->get_total_discount();
                $orderdetails[$id]['tax'] = $order->get_taxes();
                $orderdetails[$id]['total'] = $order->get_total();
                
                $orderdetails[$id]['shipping_method'] = $order->get_shipping_method();
                $orderdetails[$id]['shipping_methods'] = $order->get_shipping_methods();
                $orderdetails[$id]['shipping_to_display'] = $order->get_shipping_to_display();
                $orderdetails[$id]['status'] = $order->get_status();;
                */
                $seller_id = $seller = dokan_get_seller_id_by_order($id);
                $profile_info = dokan_get_store_info($seller_id);

                $pickup_address = '';
                $pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

                if (empty(str_replace(",", "", str_replace(" ", "", $pickup_address))))
                {
                    $orderdetails[$id]['pickup_address'] = null;
                    $orderdetails[$id]['pickup_full_address'] = null;
                }
                else
                {
                    $orderdetails[$id]['pickup_address'] = $pickup_address;
                    $orderdetails[$id]['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['zip'] . ',' . $profile_info['address']['city'] . ',' . $profile_info['address']['state'] . ',' . $profile_info['address']['country'];
                    $orderdetails[$id]['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];
                }
                $orderdetails[$id]['stornename'] = $profile_info['store_name'];

                $map_pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

                $orderdetails[$id]['pickup_phone'] = $profile_info['phone'];
                $orderdetails[$id]['pickup_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode(implode(', ', $profile_info['address']));
                $orderdetails[$id]['pickup_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($map_pickup_address);

                $delivery_address = '';
                $delivery_address = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country();

                if (empty(str_replace(",", "", str_replace(" ", "", $delivery_address))))
                {
                    $orderdetails[$id]['delivery_address'] = null;
                    $orderdetails[$id]['delivery_full_address'] = null;
                }
                else
                {
                    $orderdetails[$id]['delivery_address'] = $delivery_address;
                    $orderdetails[$id]['delivery_full_address'] = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ' \n' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country() . ' \n' . $order->get_billing_email();
                }

                $orderdetails[$id]['customername'] = $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name();

                $orderdetails[$id]['delivery_phone'] = $order->get_billing_phone();;
                $orderdetails[$id]['delivery_map'] = $order->get_shipping_address_map_url();
                $orderdetails[$id]['delivery_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country());

                $orderdetails[$id]['billing_full_address'] = $order->get_billing_address_1() . ' ' . $order->get_billing_address_2() . ' \n' . $order->get_billing_city() . ' ' . $order->get_billing_state() . ' ' . $order->get_billing_postcode() . ',' . $order->get_billing_country() . ' \n' . $order->get_billing_email();

                $orderdetails[$id]['customer_note'] = $order->get_customer_note();
                $orderdetails[$id]['payment_method'] = $order->get_payment_method();
                $orderdetails[$id]['payment_method_title'] = $order->get_payment_method_title();
                $orderdetails[$id]['total'] = $order->get_currency() . ' ' . $order->get_total();

                $orderdetails[$id]['date_created'] = $order->get_date_created();
                $orderdetails[$id]['date_completed'] = $order->get_date_completed();
                $orderdetails[$id]['date_paid'] = $order->get_date_paid();

                $orderdetails[$id]['get_customer_id'] = $order->get_customer_id();
                $orderdetails[$id]['get_user_id'] = $order->get_user_id();

                $orderdetails[$id]['status'] = $order->get_status();;
            }

            $response['status'] = "success";
            $response['orders'] = $orderdetails;
            return new WP_REST_Response($response, 200);
        }
        else
        {
            $response['status'] = "success";
            $response['orders'] = [];
            return new WP_REST_Response($response, 200);
        }
    }

    public function get_completed_orders($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";

            return new WP_REST_Response($response, 200);
        }
		$limit=2;
		if($page){
			$paged=$page;
		}else{
			$paged=1;
		}
		$response['page'] = $paged;
        $orders = wc_get_orders(array(
            'limit' => $limit,
			'paged' => $paged,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_key' => 'ddwc_driver_id',
            'meta_value' => $user_id,
            'meta_compare' => '=',
            'status' => 'wc-completed',
        ));
        if (count($orders))
        {
            $orderdetails = array();

            foreach ($orders as $order)
            {
                $id = $order->get_id();

                /* $orderdetails[$id]['billing_first_name'] = $order->get_billing_first_name();
                $orderdetails[$id]['billing_last_name'] = $order->get_billing_last_name();
                $orderdetails[$id]['billing_company'] = $order->get_billing_company();
                $orderdetails[$id]['billing_address_1'] = $order->get_billing_address_1();
                $orderdetails[$id]['billing_address_2'] = $order->get_billing_address_2();
                $orderdetails[$id]['billing_city'] = $order->get_billing_city();
                $orderdetails[$id]['billing_state'] = $order->get_billing_state();
                $orderdetails[$id]['billing_postcode'] = $order->get_billing_postcode();
                $orderdetails[$id]['billing_country'] = $order->get_billing_country();
                $orderdetails[$id]['billing_email'] = $order->get_billing_email();
                $orderdetails[$id]['billing_phone'] = $order->get_billing_phone();
                $orderdetails[$id]['shipping_first_name'] = $order->get_shipping_first_name();
                $orderdetails[$id]['shipping_last_name'] = $order->get_shipping_last_name();
                $orderdetails[$id]['shipping_company'] = $order->get_shipping_company();
                $orderdetails[$id]['shipping_address_1'] = $order->get_shipping_address_1();
                $orderdetails[$id]['shipping_address_2'] = $order->get_shipping_address_2();
                $orderdetails[$id]['shipping_city'] = $order->get_shipping_city();
                $orderdetails[$id]['shipping_state'] = $order->get_shipping_state();
                $orderdetails[$id]['get_order_key'] = $order->get_shipping_postcode();
                $orderdetails[$id]['shipping_postcode'] = $order->get_shipping_country();
                $orderdetails[$id]['address'] = $order->get_address();
                $orderdetails[$id]['shipping_address_map_url'] = $order->get_shipping_address_map_url();
                $orderdetails[$id]['formatted_billing_full_name'] = $order->get_formatted_billing_full_name();
                $orderdetails[$id]['formatted_shipping_full_name'] = $order->get_formatted_shipping_full_name();
                $orderdetails[$id]['formatted_billing_address'] = $order->get_formatted_billing_address();
                $orderdetails[$id]['formatted_shipping_address'] = $order->get_formatted_shipping_address();
                
                $orderdetails[$id]['fees'] = $order->get_fees();
                $orderdetails[$id]['subtotal'] = $order->get_subtotal();
                $orderdetails[$id]['discount'] = $order->get_total_discount();
                $orderdetails[$id]['tax'] = $order->get_taxes();
                $orderdetails[$id]['total'] = $order->get_total();
                
                $orderdetails[$id]['shipping_method'] = $order->get_shipping_method();
                $orderdetails[$id]['shipping_methods'] = $order->get_shipping_methods();
                $orderdetails[$id]['shipping_to_display'] = $order->get_shipping_to_display();
                $orderdetails[$id]['status'] = $order->get_status();;
                */
                $seller_id = $seller = dokan_get_seller_id_by_order($id);
                $profile_info = dokan_get_store_info($seller_id);

                $pickup_address = '';
                $pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

                if (empty(str_replace(",", "", str_replace(" ", "", $pickup_address))))
                {
                    $orderdetails[$id]['pickup_address'] = null;
                    $orderdetails[$id]['pickup_full_address'] = null;
                }
                else
                {
                    $orderdetails[$id]['pickup_address'] = $pickup_address;
                    $orderdetails[$id]['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['zip'] . ',' . $profile_info['address']['city'] . ',' . $profile_info['address']['state'] . ',' . $profile_info['address']['country'];
                    $orderdetails[$id]['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];
                }
                $orderdetails[$id]['stornename'] = $profile_info['store_name'];

                $map_pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

                $orderdetails[$id]['pickup_phone'] = $profile_info['phone'];
                $orderdetails[$id]['pickup_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode(implode(', ', $profile_info['address']));
                $orderdetails[$id]['pickup_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($map_pickup_address);

                $delivery_address = '';
                $delivery_address = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country();

                if (empty(str_replace(",", "", str_replace(" ", "", $delivery_address))))
                {
                    $orderdetails[$id]['delivery_address'] = null;
                    $orderdetails[$id]['delivery_full_address'] = null;
                }
                else
                {
                    $orderdetails[$id]['delivery_address'] = $delivery_address;
                    $orderdetails[$id]['delivery_full_address'] = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ' \n' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country() . ' \n' . $order->get_billing_email();
                }

                $orderdetails[$id]['customername'] = $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name();

                $orderdetails[$id]['delivery_phone'] = $order->get_billing_phone();;
                $orderdetails[$id]['delivery_map'] = $order->get_shipping_address_map_url();
                $orderdetails[$id]['delivery_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country());

                $orderdetails[$id]['billing_full_address'] = $order->get_billing_address_1() . ' ' . $order->get_billing_address_2() . ' \n' . $order->get_billing_city() . ' ' . $order->get_billing_state() . ' ' . $order->get_billing_postcode() . ',' . $order->get_billing_country() . ' \n' . $order->get_billing_email();

                $orderdetails[$id]['customer_note'] = $order->get_customer_note();
                $orderdetails[$id]['payment_method'] = $order->get_payment_method();
                $orderdetails[$id]['payment_method_title'] = $order->get_payment_method_title();
                $orderdetails[$id]['total'] = $order->get_currency() . ' ' . $order->get_total();

                $orderdetails[$id]['date_created'] = $order->get_date_created();
                $orderdetails[$id]['date_completed'] = $order->get_date_completed();
                $orderdetails[$id]['date_paid'] = $order->get_date_paid();

                $orderdetails[$id]['get_customer_id'] = $order->get_customer_id();
                $orderdetails[$id]['get_user_id'] = $order->get_user_id();

                $orderdetails[$id]['status'] = $order->get_status();;
            }

            $response['status'] = "success";
            $response['orders'] = $orderdetails;
            return new WP_REST_Response($response, 200);
        }
        else
        {
            $response['status'] = "success";
            $response['orders'] = [];
            return new WP_REST_Response($response, 200);
        }
    }


	   public function single_order($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
		
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');

        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            $response['message'] = "Session time out.Please logging again.";
            return new WP_REST_Response($response, 200);
        }
        if (!$order_id)
        {
            $response['status'] = "error";
            $response['message'] = "No order Id.";
            return new WP_REST_Response($response, 200);
        }

        $order = wc_get_order($order_id);

        $id = $order->get_id();
		
		//$orderdetails['formatted_billing_full_name'] = $order->get_formatted_billing_full_name();
		//$orderdetails['formatted_billing_address'] =  str_replace("<br/>"," \n ",$order->get_formatted_billing_address());
		
		$seller_id = $seller = dokan_get_seller_id_by_order($id);
		$profile_info = dokan_get_store_info($seller_id);
		
		$orderdetails['ID'] =$id;
		
		$orderdetails['store_name'] = $profile_info['store_name'];
		$orderdetails['store_phone'] = $profile_info['phone'];

		$pickup_address = '';
		$pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];

		if (empty(str_replace(",", "", str_replace(" ", "", $pickup_address))))
		{
			$orderdetails['store_address'] = null;
			//$orderdetails['pickup_full_address'] = null;
		}
		else
		{
			$orderdetails['store_address'] = $pickup_address;
			//$orderdetails['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['zip'] . ',' . $profile_info['address']['city'] . ',' . $profile_info['address']['state'] . ',' . $profile_info['address']['country'];
			//$orderdetails['pickup_full_address'] = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ' \n' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];
		}

		$map_pickup_address = $profile_info['address']['street_1'] . ' ' . $profile_info['address']['street_2'] . ',' . $profile_info['address']['city'] . ' ' . $profile_info['address']['state'] . ' ' . $profile_info['address']['zip'] . ',' . $profile_info['address']['country'];
		$orderdetails['store_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode(implode(', ', $profile_info['address']));
		$orderdetails['store_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($map_pickup_address);

		
		$orderdetails['customer_name'] = $order->get_formatted_shipping_full_name();
		$orderdetails['customer_phone'] = $order->get_billing_phone();; 
		$orderdetails['customer_email'] = $order->get_billing_email();
		$delivery_address = '';
		$delivery_address = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country();

		if (empty(str_replace(",", "", str_replace(" ", "", $delivery_address))))
		{
			$orderdetails['customer_address'] = null;
			//$orderdetails['delivery_full_address'] = null;
		}
		else
		{
			$orderdetails['customer_address'] = $delivery_address;
			//$orderdetails['delivery_full_address'] = $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ' \n' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country();
		}
		$orderdetails['customer_map'] = 'https://maps.google.com/maps?&q=' . rawurlencode($order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() . ',' . $order->get_shipping_city() . ' ' . $order->get_shipping_state() . ' ' . $order->get_shipping_postcode() . ',' . $order->get_shipping_country());
		//$orderdetails['formatted_shipping_address'] = str_replace("<br/>"," \n ",$order->get_formatted_shipping_address());

		foreach ( $order->get_items() as $item_id => $item ) {
			$orderdetails['products'][$item->get_product_id()]['product_id'] = $item->get_product_id();
			$orderdetails['products'][$item->get_product_id()]['variation_id'] = $item->get_variation_id();
			$orderdetails['products'][$item->get_product_id()]['name'] = $item->get_name();
			$orderdetails['products'][$item->get_product_id()]['quantity'] = $item->get_quantity();
			$orderdetails['products'][$item->get_product_id()]['subtotal'] = $item->get_subtotal();
			$orderdetails['products'][$item->get_product_id()]['total'] = $item->get_total();
			$orderdetails['products'][$item->get_product_id()]['tax'] = $item->get_subtotal_tax();
		}
		
		
		$orderdetails['shipping_method'] = $order->get_shipping_method();
		$orderdetails['payment_method'] = $order->get_payment_method();
		$orderdetails['payment_method_title'] = $order->get_payment_method_title();
		
		$orderdetails['currency'] =  $order->get_currency();
		$orderdetails['currency_symbol'] =  get_woocommerce_currency_symbol($order->get_currency());
		$orderdetails['subtotal'] =   $order->get_subtotal();
		
		if($order->get_fees()){
			$orderdetails['fees'] =  $order->get_fees();
		}else{
			$orderdetails['fees'] = null;
		}
		
		if($order->get_discount_tax()){
			$orderdetails['tax'] =   $order->get_discount_tax();
		}else{
			$orderdetails['tax'] = null;
		}
		
		if($order->get_discount_total()){
			$orderdetails['discount'] =   $order->get_discount_total();
		}else{
			$orderdetails['discount'] = null;
		}
		
		if($order->get_shipping_total()){
			$orderdetails['shipping'] =  $order->get_shipping_total();
		}else{
			$orderdetails['shipping'] = null;
		}
		/* if($order->get_taxes()){
			$orderdetails['tax'] = $order->get_taxes();
		}else{
			$orderdetails['tax'] = null;
		} */
		
		$orderdetails['total'] =  $order->get_total(); 
		
		$orderdetails['customer_note'] = $order->get_customer_note();

		$orderdetails['date_created'] = $order->get_date_created();
		$orderdetails['date_completed'] = $order->get_date_completed();
		$orderdetails['date_paid'] = $order->get_date_paid();

		$orderdetails['status'] = $order->get_status();;
        $response['order'] = $orderdetails;

        $response['status'] = "success";
        return new WP_REST_Response($response, 200);

    }
}

$wpJsonDriverOrderApi = new WPJsonDriverOrderApi();

