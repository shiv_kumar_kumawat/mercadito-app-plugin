<?php
include_once (ABSPATH . 'wp-admin/includes/plugin.php');
if (!is_plugin_active('woocommerce/woocommerce.php'))
{
    return;
}

class Push {
    private $title;
    private $message;
    private $image;
    private $url;
    // push message payload
    private $data;
    private $type;
    private $is_background;
 
    function __construct() {
         
    }
    public function setTitle($title) {
        $this->title = $title;
    }
    public function setUrl($url) {
        $this->url = $url;
    }
 
    public function setMessage($message) {
        $this->message = $message;
    }
 
    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }
 
    public function setPayload($data) {
        $this->data = $data;
    }
    public function setIsBackground($is_background) {
        $this->is_background = $is_background;
    }
    public function setType($type) {
        $this->type = $type;
    }
 
    public function getPush() {
        $res = array();
        $res['title'] = $this->title;
        $res['click_action'] ="FLUTTER_NOTIFICATION_CLICK";
        $res['url'] = $this->url;
        $res['is_background'] = $this->is_background;
        $res['message'] = $this->message;
        $res['image'] = $this->image;
        $res['payload'] = $this->data;
        $res['type'] = $this->type;
        $res['timestamp'] = date('Y-m-d G:i:s');
        return $res;
    }
 
}

class Firebase {
	
    public function sendPush($to, $message,$type='single'){
        if($type=="multiple"){
            return $this->sendMultiple($to, $message);
        }else{
            return $this->send($to, $message);
        }
    }

    public function send($to, $message) {
    	
		if($message['image']){
			$fields = array(
				'to' => $to,
				'priority' => 10,
				'notification'=>["title"=>$message['title'],"body"=>$message['message'],'sound' => 'default','image'=>$message['image']],
				'data' =>$message,
			);

		}else{
			$fields = array(
				'to' => $to,
				'priority' => 10,
				'notification'=>["title"=>$message['title'],"body"=>$message['message'],'sound' => 'default'],
				'data' =>$message,
			);
		}
        return $this->sendPushNotification($fields);
    }
    // Sending message to a topic by topic name
    public function sendToTopic($message) {
        $fields = array(
            "to"=> "/topics/all",
            'data' => $message,
        );
        return $this->sendPushNotification($fields);
    }
 
    // sending push message to multiple users by firebase registration ids
    public function sendMultiple($registration_ids, $message) {

		if($message['image']){
			 $fields = array(
				'registration_ids' => $registration_ids,
				'priority' => 10,
				'notification'=>["title"=>$message['title'],"body"=>$message['message'],'sound' => 'default','image'=>$message['image']],
				'data' =>$message,
			);
		}else{
			 $fields = array(
				'registration_ids' => $registration_ids,
				'priority' => 10,
				'notification'=>["title"=>$message['title'],"body"=>$message['message'],'sound' => 'default'],
				'data' =>$message,
			);
		}
       
        return $this->sendPushNotification($fields);
    }

    // function makes curl request to firebase servers
    private function sendPushNotification($fields) {
        //define('FIREBASE_API_KEY', $this->serverKey);
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';
		$serverkey = get_option('ddwc_pro_settings_firebase_serverkey');
		$headers = array(
			'Authorization: key='.$serverkey,
			'Content-Type: application/json'
		); 
        // Open connection
        $ch = curl_init();
         // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
         // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        return $result;
    }
	
	public function makePushNotification($title,$message,$push_type,$firebase_token,$type="single",$payload_data="",$image=''){
    	$push=new Push();
    	$payload = null;
    	$url="";
		if(!empty($payload_data)){
		   $payload=$payload_data;
		   if(isset($payload['url'])){
		   	$url	=	$payload['url'];
		   } 
		}
        $include_image = FALSE;
        $push->setTitle($title);
        $push->setUrl($url);
        $push->setMessage($message);
        $push->setType($push_type);
        $push->setImage($image);
        $push->setIsBackground(FALSE);
        $push->setPayload($payload);
        $json = '';
        $response = '';
        $json = $push->getPush();
        $response = $this->sendPush($firebase_token,$json,$type);
        return $response;
    }
	
	public function maketopicNotification($message){
        $response = $this->sendToTopic($message);
        return $response;
    }
	
}

class WPJsonFirebaseApi
{
	public function __construct()
    {
		//add_action('woocommerce_thankyou',array( $this,'firebase_api_trigger'), 10, 1);

        add_action('rest_api_init', array(
            $this,
            'JsonFirebaseApi'
        ));
    }
	
	

	public function callFirebaseAPI($method, $url, $data){
	   $curl = curl_init($url);

	   switch ($method){
		  case "POST":
			 curl_setopt($curl, CURLOPT_POST, 1);
			 if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			 break;
		  case "PUT":
			 curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			 if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
			 break;
				 
		  default:
			 if ($data)
				$url = sprintf("%s?%s", $url, http_build_query($data));
	   }

	   // OPTIONS:
	   curl_setopt($curl, CURLOPT_URL, $url);
	   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		  'APIKEY: 111111111111111111111',
		  'Content-Type: application/json',
	   ));
	   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	   // EXECUTE:
	   $result = curl_exec($curl);
	   if(!$result){die("Connection Failure");}
	   curl_close($curl);
	   return $result;
	}

	public function firebase_api_trigger( $order_id ) { 
	
		if ( ! $order_id )
			return;
			
		if( ! get_post_meta( $order_id, 'FirebaseNotificationSent', true ) ) {
			
			$msginfo = array(
			   'order_id' => $order_id,
			   'title' => 'New Order Placed',
			   'message' => '#'.$order_id.' Order Placed. Please Accept',
            );
			
			$msginfo = apply_filters('order_place_firebase_message',$msginfo);

			$fb_response = self::callFirebaseAPI('POST', get_site_url().'/wp-json/driverorderapi/notify_driver', json_encode(array(
				'order_id'=>$order_id,
				'message'=>$msginfo['message'],
				'title'=>$msginfo['title'])));
				
			$firebase_response = json_decode($fb_response, true);
			
			update_post_meta($order_id,'FirebaseNotificationSent',1) ;
		}
	}
	
    public function JsonFirebaseApi()
    {
		register_rest_route( 'driverorderapi', '/notify_driver', array(
			'methods'  => 'POST',
			'callback' => array(
                $this,
                'notify_driver'
            ) ,
		) );

	}
	
	public function notify_driver($request) {
		
		$parameters = $request->get_json_params();
        extract($parameters);

		if (!$order_id)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'order_id'.";
            return new WP_REST_Response($response, 200);
        }
		
        if (!$title)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'title'.";
            return new WP_REST_Response($response, 200);
        }
		
		if (!$message)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'message'.";
            return new WP_REST_Response($response, 200);
        }
		
		if (!$drivers_id)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'drivers_id'.";
            return new WP_REST_Response($response, 200);
        }
		
		if ($drivers_id == "null")
        {
            $response['status'] = "error";
            $response['message'] = "No matching driver";
            return new WP_REST_Response($response, 200);
        }
		

		$order 		= new WC_Order( $order_id );
		if($order->get_status() != 'ready-to-ship'){
			$response['status'] = "error";
            $response['message'] = "Not avaiable to ship from vendor";
            return new WP_REST_Response($response, 200);
		}
		$ordermeta['customer']		=	$customer 		=	get_user_by( 'id', $order->get_customer_id() );
		$ordermeta['customerid']	=	$customerid		=	$customer->ID;
		$ordermeta['customername']	=	$customername	=	$customer->display_name;
		
		
		$firebase_token =	array();
		$firebaseuser 	=	array();
		$firebasesync	=	array();
		
		/* $args = array(
					'role'    => 'driver',
					'orderby' => 'user_nicename',
					'order'   => 'ASC',
					'meta_query'  => array(
						'relation' => 'AND',
							array(
								'key'     =>'ddwc_driver_availability',
								'value'   => 'on',
								'compare' => '='
							)
						),
				);
		$users 			=	get_users( $args );
		 */
		/* foreach ( $users as $user ) {
			if(!empty(get_user_meta($user->ID,'device_id',true))){
				$firebase_token[]	= get_user_meta($user->ID,'device_id',true);
				$firebasesync[]	= array($user->ID=>get_user_meta($user->ID,'device_id',true));
			}
			$firebaseuser[]		= $user->ID;
		} */
		
		foreach ( explode(",",$drivers_id) as $userID ) {
			if(!empty(get_user_meta($userID,'device_id',true))){
				$firebase_token[]	= get_user_meta($userID,'device_id',true);
				$firebasesync[]	= array($userID=>get_user_meta($userID,'device_id',true));
			}
			$firebaseuser[]		= $userID;
		}
		
		$firebase	=	new Firebase();

		if(count($firebase_token)){
			$result				=  $firebase->makePushNotification($title,$message,"",$firebase_token,"multiple","","");
			$response['result']	=	$result;	
		}
		
		global $wpdb;
		foreach($firebaseuser as $us){
			$mid = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}driver_notification WHERE order_id = $order_id AND driver_id =$us");
			if(Empty($mid)){
				$wpdb->insert("{$wpdb->prefix}driver_notification", array(
						'order_id' => $order_id,   
						'driver_id' => $us,   
					)
				);
			}
		}
		
		$response['firebasesync']	=	$firebasesync;			
		$response['firebaseuser']	=	$firebaseuser;			
		$response['firebase_token']	=	$firebase_token;			

		return new WP_REST_Response($response, 200);
		
	}
	
	
    
}

$wpjsonfirebaseapi = new WPJsonFirebaseApi();
