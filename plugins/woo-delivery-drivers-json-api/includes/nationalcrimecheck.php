<?php
class NationalCrimeCheckAPI
{

    // Base endpoint the the API
    const BASE_ENDPOINT = 'https://api.nationalcrimecheck.com.au/consumer_v1.2/';
    
	// API credentials; these thould be stored in a configuration file
	const API_KEY = '66HV3GVUgIHjGHIfJIztLcCu57XpcnBX';
    const API_SECRET = 'RUB8R76xdLDbLIY2A5ozhcROBaEgEiSV';

    /**
     * Make an API Request to the National Crime Check API
     *
     * @param string $method HTTP method for the request (one of 'GET' or 'POST')
     * @param string $endpoint Request endpoint (e.g. 'checks/create')
     * @param array $data Data for POST requests
     * @return array API error response, keys: 'error' and 'message'
     * @return array API response, as per the docs
     */
    protected static function apiRequest(string $method, string $endpoint, array $data = null)
    {
        //$auth = 'Basic ' . base64_encode(self::API_KEY . ':' . self::API_SECRET);
		$auth = 'Basic ' . base64_encode(get_option('ddwc_police_check_api_key') . ':' .get_option('ddwc_police_check_api_secret'));
        $headers = ['Authorization: ' . $auth];

        $ch = curl_init(self::BASE_ENDPOINT . $endpoint);

        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            $headers[] = 'Content-type: application/json';
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($response === false) {
            return ['error' => 500, 'message' => 'No server response'];
        }

        $decoded = @json_decode($response, true);
        if ($decoded === null) {
            return ['error' => 500, 'message' => 'Server returned invalid JSON'];
        }

        return $decoded;
    }

    /**
     * Send a "ping" api request
     *
     * @return bool True if ping was successful, false otherwise
     */
    public static function ping()
    {
        $response = static::apiRequest('GET', 'ping');
        return ($response === 'pong');
    }

    /**
     * Send a "create check" api request
     *
     * @return array API error response, keys: 'error' and 'message'
     * @return array API chreck creation response, keys: 'id' and 'continue_url'
     */
    public static function createCheck(array $data)
    {
        return static::apiRequest('POST', 'checks/create', $data);
    }
	
	

	
}

add_action('rest_api_init','JsonPoliceCheckApi');
function JsonPoliceCheckApi()
{
	register_rest_route('driver_police_check', '/create_check', array(
		'methods' => 'POST',
		'callback' => 'create_check_api' ,
	));
}
function create_check_api($request)
{
	$parameters = $request->get_json_params();
	$response = NationalCrimeCheckAPI::createCheck($parameters);
	return new WP_REST_Response($response, 200);
	$data =array(
		"client_ref"=> "abcde",
		"cost_centre"=> "Marketing",
		"first_name"=> "John",
		"middle_name"=> "Nathan",
		"last_name"=> "Smith",
		"single_name"=> false,
		"dob"=> "1976-05-04",
		"birth_place"=> "Adelaide",
		"birth_state"=> "SA",
		"birth_country"=> "AUS",
		"sex"=> "M",
		"email"=> "john.smith@example.com",
		"resid"=> array(
			"street"=> "1 Something street",
			"suburb"=> "Adelaide",
			"state"=> "SA",
			"postcode"=> "5000",
			"years"=> 3,
			"months"=> 2
		),
		"previous"=> [
			array(
				"street"=> "1 Another street",
				"suburb"=> "Adelaide",
				"state"=> "SA",
				"postcode"=> "5000",
				"country"=> "AUS",
				"years"=> 1,
				"months"=> 1
			),
			array(
				"street"=> "1 Old avenue",
				"suburb"=> "Adelaide",
				"state"=> "SA",
				"postcode"=> "5000",
				"country"=> "AUS",
				"years"=> 3,
				"months"=> 1
			)
		],
		"type"=> "EMPLOYMENT",
		"healthcare"=> "NONE",
		"services"=> ["Vevo", "Bankruptcy"],
		"reason"=> "Computer programmer",
		"place_work"=> "ABC Computing",
		//"result_webhook"=> get_site_url().'/wp-admin/admin-ajax.php?action=policecheck_listner'
	);
	$data2	=	array(  
					'first_name' => 'John',
					'last_name' => 'Smith',
					'email' => 'john.smith@example.com',
					'type' => 'EMPLOYMENT',
					'reason' => 'Computer programmer',
				); 

	//$resp = NationalCrimeCheckAPI::createCheck($data);
	$resp2 = NationalCrimeCheckAPI::createCheck($data2);
	echo '<pre>';
	//echo get_option('ddwc_police_check_api_key');
	//echo get_option('ddwc_police_check_api_secret');
	//print_r($data); 
	print_r($data2); 
	//print_r($resp); 
	print_r($resp2); 
	echo '</pre>'; 
}
/* 
$data =array(
	"client_ref"=> "abcde",
	"cost_centre"=> "Marketing",
	"first_name"=> "John",
	"middle_name"=> "Nathan",
	"last_name"=> "Smith",
	"single_name"=> false,
	"dob"=> "1976-05-04",
	"birth_place"=> "Adelaide",
	"birth_state"=> "SA",
	"birth_country"=> "AUS",
	"sex"=> "M",
	"email"=> "john.smith@example.com",
	"resid"=> array(
		"street"=> "1 Something street",
		"suburb"=> "Adelaide",
		"state"=> "SA",
		"postcode"=> "5000",
		"years"=> 3,
		"months"=> 2
	),
	"previous"=> [
		array(
			"street"=> "1 Another street",
			"suburb"=> "Adelaide",
			"state"=> "SA",
			"postcode"=> "5000",
			"country"=> "AUS",
			"years"=> 1,
			"months"=> 1
		),
		array(
			"street"=> "1 Old avenue",
			"suburb"=> "Adelaide",
			"state"=> "SA",
			"postcode"=> "5000",
			"country"=> "AUS",
			"years"=> 3,
			"months"=> 1
		)
	],
	"type"=> "EMPLOYMENT",
	"healthcare"=> "NONE",
	"services"=> ["Vevo", "Bankruptcy"],
	"reason"=> "Computer programmer",
	"place_work"=> "ABC Computing",
	//"result_webhook"=> get_site_url().'/wp-admin/admin-ajax.php?action=policecheck_listner'
);
$data2	=	array(  
				'first_name' => 'John',
				'last_name' => 'Smith',
				'email' => 'john.smith@example.com',
				'type' => 'EMPLOYMENT',
				'reason' => 'Computer programmer',
			); 

//$resp = NationalCrimeCheckAPI::createCheck($data);
$resp2 = NationalCrimeCheckAPI::createCheck($data2);
echo '<pre>';
//echo get_option('ddwc_police_check_api_key');
//echo get_option('ddwc_police_check_api_secret');
//print_r($data); 
print_r($data2); 
//print_r($resp); 
print_r($resp2); 
echo '</pre>'; 
 */
add_action( 'wp_ajax_policecheck_listner', 'policecheck_listner' ); 
function policecheck_listner() { 
	echo '<pre>';
	print_r(get_user_meta(1,'usercheckrequest',true)['person']['id']);
	print_r(get_user_meta(1,'id_verify_done',true));
	print_r(get_user_meta(1,'id-verify-applicant',true));
	print_r(get_user_meta(1,'service-result',true));
	print_r(get_user_meta(1,'id-verify-qa',true));
	echo '</pre>';
	exit;
} 