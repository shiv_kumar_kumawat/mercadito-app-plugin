<?php

class WPJsonDriverApi
{
    public function __construct()
    {
        add_action('rest_api_init', array(
            $this,
            'JsonDriverApi'
        ));
    }

    public function get_api_controllers()
    {
        return array(
            'user'
            /* ,'auth' */
        );
    }

    public function get_controller_method($controller)
    {
        $cont = array(
            'user' => array(
                'register'
            ) ,
            /* 'auth' => array(
                'generate_auth_cookie',
                'validate_auth_cookie',
                'get_currentuserinfo',
            'get_user_meta',
            'update_user_meta',
            'delete_user_meta'
            ) , */
        );
        return $cont[$controller];
    }

    public function controller_method_exists($controller, $method)
    {
        if (in_array($method, self::get_controller_method($controller)))
        {
            return true;
        }
        return false;
    }

    public function get_nonce_id($controller, $method)
    {
        $controller = strtolower($controller);
        $method = strtolower($method);
        return "json_api-$controller-$method";
    }

    public function JsonDriverApi()
    {
        register_rest_route('driverapi', '/get_nonce', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'get_nonce'
            ) ,
        ));
        register_rest_route('driverapi', '/register', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'register'
            ) ,
        ));
        register_rest_route('driverapi', '/generate_auth_cookie', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'generate_auth_cookie'
            ) ,
        ));
        register_rest_route('driverapi', '/validate_auth_cookie', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'validate_auth_cookie'
            ) ,
        ));
        register_rest_route('driverapi', '/get_currentuserinfo', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'get_currentuserinfo'
            ) ,
        ));
        register_rest_route('driverapi', '/get_user_meta', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'get_user_meta'
            ) ,
        ));
        register_rest_route('driverapi', '/update_user_meta', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'update_user_meta'
            ) ,
        ));
        register_rest_route('driverapi', '/update_driver_availability', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'update_driver_availability'
            ) ,
        ));
        register_rest_route('driverapi', '/update_device_id', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'update_device_id'
            ) ,
        ));
        register_rest_route('driverapi', '/delete_user_meta', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'delete_user_meta'
            ) ,
        ));
        register_rest_route('driverapi', '/forgot_password', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'forgot_password'
            ) ,
        ));
        register_rest_route('driverapi', '/change_password', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'change_password'
            ) ,
        ));
        register_rest_route('driverapi', '/driver_image', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'driver_image'
            ) ,
        ));
        register_rest_route('driverapi', '/transportation_city', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'transportation_city'
            ) ,
        ));
        register_rest_route('driverapi', '/transportation_type', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'transportation_type'
            ) ,
        ));
        register_rest_route('driverapi', '/get_bank_details', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'get_bank_details'
            ) ,
        ));
        register_rest_route('driverapi', '/terms', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'terms'
            ) ,
        ));

        register_rest_route('driverapi', '/app_config', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'app_config'
            ) ,
        ));
		register_rest_route('driverapi', '/response_police_check', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'response_police_check'
            ) ,
        ));
        register_rest_route('driverapi', '/neighbourhood_citys', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'neighbourhood_citys'
            ) ,
        ));
    }
	
    public function neighbourhood_citys($request)
    {   
        $parameters = $request->get_json_params();
        extract($parameters);
       
        $arg = array(
                    'taxonomy' => 'area',
                    'parent' => $parent,
                    'hide_empty' => false
                );
        $all_parent = get_terms($arg);
 
        $all_cities_list = array();
        foreach ($all_parent as $key => $term) {
           // $all_cities_list[] = $term->term_id.'-'.$term->name;
			$all_cities_list[] = $term->name;
        }
 
        $response['status'] = "success";
        $response['options'] = $all_cities_list;
        return new WP_REST_Response($response, 200);
    }

	public function response_police_check($request)
    {
		$parameters = $request->get_json_params();
		$meta_value = $parameters['person']['id'];
		
		global $wpdb;
		$querystr = "	SELECT $wpdb->usermeta.user_id FROM $wpdb->usermeta WHERE $wpdb->usermeta.meta_key	= 'police_check_id' AND $wpdb->usermeta.meta_value	= '$meta_value' ";
		$results = $wpdb->get_results( $querystr, ARRAY_N );
		
		if ( is_numeric( $results[0][0] ) )
			$userid= $results[0][0];
		else
			return false;
		
		/* if($parameters['event']['type']=='id-verify-done'){
			update_user_meta($userid,'id_verify_done',$parameters);
		}
		if($parameters['event']['type']=='id-verify-applicant'){
			update_user_meta($userid,'id-verify-applicant',$parameters);
		}
		if($parameters['event']['type']=='service-result'){
			update_user_meta($userid,'service-result',$parameters);
		}
		if($parameters['event']['type']=='id-verify-qa'){
			update_user_meta($userid,'id-verify-qa',$parameters);
		} */
		update_user_meta($userid,'police_check_status',$parameters['event']['type']);
		$response['parameters'] = $parameters; 
		$response['status'] = "success"; 
		return new WP_REST_Response($response, 200);
    }
    public function app_config()
    {
        $response['status'] = "success";
        $response['stripe_termsurl'] = "https://stripe.com/in/connect/legal";
        return new WP_REST_Response($response, 200);
    }

    public function terms()
    {
        $response['status'] = "success";
        $response['content'] = get_post_field('post_content', 9342);
        return new WP_REST_Response($response, 200);
    }

    public function get_bank_details($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' var in your request. Use the `generate_auth_cookie` API method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        return array(
            "ddwc_driver_account_number" => str_repeat('*', strlen(get_user_meta($user_id, 'ddwc_driver_account_number', true)) - 4) . substr(get_user_meta($user_id, 'ddwc_driver_account_number', true) , -4) ,
            "ddwc_driver_bank_name" => get_user_meta($user_id, 'ddwc_driver_bank_name', true) ,
            "ddwc_driver_account_holder_name" => get_user_meta($user_id, 'ddwc_driver_account_holder_name', true) ,
            "ddwc_driver_ssn_number" => str_repeat('*', strlen(get_user_meta($user_id, 'ddwc_driver_ssn_number', true)) - 4) . substr(get_user_meta($user_id, 'ddwc_driver_ssn_number', true) , -4) ,
            "ddwc_driver_routing_number" => str_repeat('*', strlen(get_user_meta($user_id, 'ddwc_driver_routing_number', true)) - 4) . substr(get_user_meta($user_id, 'ddwc_driver_routing_number', true) , -4) ,
        );
    }
  
    public function driver_image($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }

        if (!$ddwc_driver_picture)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'ddwc_driver_picture ' var in your request , which is base64 string";
            return new WP_REST_Response($response, 200);
        }

        $attachmentid = self::Base64ToJpeg($ddwc_driver_picture, $user_id);
        $url = wp_get_attachment_url($attachmentid);
        update_user_meta($user_id, 'ddwc_driver_picture', array(
            'url' => $url
        ));
        $response['ddwc_driver_picture'] = $url;
        $response['status'] = "success";
        $response['message'] = "Driver Image uploaded";
        return new WP_REST_Response($response, 200);
    }

    public function transportation_city()
    {
        $cities_list = get_option('ddwc_driver_citys_list');
        $citylist_array = explode(', ', $cities_list);
        $response['status'] = "success";
        $response['city'] = $citylist_array;
        return new WP_REST_Response($response, 200);
    }

    public function transportation_type()
    {
        $response['status'] = "success";
        $response['type'] = array(
            "Bicycle",
            "Motorcycle",
            "Car",
            "SUV",
            "Truck"
        );
        return new WP_REST_Response($response, 200);
    }
    public function Base64ToJpeg($base64_img, $title)
    {

        $upload_dir = wp_upload_dir();
        $upload_path = str_replace('/', DIRECTORY_SEPARATOR, $upload_dir['path']) . DIRECTORY_SEPARATOR;

        $img = str_replace('data:image/jpeg;base64,', '', $base64_img);
        $img = str_replace(' ', '+', $img);
        $decoded = base64_decode($img);
        $filename = $title . '.jpeg';
        $file_type = 'image/jpeg';
        $hashed_filename = md5($filename . microtime()) . '_' . $filename;

        // Save the image in the uploads directory.
        $upload_file = file_put_contents($upload_path . $hashed_filename, $decoded);

        $attachment = array(
            'post_mime_type' => $file_type,
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($hashed_filename)) ,
            'post_content' => '',
            'post_status' => 'inherit',
            'guid' => $upload_dir['url'] . '/' . basename($hashed_filename)
        );

        return $attach_id = wp_insert_attachment($attachment, $upload_dir['path'] . '/' . $hashed_filename);

    }

    public function get_nonce($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if ($controller && $method)
        {
            $controller = strtolower($controller);
            if (!in_array($controller, array(
                'user'
            )))
            {
                $response['status'] = "error";
                $response['message'] = "Unknown controller '$controller'";
                return new WP_REST_Response($response, 200);
            }

            if (!in_array($method, array(
                'register'
            )))
            {
                $response['status'] = "error";
                $response['message'] = "Unknown method '$method'";
                return new WP_REST_Response($response, 200);
            }

            $nonce_id = self::get_nonce_id($controller, $method);
            $response['status'] = "success";
            $response['controller'] = $controller;
            $response['method'] = $method;
            $response['nonce'] = wp_create_nonce($nonce_id);
            return new WP_REST_Response($response, 200);
        }
        else
        {
            $response['status'] = "error";
            $response['message'] = "Unknown parameters";
            return new WP_REST_Response($response, 200);
        }
    }


	public function callWpDriverAPI($method, $url, $data){
	   $curl = curl_init($url);

	   switch ($method){
		  case "POST":
			 curl_setopt($curl, CURLOPT_POST, 1);
			 if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			 break;
		  case "PUT":
			 curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			 if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
			 break;
				 
		  default:
			 if ($data)
				$url = sprintf("%s?%s", $url, http_build_query($data));
	   }

	   // OPTIONS:
	   curl_setopt($curl, CURLOPT_URL, $url);
	   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		  'APIKEY: 111111111111111111111',
		  'Content-Type: application/json',
	   ));
	   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	   // EXECUTE:
	   $result = curl_exec($curl);
	   if(!$result){die("Connection Failure");}
	   curl_close($curl);
	   return $result;
	}
	
    public function register($request)
    {
        if (!get_option('users_can_register'))
        {
            $response['status'] = "error";
            $response['message'] = "User registration is disabled. Please enable it in Settings > Gereral.";
            return new WP_REST_Response($response, 200);
        }
        $parameters = $request->get_json_params();
        extract($parameters);
        /* if (!$username)
        {
            $response['status'] = "error";
            $response['message'] = "You must include 'username' var in your request. ";
            return new WP_REST_Response($response, 200);
        }
        else
        {
            $parameters['user_login'] = $username = sanitize_user($username);
        } */
        if (!$email)
        {
            $response['status'] = "error";
            $response['message'] = "You must include 'email' var in your request. ";
            return new WP_REST_Response($response, 200);
        }
        else
        {
            $parameters['user_email'] = $email = sanitize_user($email);
        }
        $username = strstr($parameters['user_email'], '@', true);

        $i = 0;
        while (username_exists($username))
        {
            $username .= ($username . $i);
            $i++;
        }
        $parameters['user_login'] = $username;

        if (!$nonce)
        {
            $response['status'] = "error";
            $response['message'] = "You must include 'nonce' var in your request. Use the 'get_nonce' Core API method. ";
            return new WP_REST_Response($response, 200);
        }
        else
        {
            $nonce = sanitize_text_field($nonce);
        }

        if (!$meta_fields['ddwc_driver_ssn_number'])
        {
            $response['status'] = "error";
            $response['message'] = "You must include 'ddwc_driver_ssn_number' var in your request.";
            return new WP_REST_Response($response, 200);
        }

        if (!$meta_fields['ddwc_driver_license_number'])
        {
            $response['status'] = "error";
            $response['message'] = "You must include 'ddwc_driver_license_number' var in your request.";
            return new WP_REST_Response($response, 200);
        }
        if (!$meta_fields['ddwc_driver_license_front'])
        {
            $response['status'] = "error";
            $response['message'] = "You must include 'ddwc_driver_license_front' var in your request.";
            return new WP_REST_Response($response, 200);
        }
        if (!$meta_fields['ddwc_neighbourhood_city'])
        {
            $response['status'] = "error";
            $response['message'] = "You must include 'ddwc_neighbourhood_city' var in your request.";
            return new WP_REST_Response($response, 200);
        }
        if (!$meta_fields['ddwc_driver_transportation_city'])
        {
            $response['status'] = "error";
            $response['message'] = "You must include 'ddwc_driver_transportation_city' var in your request.";
            return new WP_REST_Response($response, 200);
        }

        if ($password)
        {
            $parameters['user_pass'] = sanitize_text_field($password);
        }
        else
        {
            $parameters['user_pass'] = wp_generate_password();
        }
        $invalid_usernames = array(
            'admin'
        );
        $nonce_id = self::get_nonce_id('user', 'register');
        if (!wp_verify_nonce($nonce, $nonce_id))
        {
            $response['status'] = "error";
            $response['message'] = "Invalid access, unverifiable 'nonce' value. Use the 'get_nonce' API method. ";
            return new WP_REST_Response($response, 200);
        }
        if (!validate_username($username) || in_array($username, $invalid_usernames))
        {
            $response['status'] = "error";
            $response['message'] = "Username is invalid.";
            return new WP_REST_Response($response, 200);
        }
        elseif (username_exists($username))
        {
            $response['status'] = "error";
            $response['message'] = "Username already exists.";
            return new WP_REST_Response($response, 200);
        }
        if (!is_email($email))
        {
            $response['status'] = "error";
            $response['message'] = "E-mail address is invalid.";
            return new WP_REST_Response($response, 200);
        }
        elseif (email_exists($email))
        {
            $response['status'] = "error";
            $response['message'] = "E-mail address is already in use.";
            return new WP_REST_Response($response, 200);
        }
        $allowed_params = array(
            'user_login',
            'user_email',
            'user_pass',
            'display_name',
            'user_nicename',
            'user_url',
            'nickname',
            'first_name',
            'last_name',
            'description',
            'rich_editing',
            'user_registered',
            'role',
            'jabber',
            'aim',
            'yim',
            'comment_shortcuts',
            'admin_color',
            'use_ssl',
            'show_admin_bar_front'
        );
        if ($role)
        {
            $user['role'] = $role;
        }
        else
        {
            $user['role'] = get_option('default_role');
        }
        foreach ($parameters as $field => $value)
        {
            if (in_array($field, $allowed_params))
            {
                $user[$field] = trim(sanitize_text_field($value));
            }
            if (!empty($user['first_name']) and !empty($user['last_name']))
            {
                $user['display_name'] = $user['first_name'] . ' ' . $user['last_name'];
            }
        }
        $user_id = wp_insert_user($user);
        if (isset($password) && $notify == 'no')
        {
            $notify = '';
        }
        elseif ($notify != 'no')
        {
            $notify = $notify;
        }
        if ($user_id)
        {
            wp_new_user_notification($user_id, '', $notify);
            if (is_array($meta_fields))
            {
                foreach ($meta_fields as $field => $val)
                {
                    if ($field == 'ddwc_driver_license_back')
                    {

                        $attachmentid = self::Base64ToJpeg($val, $user_id);
                        $url = wp_get_attachment_url($attachmentid);
                        $data[$field] = update_user_meta($user_id, 'ddwc_driver_license_back', $url);

                    }
                    elseif ($field == 'ddwc_driver_license_front')
                    {

                        $attachmentid = self::Base64ToJpeg($val, $user_id);
                        $url = wp_get_attachment_url($attachmentid);
                        $data[$field] = update_user_meta($user_id, 'ddwc_driver_license_front', $url);

                    }
                    else 
                    {

                        $data[$field] = update_user_meta($user_id, $field, $val);

                    }
                }
            }
            $seconds = 1209600; //14 days
            $expiration = time() + apply_filters('auth_cookie_expiration', $seconds, $user_id, true);
            $cookie = wp_generate_auth_cookie($user_id, $expiration, 'logged_in');
            $cookie_admin = wp_generate_auth_cookie($user_id, $expiration, 'secure_auth');
            $user_info = get_userdata($user_id);
            $response['cookie'] = $cookie;
            $response['cookie_admin'] = $cookie_admin;
            $response['cookie_name'] = LOGGED_IN_COOKIE;
            $response['user_id'] = $user_id;
            $response['username'] = $user_info->user_login;
			
			$pc_response = self::callWpDriverAPI('POST', get_site_url().'/wp-json/driver_police_check/create_check', json_encode(
				array(
					'first_name' => $user_info->user_firstname,
					'last_name' => $user_info->last_name,
					'email' => $user_info->user_email,
					'type' => 'EMPLOYMENT',
					'reason' => 'Delivery Driver',
				)
			));
			$arr =json_decode($pc_response);
			update_user_meta($user_id,'police_check_id',$arr->id);
			update_user_meta($user_id,'police_check_continue_url',$arr->continue_url);
			$response['police_check_id'] 			=	$arr->id;
			$response['police_check_continue_url'] 	=	$arr->continue_url;
            return new WP_REST_Response($response, 200);
        }
        else
        {
            $response['status'] = 'error';
            $response['message'] = 'Something wrong happend.';
            return new WP_REST_Response($response, 200);
        }
    }

    public function generate_auth_cookie($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$username && !$email)
        {
            $response['status'] = "error";
            $response['message'] = "You must include 'username' or 'email' var in your request to generate cookie.";
            return new WP_REST_Response($response, 200);
        }
        if (!$password)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'password' var in your request.";
            return new WP_REST_Response($response, 200);
        }
        /* if (!$device_id)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'device_id' var in your request.";
            return new WP_REST_Response($response, 200);
        } */
        $seconds = 1209600; //14 days
        if ($email)
        {
            if (is_email($email))
            {
                if (!email_exists($email))
                {
                    $response['status'] = "error";
                    $response['message'] = "email does not exist.";
                    return new WP_REST_Response($response, 200);
                }
            }
            else
            {
                $response['status'] = "error";
                $response['message'] = "Invalid email address.";
                return new WP_REST_Response($response, 200);
            }
            $user_obj = get_user_by('email', $email);
            $user = wp_authenticate($user_obj
                ->data->user_login, $password);
        }
        else
        {
            $user = wp_authenticate($username, $password);
        }
        if (is_wp_error($user))
        {
            remove_action('wp_login_failed', $username);
            $response['status'] = "error";
            $response['error'] = "401";
            $response['message'] = "Invalid username/email and/or password.";
            return new WP_REST_Response($response, 200);
        }

        if (!in_array('driver', $user->roles))
        {
            $response['status'] = "error";
            $response['message'] = "User Is Not a driver.";
            return new WP_REST_Response($response, 200);
        }

        $expiration = time() + apply_filters('auth_cookie_expiration', $seconds, $user->ID, true);
        $cookie = wp_generate_auth_cookie($user->ID, $expiration, 'logged_in');
        $cookie_admin = wp_generate_auth_cookie($user->ID, $expiration, ‘secure_auth’);
        preg_match('|src="(.+?)"|', get_avatar($user->ID, 512) , $avatar);

        update_user_meta($user->ID, 'device_id', $device_id);

        $fields = array();
        foreach (get_user_meta($user->ID) as $k => $v)
        {
            if ($k == 'ddwc_driver_picture' and !empty($v))
            {
                $fields[$k] = unserialize($v[0]) ['url'];
            }
			elseif ($k == 'id-verify-applicant' and !empty($v))
            {
                $fields[$k] = unserialize($v[0]);
            }
            elseif ($k == 'id-verify-qa' and !empty($v))
            {
                $fields[$k] = unserialize($v[0]);
            }
            elseif ($k == 'id_verify_done' and !empty($v))
            {
                $fields[$k] = unserialize($v[0]);
            }
            elseif ($k == 'service-result' and !empty($v))
            {
                $fields[$k] = unserialize($v[0]);
            }
            else
            {
                $fields[$k] = $v[0];
            }
        }
        unset($fields['session_tokens']);

        $payment_type = get_option('ddwc_driver_payment_type');
        if (get_option('ddwc_driver_payment_type') == 'terawallet')
        {
            $payment_menu = 'no';
        }
        if (get_option('ddwc_driver_payment_type') == 'stripe')
        {
            $payment_menu = 'yes';
        }

        return array(
            "cookie" => $cookie,
            "cookie_admin" => $cookie_admin,
            "cookie_name" => LOGGED_IN_COOKIE,
            "payment_type" => $payment_type,
            "payment_menu" => $payment_menu,
            "user" => array(
                "id" => $user->ID,
                "username" => $user->user_login,
                "nicename" => $user->user_nicename,
                "email" => $user->user_email,
                "url" => $user->user_url,
                "registered" => $user->user_registered,
                "displayname" => $user->display_name,
                "firstname" => $user->user_firstname,
                "lastname" => $user->last_name,
                "nickname" => $user->nickname,
                "description" => $user->user_description,
                "capabilities" => $user->wp_capabilities,
                "avatar" => $avatar[1],
                "meta" => $fields
            ) ,
        );
    }

    public function validate_auth_cookie($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $valid = wp_validate_auth_cookie($cookie, 'logged_in') ? true : false;
        return array(
            "valid" => $valid
        );
    }

    public function get_currentuserinfo($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' var in your request. Use the `generate_auth_cookie` API method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user = get_userdata($user_id);
		
		/* $pc_response = self::callWpDriverAPI('POST', get_site_url().'/wp-json/driver_police_check/create_check', json_encode(
			array(
				'first_name' => $user->user_firstname,
				'last_name' => $user->last_name,
				'email' => $user->user_email,
				'type' => 'EMPLOYMENT',
				'reason' => 'Delivery Driver',
			)
		));
		$arr =json_decode($pc_response);
		update_user_meta($user->ID,'police_check_id',$arr->id);
		update_user_meta($user->ID,'police_check_continue_url',$arr->continue_url);  */
		
        preg_match('|src="(.+?)"|', get_avatar($user->ID, 32) , $avatar);
        $fields = array();
        foreach (get_user_meta($user->ID) as $k => $v)
        {
            if ($k == 'ddwc_driver_picture' and !empty($v))
            {
                $fields[$k] = unserialize($v[0]) ['url'];
            }
			elseif ($k == 'id-verify-applicant' and !empty($v))
            {
                $fields[$k] = unserialize($v[0]);
            }
            elseif ($k == 'id-verify-qa' and !empty($v))
            {
                $fields[$k] = unserialize($v[0]);
            }
            elseif ($k == 'id_verify_done' and !empty($v))
            {
                $fields[$k] = unserialize($v[0]);
            }
            elseif ($k == 'service-result' and !empty($v))
            {
                $fields[$k] = unserialize($v[0]);
            }
            else
            {
                $fields[$k] = $v[0];
            }
        }
        unset($fields['session_tokens']);
        $payment_type = get_option('ddwc_driver_payment_type');
        if (get_option('ddwc_driver_payment_type') == 'terawallet')
        {
            $payment_menu = 'no';
        }
        if (get_option('ddwc_driver_payment_type') == 'stripe')
        {
            $payment_menu = 'yes';
        }
		
        return array(
            "payment_type" => $payment_type,
            "payment_menu" => $payment_menu,
            "user" => array(
                "id" => $user->ID,
                "username" => $user->user_login,
                "nicename" => $user->user_nicename,
                "email" => $user->user_email,
                "url" => $user->user_url,
                "registered" => $user->user_registered,
                "displayname" => $user->display_name,
                "firstname" => $user->user_firstname,
                "lastname" => $user->last_name,
                "nickname" => $user->nickname,
                "description" => $user->user_description,
                "capabilities" => $user->wp_capabilities,
                "avatar" => $avatar[1],
                "meta" => $fields
            )
        );
    }

    public function get_user_meta($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        if ($meta)
        {
            $data[$meta_key] = get_user_meta($user_id, $meta_key);
        }
        else
        {
            $meta = get_user_meta($user_id);
            $data = array_filter(array_map(function ($a)
            {
                return $a[0];
            }
            , $meta));
        }
        return $data;
    }

    public function update_user_meta($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }

        if (!$meta)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'meta' var in your request , wchich cantain array of metakeys and value which need to update.";
            return new WP_REST_Response($response, 200);
        }
        if (!empty($meta['first_name']) and !empty($meta['last_name']))
        {
            wp_update_user(array(
                'ID' => $user_id,
                'display_name' => $meta['first_name'] . ' ' . $meta['last_name']
            ));
        }
        foreach ($meta as $k => $v)
        {
            if (is_array($v))
            {
                $meta_values = array_map('trim', $v);
                update_user_meta($user_id, $k, $meta_values);
            }
            else
            {
                update_user_meta($user_id, $k, $v);
            }
            $data[$k] = 'updated';
        }
        /*  if (!$meta_value)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'meta_value' var in your request. If you have multiple values for any meta_key, you must send it as an array meta_value[] in POST method.";
            return new WP_REST_Response($response, 200);
        }
        */
        return $data;
    }

    public function update_driver_availability($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }

        if (!$status)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'status' var in your request , which may be empty 'on' or 'off'";
            return new WP_REST_Response($response, 200);
        }
		$payment_type  =  get_option( 'ddwc_driver_payment_type' );
	
		if($payment_type == 'stripe'){
			if (!get_user_meta($user_id, 'stripe_account_id', true))
			{
				$response['status'] = "error";
				$response['message'] = "Please enter account details first for accepting deliveries.";
				return new WP_REST_Response($response, 200);
			}
			$response['Stripe Account ID'] = get_user_meta($user_id, 'stripe_account_id', true);
		}
		
		if (get_option('ddwc_police_check_enable') == 'yes')
        {
			if (empty( get_user_meta($user_id, 'police_check_id', true) ) ){
				
				$user_info = get_userdata($user_id);
				$pc_response = self::callWpDriverAPI('POST', get_site_url().'/wp-json/driver_police_check/create_check', json_encode(
					array(
						'first_name' => $user_info->user_firstname,
						'last_name' => $user_info->last_name,
						'email' => $user_info->user_email,
						'type' => 'EMPLOYMENT',
						'reason' => 'Delivery Driver',
					)
				));
				
				$arr =	json_decode($pc_response);
				
				update_user_meta($user_id,'police_check_id',$arr->id);
				update_user_meta($user_id,'police_check_continue_url',$arr->continue_url);
				
				$response['message'] = "Your details are sent to police check verification";
				$response['status'] = "error";
				return new WP_REST_Response($response, 200);	
				
			}else{
			
				if ( !empty( get_user_meta($user_id, 'police_check_status', true) ) ){
					
					$policestatus	=	get_user_meta($user_id, 'police_check_status', true);
					
					/* if($policestatus	==	'id-verify-done'){
						$response['message'] = ".";
					} */
					/* if($policestatus 	==	'service-result'){
						$response['message'] = ".";
					} */
					if($policestatus	==	'id-verify-applicant'){
						$response['message'] = "Current Police check status is applicant verified";
						$response['status'] = "error";
						return new WP_REST_Response($response, 200);	
					}
					
					if($policestatus 	==	'id-verify-qa'){
						$response['message'] = "Current Police check status is QA verified";
						$response['status'] = "error";
						return new WP_REST_Response($response, 200);	
					}
					
					
				}else{
					
					$response['status'] = "error";
					$response['message'] = "Police check still not verify your details.";
					return new WP_REST_Response($response, 200);
					
				}
			}
           
        }

        if ($status == 'off')
        {
            update_user_meta($user_id, 'ddwc_driver_availability', '');
            $response['Current Driver Status'] = 'not available';
			$response['status'] = "success";
        }
        if ($status == 'on')
        {
            update_user_meta($user_id, 'ddwc_driver_availability', 'on');
            $response['Current Driver Status'] = 'available';
			$response['status'] = "success";
        }

       return new WP_REST_Response($response, 200);
    }

    public function update_device_id($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }

        if (!$device_id)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'device_id' var in your request.";
            return new WP_REST_Response($response, 200);
        }

        update_user_meta($user_id, 'device_id', $device_id);

        $response['device_id'] = $device_id;
        $response['message'] = "Device Id Updated";
        return new WP_REST_Response($response, 200);

    }

    public function delete_user_meta($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        if (!$meta_key)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'meta_key' var in your request.";
            return new WP_REST_Response($response, 200);
        }
        if (!$meta_value)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'meta_value' var in your request. If you have multiple values for any meta_key, you must send it as an array meta_value[] in POST method.";
            return new WP_REST_Response($response, 200);
        }
        $meta_value = sanitize_text_field($meta_value);
        $data['deleted'] = delete_user_meta($user_id, $meta_key, $meta_value);
        return $data;
    }

    public function change_password($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
        if (!$password)
        {
            $response['status'] = "error";
            $response['message'] = 'Enter a password.';
            return new WP_REST_Response($response, 200);
        }

        global $wpdb;

        $hash = wp_hash_password($password);
        $wpdb->update($wpdb->users, array(
            'user_pass' => $hash,
            'user_activation_key' => '',
        ) , array(
            'ID' => $user_id
        ));
        //wp_set_password($password,$user_id);
        $response['status'] = "success";
        $response['message'] = "Password Successfully Changed";
        return new WP_REST_Response($response, 200);
        //return $data;
    }

    public function forgot_password($request)
    {
        $parameters = $request->get_json_params();
        extract($parameters);

        $login = isset($user_login) ? sanitize_user(wp_unslash($user_login)) : ''; // WPCS: input var ok, CSRF ok.
        if (empty($login))
        {

            $response['status'] = "error";
            $response['message'] = 'Enter a username or email address.';
            return new WP_REST_Response($response, 200);

        }
        else
        {
            // Check on username first, as customers can use emails as usernames.
            $user_data = get_user_by('login', $login);
        }

        // If no user found, check if it login is email and lookup user based on email.
        if (!$user_data && is_email($login) && apply_filters('woocommerce_get_username_from_email', true))
        {
            $user_data = get_user_by('email', $login);
        }

        $errors = new WP_Error();

        do_action('lostpassword_post', $errors);

        if ($errors->get_error_code())
        {
            $response['status'] = "error";
            $response['message'] = $errors->get_error_message();
            return new WP_REST_Response($response, 200);
        }

        if (!$user_data)
        {
            $response['status'] = "error";
            $response['message'] = 'Invalid username or email.';
            return new WP_REST_Response($response, 200);
        }

        if (is_multisite() && !is_user_member_of_blog($user_data->ID, get_current_blog_id()))
        {
            $response['status'] = "error";
            $response['message'] = 'Invalid username or email.';
            return new WP_REST_Response($response, 200);
        }

        // Redefining user_login ensures we return the right case in the email.
        $user_login = $user_data->user_login;

        do_action('retrieve_password', $user_login);

        $allow = apply_filters('allow_password_reset', true, $user_data->ID);

        if (!$allow)
        {
            $response['status'] = "error";
            $response['message'] = 'Password reset is not allowed for this user';
            return new WP_REST_Response($response, 200);

        }
        elseif (is_wp_error($allow))
        {
            $response['status'] = "error";
            $response['message'] = $allow->get_error_message();
            return new WP_REST_Response($response, 200);
        }

        // Get password reset key (function introduced in WordPress 4.4).
        $key = get_password_reset_key($user_data);

        // Send email notification.
        WC()->mailer(); // Load email classes.
        do_action('woocommerce_reset_password_notification', $user_login, $key);
        $response['status'] = "success";
        $response['message'] = 'You will receive a link to create a new password via email';
        return new WP_REST_Response($response, 200);

    }

}
$wpJsonDriverApi = new WPJsonDriverApi();
