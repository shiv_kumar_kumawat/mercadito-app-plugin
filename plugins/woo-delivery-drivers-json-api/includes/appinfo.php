<?php include_once (ABSPATH . 'wp-admin/includes/plugin.php');
if (!is_plugin_active('woocommerce/woocommerce.php'))
{
    return;
}
class WPJsonAppInfoApi
{
    public function __construct()
    {
        add_action('rest_api_init', array(
            $this,
            'JsonAppInfoApi'
        ));
    }
    public function JsonAppInfoApi()
    {
        register_rest_route('driverappinfoapi', '/appdata', array(
            'methods' => array('GET','POST'),
            'callback' => array(
                $this,
                'appdata'
            ) ,
        ));
        register_rest_route('driverappinfoapi', '/signup_fields', array(
            'methods' => array('GET','POST'),
            'callback' => array(
                $this,
                'signup_fields'
            ) ,
        ));
        register_rest_route('driverappinfoapi', '/profile_fields', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'profile_fields'
            ) ,
        ));
        register_rest_route('driverappinfoapi', '/payment_fields', array(
            'methods' => 'POST',
            'callback' => array(
                $this,
                'payment_fields'
            ) ,
        ));
		
    }
    public function appdata($request)
    {
        $appinfo = get_option('woodeliverydriverappinfo');
        
            foreach (get_option('woodeliverydriverappinfo') as $k => $v)
            {
                $response['appdata'][$k] = $v;
            }
        
        $response['status'] = "success";
        return new WP_REST_Response($response, 200);
    }
    public function signup_fields($request)
    {
		
        //$cities_list = get_option('ddwc_driver_citys_list');
        //$citylist_array = explode(', ', $cities_list);

        $arg = array(
                    'taxonomy' => 'area',
                    'parent' => 0,
                    'hide_empty' => false
                );
        $all_parent = get_terms($arg);
 
        $all_cities_list = array();
		$i=1;
		$firstcity='';
        foreach ($all_parent as $key => $term) {
            $all_cities_list[] = $term->term_id.'-'.$term->name;
			if($i == 1){
				$firstcity	= $term->term_id;
			}
			$i++;
        }

        $areaarg = array(
                    'taxonomy' => 'area',
                    'parent' => $firstcity,
                    'hide_empty' => false
                );
        $area = get_terms($areaarg);
 
        $arealist = array();
        foreach ($area as $key => $term) {
			$arealist[] = $term->name;
        } 
		
        $response['fields'] = apply_filters('driver_signup_fields', array(
            array(
                'label' => 'First Name',
                'name' => 'first_name',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'face_unlock_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'First Name',
				"validation_message"=>null,
                'value' => ''
            ) ,
           array(
                'label' => 'Last Name',
                'name' => 'last_name',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'face_unlock_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => '',
                'value' =>  'Last Name',
				"validation_message"=>null,
            ) ,
            array(
                'label' => 'Email',
                'name' => 'email',
                'tag' => 'input',
                'type' => 'email',
                'icon' => 'mail_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => false,
                'placeholder' => 'Email',
				"validation_message"=>null,
                'value' => ''
            ) ,
           array(
                'label' => 'Phone',
                'name' => 'phone',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'phone_android',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' =>  'Phone',
				"validation_message"=>null,
                'value' => ''
            ) ,
            array(
                'label' => 'Password',
                'name' => 'password',
                'tag' => 'input',
                'type' => 'password',
                'icon' => 'lock',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => false,
                'placeholder' => 'Password',
				"validation_message"=>null,
                'value' => ''
            ) ,
            array(
                'label' => 'Date Of Birth',
                'name' => 'ddwc_driver_dob',
                'tag' => 'input',
                'type' => 'calendar',
                'icon' => 'cake_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Date Of Birth',
				"validation_message"=>null,
                'value' => ''
            ) ,
            /*array(
                'label' => 'Delivery Town/City',
                'name' => 'ddwc_driver_transportation_city',
                'tag' => 'select',
                'type' => 'multiple',
                'icon' => 'local_shipping_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
				'options' =>$citylist_array,
                'placeholder' =>'Delivery Town/City',
				"validation_message"=>null,
                'value' => ''
            ) ,*/
            array(
                'label' => 'Neighbourhood City',
                'name' => 'ddwc_neighbourhood_city',
                'tag' => 'select',
                'type' => 'single',
                'icon' => 'directions_bus_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
                'ismeta' => true,
                'placeholder' => 'Neighbourhood City',
                'options' => $all_cities_list,
                'validation_message'=>null,
                'value' => ''
            ) ,
           /*  array(
                'label' => 'City',
                'name' => 'ddwc_neighbourhood_select_cities',
                'tag' => 'select',
                'type' => 'multiple',
                'icon' => 'local_shipping_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
                'ismeta' => true,
                'city' => [],
                'placeholder' =>'City',
                "validation_message"=>null,
                'value' => ''
            ) , */
            array(
                'label' => 'City',
                'name' => 'ddwc_driver_transportation_city',
                'tag' => 'select',
                'type' => 'multiple',
                'icon' => 'local_shipping_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
                'ismeta' => true,
                'options' =>$arealist,
                'placeholder' =>'City',
                "validation_message"=>null,
                'value' => ''
            ) ,
            array(
                'label' => 'Transpotation Type',
                'name' => 'ddwc_driver_transportation_type',
                'tag' => 'select',
                'type' => 'single',
                'icon' => 'directions_bus_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Transpotation Type',
                'options' => apply_filters('transportation_type',array(
					"Bicycle",
					"Motorcycle",
					"Car",
					"SUV",
					"Truck"
				)),
				"validation_message"=>null,
                'value' => ''
            ) ,
            array(
                'label' => 'Vehical Model',
                'name' => 'ddwc_driver_vehicle_model',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'motorcycle_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Vehical Model',
				"validation_message"=>null,
                'value' => ''
            ) ,
            array(
                'label' => 'Vehical Color',
                'name' => 'ddwc_driver_vehicle_color',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'motorcycle_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' =>'Vehical Color',
				"validation_message"=>null,
                'value' => ''
            ) ,
            array(
                'label' => 'Licence Plate Number',
                'name' => 'ddwc_driver_license_plate',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'straighten_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Licence Plate Number',
				"validation_message"=>null,
                'value' => ''
            ) ,
            array(
                'label' => 'Social Security Number',
                'name' => 'ddwc_driver_ssn_number',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'straighten_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Social Security Number',
				"validation_message"=>null,
                'value' => ''
            ) ,
            array(
                'label' => 'Driving Licence Number',
                'name' => 'ddwc_driver_license_number',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'subtitles_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Driving Licence Number',
				"validation_message"=>null,
                'value' => ''
            ) ,
            array(
                'label' => 'Front Licence Image',
                'name' => 'ddwc_driver_license_front',
                'tag' => 'input',
                'type' => 'file',
                'icon' => 'camera_alt_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'accept' => 'image/*',
                'placeholder' =>  'Front Licence Image',
				"validation_message"=>null,
                'value' => ''
            ) ,
            array(
                'label' => 'Back Licence Image',
                'name' => 'ddwc_driver_license_back',
                'tag' => 'input',
                'type' => 'file',
                'icon' => 'camera_alt_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'accept' => 'image/*',
                'placeholder' => 'Back Licence Image',
				"validation_message"=>null,
                'value' => ''
            ) ,
        ));
        $response['status'] = "success";
        return new WP_REST_Response($response, 200);
    }
	
	
    public function profile_fields($request)
    {
		
		$parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' var in your request. Use the `generate_auth_cookie` API method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }
		$user = get_userdata($user_id);
		
        //$cities_list = get_option('ddwc_driver_citys_list');
        //$citylist_array = explode(', ', $cities_list);

        $arg = array(
                    'taxonomy' => 'area',
                    'parent' => 0,
                    'hide_empty' => false
                );
        $all_parent = get_terms($arg);
 
        $all_cities_list = array();
        foreach ($all_parent as $key => $term) {
            $all_cities_list[] = $term->term_id.'-'.$term->name;
        }
		
		$areaarg = array(
                    'taxonomy' => 'area',
                    'parent' => get_user_meta($user_id,'ddwc_neighbourhood_city',true),
                    'hide_empty' => false
                );
        $area = get_terms($areaarg);
 
        $arealist = array();
        foreach ($area as $key => $term) {
			$arealist[] = $term->name;
        } 
		
        $response['fields'] = apply_filters('driver_profile_fields', array(
            array(
                'label' => 'First Name',
                'name' => 'first_name',
                'tag' => 'input',
                'type' => 'text',
                'icon' =>  'face_unlock_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true, 
                'placeholder' => 'First Name',
				"validation_message"=>null,
                'value' => get_user_meta($user_id,'first_name',true)
            ) ,
            array(
                'label' => 'Last Name',
                'name' => 'last_name',
                'tag' => 'input',
                'type' => 'text',
                'icon' =>  'face_unlock_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Last Name',
				"validation_message"=>null,
                'value' =>  get_user_meta($user_id,'last_name',true),
            ) ,
            array(
                'label' => 'Email',
                'name' => 'email',
                'tag' => 'input',
                'type' => 'email',
                'icon' => 'mail_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => true,
				'ismeta' => false,
                'placeholder' => 'Email',
				"validation_message"=>null,
                'value' => $user->user_email
            ) ,
            array(
                'label' => 'Phone',
                'name' => 'phone',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'phone_android',
                'icon_color' => '',
                'required' => false,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' =>  'Phone',
				"validation_message"=>null,
                'value' =>   get_user_meta($user_id,'phone',true),
            ) ,
            array(
                'label' => 'Date Of Birth',
                'name' => 'ddwc_driver_dob',
                'tag' => 'input',
                'type' => 'calendar',
                'icon' => 'cake_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Date Of Birth',
				"validation_message"=>null,
                'value' =>  get_user_meta($user_id,'ddwc_driver_dob',true),
            ) ,
            /*array(
                'label' => 'Delivery Town/City',
                'name' => 'ddwc_driver_transportation_city',
                'tag' => 'select',
                'type' => 'multiple',
                'icon' => 'local_shipping_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
				'options' =>$citylist_array,
                'placeholder' =>'Delivery Town/City',
				"validation_message"=>null,
                'value' => get_user_meta($user_id,'ddwc_driver_transportation_city',true),
            ) ,*/
            array(
                'label' => 'Neighbourhood City',
                'name' => 'ddwc_neighbourhood_city',
                'tag' => 'select',
                'type' => 'single',
                'icon' => 'directions_bus_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => false,
                'ismeta' => true,
                'placeholder' => 'Neighbourhood City',
                'options' => $all_cities_list,
                'validation_message'=>null,
                'value' => get_user_meta($user_id,'ddwc_neighbourhood_city',true)
            ) ,
            array(
                'label' => 'City',
                'name' => 'ddwc_driver_transportation_city',
                'tag' => 'select',
                'type' => 'multiple',
                'icon' => 'local_shipping_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => false,
                'ismeta' => true,
                'options' => $arealist,
                'placeholder' =>'City',
                "validation_message"=>null,
                'value' => get_user_meta($user_id,'ddwc_driver_transportation_city',true)
            ) ,
            array(
                'label' => 'Transpotation Type',
                'name' => 'ddwc_driver_transportation_type',
                'tag' => 'select',
                'type' => 'single',
                'icon' => 'directions_bus_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Transpotation Type',
                'options' => apply_filters('transportation_type',array(
					"Bicycle",
					"Motorcycle",
					"Car",
					"SUV",
					"Truck"
				)),
				"validation_message"=>null,
                'value' => get_user_meta($user_id,'ddwc_driver_transportation_type',true),
            ) ,
            array(
                'label' => 'Vehical Model',
                'name' => 'ddwc_driver_vehicle_model',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'motorcycle_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Vehical Model',
				"validation_message"=>null,
                'value' =>  get_user_meta($user_id,'ddwc_driver_vehicle_model',true),
            ) ,
            array(
                'label' => 'Vehical Color',
                'name' => 'ddwc_driver_vehicle_color',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'motorcycle_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' =>'Vehical Color',
				"validation_message"=>null,
                'value' =>  get_user_meta($user_id,'ddwc_driver_vehicle_color',true),
            ) ,
            array(
                'label' => 'Licence Plate Number',
                'name' => 'ddwc_driver_license_plate',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'straighten_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => true,
				'ismeta' => true,
                'placeholder' => 'Licence Plate Number',
				"validation_message"=>null,
                'value' =>   get_user_meta($user_id,'ddwc_driver_license_plate',true),
            ) ,
            array(
                'label' => 'Social Security Number',
                'name' => 'ddwc_driver_ssn_number',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'straighten_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => true,
				'ismeta' => true,
                'placeholder' => 'Social Security Number',
				"validation_message"=>null,
                'value' =>  get_user_meta($user_id,'ddwc_driver_ssn_number',true),
            ) ,
            array(
                'label' => 'Driving Licence Number',
                'name' => 'ddwc_driver_license_number',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'subtitles_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => true,
				'ismeta' => true,
                'placeholder' => 'Driving Licence Number',
				"validation_message"=>null,
                'value' =>  get_user_meta($user_id,'ddwc_driver_license_number',true),
            ) ,
            array(
                'label' => 'Front Licence Image',
                'name' => 'ddwc_driver_license_front',
                'tag' => 'input',
                'type' => 'file',
                'icon' => 'camera_alt_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => true,
				'ismeta' => true,
                'accept' => 'image/*',
                'placeholder' =>  'Front Licence Image',
				"validation_message"=>null,
                'value' =>   get_user_meta($user_id,'ddwc_driver_license_front',true),
            ) ,
            array(
                'label' => 'Back Licence Image',
                'name' => 'ddwc_driver_license_back',
                'tag' => 'input',
                'type' => 'file',
                'icon' => 'camera_alt_sharp',
                'icon_color' => '',
                'required' => false,
                'readonly' => true,
				'ismeta' => true,
                'accept' => 'image/*',
                'placeholder' => 'Back Licence Image',
				"validation_message"=>null,
                'value' =>  get_user_meta($user_id,'ddwc_driver_license_back',true),
            ) ,
        ));
        $response['status'] = "success";
        return new WP_REST_Response($response, 200);
    }
	
	
	public function payment_fields($request)
    {
		
		$parameters = $request->get_json_params();
        extract($parameters);
        if (!$cookie)
        {
            $response['status'] = "error";
            $response['message'] = "You must include a 'cookie' var in your request. Use the `generate_auth_cookie` API method.";
            return new WP_REST_Response($response, 200);
        }
        $user_id = wp_validate_auth_cookie($cookie, 'logged_in');
        if (!$user_id)
        {
            $response['status'] = "error";
            $response['message'] = "Invalid authentication cookie. Use the `generate_auth_cookie` method.";
            return new WP_REST_Response($response, 200);
        }

        $response['fields'] = apply_filters('driver_payment_fields', array(
            array(
                'label' => 'Account Number',
                'name' => 'ddwc_driver_account_number',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'account_balance_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Account Number',
				"validation_message"=>null,
                'value' =>  str_repeat('*', strlen(get_user_meta($user_id, 'ddwc_driver_account_number', true)) - 4) . substr(get_user_meta($user_id, 'ddwc_driver_account_number', true) , -4) ,
            ) ,
            array(
                'label' => 'Bank Name',
                'name' => 'ddwc_driver_bank_name',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'account_balance_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Bank Name',
				"validation_message"=>null,
                'value' =>  get_user_meta($user_id,'ddwc_driver_bank_name',true),
            ) ,
            array(
                'label' => 'Account Holder Name',
                'name' => 'ddwc_driver_account_holder_name',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'face_unlock_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' =>  'Account Holder Name',
				"validation_message"=>null,
                'value' =>  get_user_meta($user_id,'ddwc_driver_account_holder_name',true),
            ) ,
            array(
                'label' =>  'Social Security Number',
                'name' => 'ddwc_driver_ssn_number',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'straighten_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' =>  'Social Security Number',
				"validation_message"=>null,
                'value' => str_repeat('*', strlen(get_user_meta($user_id, 'ddwc_driver_ssn_number', true)) - 4) . substr(get_user_meta($user_id, 'ddwc_driver_ssn_number', true) , -4) ,
            ) ,
            array(
                'label' => 'Routing Number',
                'name' => 'ddwc_driver_routing_number',
                'tag' => 'input',
                'type' => 'text',
                'icon' => 'ballot_sharp',
                'icon_color' => '',
                'required' => true,
                'readonly' => false,
				'ismeta' => true,
                'placeholder' => 'Routing Number',
				"validation_message"=>null,
                'value' =>  str_repeat('*', strlen(get_user_meta($user_id, 'ddwc_driver_routing_number', true)) - 4) . substr(get_user_meta($user_id, 'ddwc_driver_routing_number', true) , -4) ,
            ) ,
            
        ));
        $response['status'] = "success";
        return new WP_REST_Response($response, 200);
    }
}
$wpjsonappinfoapi = new WPJsonAppInfoApi();

