<?php

$woo_api_driver		=	get_option('woo_api_driver'); 

$ddwc_police_check_enable		=	get_option('ddwc_police_check_enable');
$ddwc_police_check_api_key		=	get_option('ddwc_police_check_api_key');
$ddwc_police_check_api_secret		=	get_option('ddwc_police_check_api_secret');

$ddwc_pro_settings_firebase_serverkey		=	get_option('ddwc_pro_settings_firebase_serverkey');

$appinfo = get_option('woodeliverydriverappinfo');

if (empty($_GET['tab'])) {
    $tab = 'api_lists';
} elseif ($_GET['tab'] == 'api_lists') {
    $tab = 'api_lists';
} else {
    $tab = $_GET['tab']; 
}

/*if($woo_api_driver){ 
	if (class_exists('WPJsonDriverApi') and class_exists('WPJsonDriverOrderApi')) {*/

		$user_methods = get_class_methods(new WPJsonDriverApi());
		$remove_user_methods=array('__construct','get_api_controllers','get_controller_method','controller_method_exists','get_nonce_id','JsonDriverApi','callWpDriverAPI','Base64ToJpeg');

		$order_methods = get_class_methods(new WPJsonDriverOrderApi());
		$remove_order_methods=array('__construct','JsonDriverOrderApi');
/*	}
}*/

$appinfo_methods = get_class_methods(new WPJsonAppInfoApi());
$remove_appinfo_methods=array('__construct','JsonAppInfoApi');


$firebase_methods = get_class_methods(new WPJsonFirebaseApi());
$remove_firebase_methods=array('__construct','JsonFirebaseApi','callFirebaseAPI','firebase_api_trigger');



?>

<div class="wrap">
<h1 class="wp-heading-inline">Settings</h1>
<div class="row" style="background: white;padding: 3%;float: left;width: 94%;">
	<div class="col-75">
		<div class="container">
			<div class="apitab">
				<button class="tablinks <?php if($tab=='api_lists'){echo 'active';}?>" onclick="opentab(event, 'api_lists')">Api Lists</button>
				<button class="tablinks <?php if($tab=='police_check_settings'){echo 'active';}?>" onclick="opentab(event, 'police_check_settings')">Police check settings</button>
				<button class="tablinks <?php if($tab=='firebase'){echo 'active';}?>" onclick="opentab(event, 'firebase')">Firebase</button>
				<button class="tablinks <?php if($tab=='app_info'){echo 'active';}?>" onclick="opentab(event, 'app_info')">App Info</button>
			</div>
			<!-- app info section -->

			<div id="app_info" <?php if($tab=='app_info' ){echo 'style="display: block;"';}?> class="apitabcontent">
             <form method="post" action="?page=woo-driver-api&tab=app_info" enctype="multipart/form-data" >
			   <div class="wrapper">
                  <div class="row app_info speceq">
                	<div class="col-100">
						<hr>
							<b>App Intro Screen</b>
						<hr> 
					</div>
                    <div class="row">
                        <div class="aap-info-enable">
                            <label for= "info_screen_enable">Enable Intro Screen </label>
                            <fieldset>
                            <input type="checkbox" name="intro_screen_enable" <?php if($appinfo['intro_screen_enable'] == "yes"){echo "checked";}?> value="yes" />
                            </fieldset>
                        </div>
					</div>
                    <div class="row table-introscreen">
                   <fieldset>
                            <table>
                                <thead>
                                    <tr>
                                        <th style="width: 120px;">Info Icon</th>
                                        <th>Info Title</th>
                                        <th>Info Content</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                
									<?php
									
									$appinfos = $appinfo['intro_screen'];
										if(!empty($appinfos)) {
										$i = 1;
										foreach($appinfos as $k => $v){
                                    ?>
                                        <tr class="icon_screen_detail">
                                            <td style="width: 120px;">
                                            	<div class="custom_css">
                                                <a href="#" class="url_img_<?php echo $i;?> url_img" onclick="media_upload_url('url[]')">
                                                	<img src="<?php echo $v['icon'];?>" />
                                                	<input type="hidden" required name="url[]" value="<?php echo $v['icon'];?>"></a>
	                                            <a href="#" class="remove_url">Remove image</a>
	                                        </div>
                                            </td>
                                            <td>
                                                <input style=" width: 100%; " type="text" name="title[]" required placeholder="Title" value="<?php echo $v['heading'];?>"/>
                                            </td>
                                            <td >
                                                <textarea  style="width: 100%;height: 100px;" name="content[]" required placeholder="Content"><?php echo $v['content'];?></textarea>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="remove_field">Remove</a>
                                            </td>
                                        </tr>
                                    <?php 
                                    $i++;
                                    }
                                    }
                                    else{ ?>
                                        <tr class="icon_screen_detail">
                                            <td style="width: 120px;">
                                            	<div class="custom_css">
												<a href="#" class="url_img_1 url_img bganchor" onclick="media_upload_url('url[]')">Upload image</a>
												<a href="#" class="remove_url" style="display:none">Remove image</a>
											</div>
                                            </td>
                                            <td>
                                                <input style="width: 100%;" type="text" name="title[]" placeholder="Title" />
                                            </td>
                                            <td>
                                                <textarea  style="width: 100%;height: 100px;" name="content[]" placeholder="Content" /></textarea>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="remove_field">Remove</a>
                                            </td>
                                        </tr>
                                    <?php }
                                    ?>
                                    
                                </tbody>
                            </table>
                            </fieldset>
                            <p>
                                <button class="add_fields">Add More Fields <span>+</span></button>
                            </p> 
                        </div>
                      </div>
                      <div class="row logo_sec speceq">
                        <div class="col-100">
                            <hr> 
                                <b>App Logo</b>
                            <hr> 
                        </div>
                        <div class="col-100">
                            <div class="app_logo">
                                <label for="app_logo">App Logo</label>
                                <div class="logo_urldiv">
                                <?php
								if($appinfo['logo']) {
 
									echo '<a href="#" class="logo_url_img" onclick="media_upload_url(\'logo\')"><img src="' . $appinfo['logo'] . '" /><input type="hidden" name="logo" value="' . $appinfo['logo'] . '"></a>
									<a href="#" class="logo_remove">Remove image</a>
									';

									} else {

									echo '<a href="#" class="logo_url_img bganchor" onclick="media_upload_url(\'logo\')">Upload image</a>
									<a href="#" class="logo_remove" style="display:none">Remove image</a>';

									}
									?>
								</div>
                               
                                
                            </div>
                        </div>
                       </div>
                       <div class="row theeme_sec speceq">
                        <div class="col-100">
                            <hr> 
                                <b>App Theme(color)</b>
                            <hr> 
                        </div>
                        <div class="col-100">
                            <div class="app_theme_color">
                                <label for="theme_color">App Theme</label>
                                <fieldset>
                                <input type="color" value="<?php echo $appinfo['background'];?>" name="background" id="app_theme_color" />
                                </fieldset>
                            </div>
                        </div>
                      </div>
				</div>
				<input type="submit" name="driverapisettings4" class="button button-primary" value="Save">

			 </form>
			</div>

			<!--app info end-->
			<div id="firebase" <?php if($tab=='firebase' ){echo 'style="display: block;"';}?> class="apitabcontent">
				<form  style=" float: left; width: 100%; " enctype="multipart/form-data" method="post" >
					<div class="row" style=" width: 100%; float: left; ">
						<div class="col-100">
							<hr>
								<b>Firebase Details</b>
							<hr> 
						</div>
						<div class="row" style=" width: 100%; float: left; ">
							<div class="col-100-flexbox">
								<label class="col50"> Api Key : </label>
								<textarea class="form-control apitext" type="text" name="ddwc_pro_settings_firebase_serverkey" id="ddwc_pro_settings_firebase_serverkey" /><?php echo $ddwc_pro_settings_firebase_serverkey;?></textarea>
							</div>
						</div>
					</div>
					<input type="submit" name="driverapisettings3" class="button button-primary" value="Save">
				</form>
			</div>
			
			<div id="api_lists" <?php if($tab=='api_lists' ){echo 'style="display: block;"';}?> class="apitabcontent">
				<div class="row" style=" width: 100%; float: left; ">
				<?php //if($woo_api_driver){ ?>
					<div class="col-100">
						<hr>
							<b>Driver Api Lists (post)</b>
						<hr> 
					</div>
					<div class="row" style=" width: 100%; float: left; ">
						<?php foreach ($user_methods as $method_name) { 
							if(!in_array($method_name,$remove_user_methods)){
							?>
								<code><a target="blank" href="<?php echo site_url(); ?>/wp-json/driverapi/<?php  echo $method_name; ?>"><?php  echo $method_name; ?></a></code>
							<?php 
							}
						} ?>
					</div>
					<div class="col-100">
						<hr>
							<b>Driver Order Api Lists (post)</b>
						<hr> 
					</div>
					<div class="row" style=" width: 100%; float: left; ">
						<?php foreach ($order_methods as $method_name) { 
							if(!in_array($method_name,$remove_order_methods)){
							?>
								<code><a target="blank" href="<?php echo site_url(); ?>/wp-json/driverorderapi/<?php  echo $method_name; ?>"><?php  echo $method_name; ?></a></code>
							<?php 
							}
						} ?>
					</div>
					<?php //} ?>
					<div class="col-100">
						<hr>
							<b>App Info Api Lists (post)</b>
						<hr> 
					</div>
					<div class="row" style=" width: 100%; float: left; ">
						<?php foreach ($appinfo_methods as $method_name) { 
							if(!in_array($method_name,$remove_appinfo_methods)){
							?>
								<code><a target="blank" href="<?php echo site_url(); ?>/wp-json/driverappinfoapi/<?php  echo $method_name; ?>"><?php  echo $method_name; ?></a></code>
							<?php 
							}
						} ?>
					</div>
					<div class="col-100">
						<hr>
							<b>Firebase Api Lists (post)</b>
						<hr> 
					</div>
					<div class="row" style=" width: 100%; float: left; ">
						<?php foreach ($firebase_methods as $method_name) { 
							if(!in_array($method_name,$remove_firebase_methods)){
							?>
								<code><a target="blank" href="<?php echo site_url(); ?>/wp-json/driverorderapi/<?php  echo $method_name; ?>"><?php  echo $method_name; ?></a></code>
							<?php 
							}
						} ?>
					</div>
				</div>
			</div>
		
			<div id="police_check_settings" <?php if($tab=='police_check_settings' ){echo 'style="display: block;"';}?> class="apitabcontent">
				<form  style=" float: left; width: 100%; " enctype="multipart/form-data" method="post" action="?page=woo-driver-api&tab=police_check_settings" >
					<div class="row" style=" width: 100%; float: left; ">
						<div class="col-100">
							<hr>
								<b>Police check settings</b>
							<hr> </div>
							<div class="row" style=" width: 100%; float: left; ">
								<div class="col-100-flexbox">
									<input class="form-control apicheckbox" <?php if($ddwc_police_check_enable){ echo 'checked'; } ?> name="ddwc_police_check_enable" id="ddwc_police_check_enable" type="checkbox" value="1">
									<label class="col50" for="woo_api_vendor"> Enable police check</label>
								</div>
							</div>
							<div class="row" style=" width: 100%; float: left; ">
								<div class="col-100-flexbox">
									<label class="col50"> Api Key : </label>
									<input class="form-control apitext" type="text" name="ddwc_police_check_api_key" id="ddwc_police_check_api_key" value="<?php echo $ddwc_police_check_api_key;?>" />
								</div>
							</div>
							<div class="row" style=" width: 100%; float: left; ">
								<div class="col-100-flexbox">
									<label class="col50">Api Secret : </label>
									<input class="form-control apitext" type="text" name="ddwc_police_check_api_secret" id="ddwc_police_check_api_secret" value="<?php echo $ddwc_police_check_api_secret;?>" />
								</div>
							</div>
							
					</div>
					<input type="submit" name="driverapisettings2" class="button button-primary" value="Save">
					</form>
			</div>
			
		</div>
	</div>
</div>
<style>
/*.app_logo img {
    margin-left: 20px;
}*/
@media screen and  (min-width: 992px){
.row>div {
    display: block;
    
    padding-left: 0px;
    padding-right: 0px;
}
}
.row,.row>div{
	display: block;
}


input[name="infosetting"] {
    border: white;
    padding: 1%;
    background: black;
    color: white;
    font-weight: bold;
    min-width: 100px;
}
.row.speceq {
    margin-bottom: 25px;
}
.row .col-100-flexbox {
    display: flex;
    margin-bottom: 8px;
}
.aap-info-enable input[type="checkbox"],.app_logo input#app_logo,.app_theme_color input#app_theme_color {
    margin-left: 25px;
    margin-top: 4px;
}
.row .aap-info-enable ,.row .app_logo,.row .app_theme_color{
    display: flex;
    align-items: center;
}

button.tablinks {
    border: white;
    padding: 1%;
    background: black;
    color: white;
    font-weight: bold;
    min-width: 100px;
}
input.apicheckbox {
    margin: 0;
    margin-right: 10px;
}
label.col50 {
    min-width: 150px;
}
input.apitext, textarea.apitext {
    min-width: 50%;
    min-height: 41px;
}
.row {
    margin-bottom: 10px;
}
div.apitabcontent {
    display: none;
}
div#api_lists code {
    margin-bottom: 10px;
    float: left;
    margin-left: 10px;
    padding: 5px 10px;
    font-weight: bold;
}

.icon_screen_detail img {
    width: 40px;
    margin-right: 20px;
}
.row.table-introscreen table tr th, .row.table-introscreen table tr td {
    
    padding: 10px 16px;
    margin: 0;
    
}
.row.table-introscreen table {
    width: 100%;
    text-align: center;
    padding: 0;
}
.row.table-introscreen fieldset table thead tr {
    background-color: #121212;
    color: #fff;
}
.row.table-introscreen fieldset table tbody tr.icon_screen_detail:nth-child(even) {
    background-color: #eee;
}
.logo_urldiv a.logo_remove,.custom_css a.remove_url {
	opacity: 0;
    position: absolute;
    left: 0;
    font-size: 12px;
    text-decoration: none;
    top: 0;
    display: flex;
    height: 100%;
    color: #fff;
    width: 100%;
    background-color: #0000004a;
    align-items: center;
    justify-content: center;
}
.logo_urldiv,.custom_css {
    margin-left: 20px;
    position: relative;
}
.logo_urldiv a img,.custom_css a img {
width:100px;

}
.custom_css a img{
width:60px;	
}
.custom_css{
	justify-content: center;
}
.row .logo_urldiv,.row .custom_css {
    margin-left: 20px;
    position: relative;
    /* overflow: hidden; */
    /* margin-top: 10px; */
    display: flex;
}
.logo_urldiv a.logo_url_img,.custom_css a.url_img {
    /* border: white; */
    text-decoration: none;
    
    line-height: 10px;
    
    color: white;
    font-weight: bold;
    min-width: 100px;
    margin-top: 4px;
    text-align: center;
}
/*.custom_css a.url_img{
min-width: 100px;

}*/
.logo_urldiv a.logo_url_img.bganchor,.custom_css a.url_img.bganchor {
    
    
    padding: 10px;
    
    background: black;
    
}
.logo_urldiv:hover a.logo_remove,.custom_css:hover a.remove_url{
	opacity: 1;
}
button.add_fields {
    border: white;
   padding: 10px;
    background: black;
    color: white;
    font-weight: bold;
    min-width: 100px;
    cursor: pointer;
}
button.add_fields span{
	font-size: 15px;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>

<script  type='text/javascript'>

	// on upload button click
	function media_upload_url(name){
  //
  event.preventDefault();
  		var classNames = event.target.className;
  		var classes = classNames.split(" ");

		

		var button = $(this),
		custom_uploader = wp.media({
			title: 'Insert image',
			library : {
				// uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
				type : 'image'
			},
			button: {
				text: 'Use this image' // button label text
			},
			multiple: false
		}).on('select', function() { // it also has "open" and "close" events
			var attachment = custom_uploader.state().get('selection').first().toJSON();
			
           var element = document.getElementsByClassName(classes[0])[0];
            console.log(document.getElementsByClassName(classes[0])[0]);
            element.classList.remove("bganchor");
			document.getElementsByClassName(classes[0])[0].innerHTML = '<img src="' + attachment.url + '"><input type="hidden" name="'+name+'" value="' + attachment.url + '">';
		}).open();

 
	};
 jQuery(function($){
	// on remove button click
	$('body').on('click', '.logo_remove', function(e){
 
		e.preventDefault();
 
		var button = $(this);
		button.next().val(''); // emptying the hidden field
		button.hide().prev().addClass('bganchor');
		//button.hide().prev().attr('onclick','media_upload_url("logo_url")');
		button.hide().prev().html('Upload image');
	});
	$('body').on('click', '.remove_url', function(e){
 
		e.preventDefault();
 
		var button = $(this);
		button.next().val(''); // emptying the hidden field
		button.hide().prev().addClass('bganchor');
		button.hide().prev().html('Upload image');
	});
 
});
	//Add Input Fields
jQuery(document).ready(function() {

    var max_fields = 10; //Maximum allowed input fields 
    var wrapper    = jQuery(".wrapper .table-introscreen table tbody"); //Input fields wrapper
    var add_button = jQuery(".add_fields"); //Add button class or ID
    var x = jQuery('.icon_screen_detail').length; //Initial input field is set to 1
 
 //When user click on add input button
 jQuery(add_button).click(function(e){
        e.preventDefault();
 //Check maximum allowed input fields
        if(x < max_fields){ 
            x++; //input field increment
 //add input field
 var htmltable = '<tr class="icon_screen_detail">'+
                                            '<td style="width: 120px;">'+
                                            '<div class="custom_css">'+
                                                '<a href="#" class="url_img_'+x+' url_img bganchor" onclick="media_upload_url(\'url[]\')">Upload image</a>'+
												'<a href="#" class="remove_url" style="display:none">Remove image</a>'+
												'</div>'+
                                            '</td>'+
                                            '<td>'+
                                                '<input style="width: 100%;" type="text" name="title[]" placeholder="Title" />'+
                                            '</td>'+
                                            '<td>'+
                                               ' <textarea  style="width: 100%;height: 100px;" name="content[]" placeholder="Content" /></textarea>'+
                                            '</td>'+
                                            '<td>'+
                                                '<a href="javascript:void(0);" class="remove_field">Remove</a>'+
                                            '</td>'+
                                        '</tr>';
            jQuery(wrapper).append(htmltable);
        }
    });
 
    //when user click on remove button
    $(wrapper).on("click",".remove_field", function(e){ 
        e.preventDefault();
 $(this).parents('tr').remove(); //remove inout field
 x--; //inout field decrement
    })
});
function opentab(e, t) {
    var a, l, n;
    for (l = document.getElementsByClassName("apitabcontent"), a = 0; a < l.length; a++) l[a].style.display = "none";
    for (n = document.getElementsByClassName("tablinks"), a = 0; a < n.length; a++) n[a].className = n[a].className.replace(" active", "");
    document.getElementById(t).style.display = "block", e.currentTarget.className += " active"
}</script>