<?php
/**
 * Plugin Name: Woo Delivery Drivers Json Api
 * Version:1.0
 * Author: Expert Web Technologies
 * Author URI: https://expertwebtechnologies.com/
 */

if (!defined('ABSPATH'))
{
    exit; // Exit if accessed directly
    
}
include_once (ABSPATH . 'wp-admin/includes/plugin.php');
if (!is_plugin_active('woocommerce/woocommerce.php'))
{
    return;
}
add_action('admin_enqueue_scripts', 'woodeliverydriver_include_js');
function woodeliverydriver_include_js()
{
    if (!did_action('wp_enqueue_media'))
    {
        wp_enqueue_media();
    }
    
}
function woodeliverydriver_jsonapi_install()
{

}

/* Uninstall Function */
function woodeliverydriver_jsonapi_uninstall()
{

}

/* Registration Hooks */
register_deactivation_hook(__FILE__, 'woodeliverydriver_jsonapi_uninstall');
register_activation_hook(__FILE__, 'woodeliverydriver_jsonapi_install');

add_action('init', 'woo_driver_setting_form_submit');
function woo_driver_setting_form_submit()
{
    if (isset($_POST["driverapisettings4"]))
    {
        $app_info = array();
        if (isset($_POST['intro_screen_enable']) && $_POST['intro_screen_enable'] == "yes")
        {
            $app_info['intro_screen_enable'] = 'yes';
            unset($_POST['driverapisettings4'], $_POST['intro_screen_enable']);
        }
        if (isset($_POST['background']))
        {
            $app_info['background'] = $_POST['background'];
            unset($_POST['driverapisettings4'], $_POST['intro_screen_enable'], $_POST['background']);
        }
        if ($_POST['logo'])
        {

            $app_info['logo'] = $_POST['logo'];

            unset($_POST['driverapisettings4'], $_POST['intro_screen_enable'], $_POST['theme_color'], $_POST['logo_url']);
        }
        $intro_screen = array();
        foreach ($_POST['url'] as $k => $v)
        {
            $intro_screen[$k] = array(
                'icon' => $v,
                'heading' => $_POST['title'][$k],
                'content' => stripslashes($_POST['content'][$k])
            );
        }

        $app_info['intro_screen'] = $intro_screen;

        update_option('woodeliverydriverappinfo', $app_info);
    }
    if (isset($_POST['driverapisettings1']))
    {
        $savedata = array(
            'woo_api_driver',
        );
        foreach ($savedata as $k)
        {
            if (!empty($_POST[$k]))
            {
                update_option($k, $_POST[$k]);
            }
            else
            {
                update_option($k, '');
            }
        }
    }

    if (isset($_POST['driverapisettings2']))
    {
        $savedata = array(
            'ddwc_police_check_enable',
            'ddwc_police_check_api_key',
            'ddwc_police_check_api_secret',
        );
        foreach ($savedata as $k)
        {
            if (!empty($_POST[$k]))
            {
                update_option($k, $_POST[$k]);
            }
            else
            {
                update_option($k, '');
            }
        }
    }

    if (isset($_POST['driverapisettings3']))
    {
        $savedata = array(
            'ddwc_pro_settings_firebase_serverkey',
        );
        foreach ($savedata as $k)
        {
            if (!empty($_POST[$k]))
            {
                update_option($k, $_POST[$k]);
            }
            else
            {
                update_option($k, '');
            }
        }
    }

}

add_action('admin_menu', 'woo_driver_api_menu');
function woo_driver_api_menu()
{
    add_menu_page('Woo Driver Api', 'Woo Driver Api', 'read', 'woo-driver-api', 'woo_driver_api');
}

function woo_driver_api()
{
    include 'includes/settings.php';
}

@include_once "includes/driverapi.php";
@include_once "includes/driverorderapi.php";
@include_once "includes/appinfo.php";
@include_once "includes/firebaseapi.php";
@include_once "includes/nationalcrimecheck.php";
